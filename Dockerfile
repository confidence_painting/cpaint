FROM pytorch/pytorch:1.7.1-cuda11.0-cudnn8-devel
LABEL maintainer "Alexander Mai alexandertmai@gmail.com"

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/:/usr/local/lib
ENV DEBIAN_FRONTEND=noninteractive

# Python takes longer with install
RUN apt-get update \
    && apt-get install -y --no-install-recommends dialog apt-utils \
    && apt-get install -y --no-install-recommends \
        # misc
        python3 python3-pip \
        wget apt-utils unzip build-essential sudo ca-certificates net-tools vim git cmake tmux openssh-client pigz \
        # opencv
        python3-dev python-dev libzstd-dev libfaac-dev libmp3lame-dev \
        libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev ffmpeg x264 \
        libx264-dev libv4l-0 v4l-utils libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libgtk2.0-dev \
        libtbb2 libtbb-dev libjpeg-dev libpng-dev libdc1394-22-dev libharfbuzz-dev \
        libfreetype6-dev jsonlint rsync libopencv-dev \
        # open3d
        libglfw3-dev libglew-dev libeigen3-dev openmpi-common openmpi-bin libboost1.65-dev \
        # octree
        libglm-dev \
    && pip3 install setuptools wheel

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install CMake 3.12 for open3D
RUN set -x \
    && apt-get install -y --no-install-recommends apt-transport-https ca-certificates gnupg software-properties-common wget \
    && wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null \
    && apt-add-repository -y 'deb https://apt.kitware.com/ubuntu/ bionic main' \
    && apt-get update \
    && apt-get install -y --no-install-recommends cmake

# Install open3d
WORKDIR /root/
RUN set -x \
    && git clone --recursive https://github.com/intel-isl/Open3D && mkdir /root/Open3D/build \
    && cd /root/Open3D \
    && bash /root/Open3D/util/install_deps_ubuntu.sh assume-yes \
    && cd /root/Open3D/build \
    && cmake -DBUILD_PYBIND11=OFF -DPYTHON_EXECUTABLE=$(which python3) -DCMAKE_INSTALL_PREFIX=/usr/ -DBUILD_SHARED_LIBS=ON .. \
    && make -j16 \
    && make install \
    && cd /root/ \
    && rm -r /root/Open3D

ENV MINICONDA_VERSION latest
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash \
    PATH=/opt/conda/bin:$PATH

SHELL ["/bin/bash", "-c"]
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

# Install conda
# RUN set -x \
#     && cd /tmp \
#     && wget --quiet https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh \
#     && /bin/bash Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR \
#     && rm Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh \
#     && $CONDA_DIR/bin/conda config --system --prepend channels conda-forge \
#     && $CONDA_DIR/bin/conda config --system --set auto_update_conda false \
#     && $CONDA_DIR/bin/conda config --system --set show_channel_urls true \
#     && $CONDA_DIR/bin/conda update --all --quiet --yes \
#     && echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc \
#     && echo "conda activate base" >> ~/.bashrc \
#     && find /opt/conda/ -follow -type f -name '*.a' -delete \
#     && find /opt/conda/ -follow -type f -name '*.js.map' -delete \
#     && /opt/conda/bin/conda clean -afy \
#     && conda init bash
#
# ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/conda/envs/tf/lib
RUN echo "imap fd <Esc>" > ~/.vimrc

# Install env deps
# ADD ./environment.yml /root/
# RUN set -x \
#     && conda env create -n /root/th --file environment.yml
    # && conda env update --file environment.yml
# RUN set -x \
#     && conda install anaconda-client -n base \
#     && conda env create -n /root/th

ADD ./requirements.txt /root/
# RUN conda run -n th pip install -r /root/requirements.txt
RUN pip3 install scikit-build && \
    pip3 install -r /root/requirements.txt && \
    pip3 install imgaug

# COMPILE OVERLAP_LIB
add . /root/cpaint
ENV OVERLAP_LIB=/root/cpaint/cpaint/overlap_lib

# Install octree
WORKDIR /root
RUN set -x \
    && git clone https://gitlab.com/confidence_painting/octree.git \
    && mkdir octree/build \
    && cd octree/build \
    && cmake -D ENABLE_VIS=OFF .. \
    && make install -j4 \
    && cd ../

WORKDIR $OVERLAP_LIB
RUN set -x \
  && rm -rf $OVERLAP_LIB/build \
  && mkdir $OVERLAP_LIB/build \
  && cd $OVERLAP_LIB/build \
  && sudo ln -s /usr/include/open3d /usr/include/Open3D \
  # && conda run -n th cmake -DCMAKE_PREFIX_PATH=/opt/conda/envs/tf/lib/python3.6/site-packages/torch/share/cmake/Torch/ .. \
  && cmake -DCMAKE_PREFIX_PATH=/opt/conda/lib/python3.8/site-packages/torch/share/cmake/Torch/ .. \
  && make -j4

WORKDIR /root/
