import os
import random

num_per = 10
areas = os.listdir("gibson")
random.shuffle(areas)

for i in range(len(areas)//num_per):
    with open("splits/split_{}.txt".format(i), "w+") as f:
        f.write(" ".join([os.path.join("gibson", areas[num_per*i+j]) for j in range(num_per)]))
