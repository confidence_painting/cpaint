import subprocess
import re
import os
import argparse

size_re = re.compile(r"^.*got mesh bounding box \[([-+]?\d*\.\d+|\d+), ([-+]?\d*\.\d+|\d+), ([-+]?\d*\.\d+|\d+).*\[([-+]?\d*\.\d+|\d+), ([-+]?\d*\.\d+|\d+), ([-+]?\d*\.\d+|\d+)")
base_dir = "gibson_scene_data"
resolution = 0.010
binvox2colorbt = "binvox2bt"

logs = b""

measuring = True
metadata = ["area, dim_x, dim_y, dim_z, octores"]

AREAS = [
    'American', 'Wyatt', 'Eastville', 'Landing', 'Aulander', 'Beach',
    'Wattsville', 'Deemston', 'Pettigrew', 'Dauberville', 'Stokes',
    'Anthoston', 'Assinippi', 'Kremlin', 'Cobalt', 'Annawan', 'Ewansville',
    'Dunmor', 'Fleming', 'Coeburn', 'Callicoon', 'Kangley', 'Munsons',
    'Moberly', 'Dryville', 'Gasburg', 'Hornsby', 'Brentsville', 'Dalcour',
    'Fredericksburg', 'Lluveras', 'Coronado', 'Circleville', 'Bremerton',
    'Frierson', 'Grassy', 'Broadwell', 'Inkom', 'Kinde', 'McEwen', 'Cornville',
    'Mobridge', 'Maryhill', 'Brewton', 'Espanola', 'Copemish', 'Auburn',
    'Oriole', 'Bountiful', 'Caruthers', 'Traver', 'Artois', 'Schoolcraft',
    'Ashport', 'Hambleton', 'Seiling', 'Booth', 'Pleasant', 'Herricks',
    'Corder', 'Random', 'Castroville', 'Roane', 'Macksville', 'Mullica',
    'Capistrano', 'Calmar', 'Belpre', 'Mayesville', 'Connellsville',
    'Kerrtown', 'Ooltewah', 'Mahtomedi', 'Islandton', 'Imbery', 'Kathryn',
    'Jenners', 'Cason', 'Alstown', 'Maiden', 'Adrian', 'Cantwell', 'Destin',
    'Jacobus', 'Lenoir', 'Nicut', 'Galatia', 'Yscloskey', 'Macedon',
    'Michiana', 'Carpendale'
]

def voxelize(area):
    global logs, metadata
    # First, get the size of the area by running binvox
    # We are looking for:
    # got mesh bounding box [-10.0101, -0.416016, -2.54232, 1] - [2.27441, 2.76595, 14.3983, 1]
    binvox_f = f"{base_dir}/{area}/mesh.bk.binvox"
    if os.path.isfile(binvox_f):
        os.remove(binvox_f)
    print("Finding size")
    o = subprocess.run(["binvox", f"{base_dir}/{area}/mesh.bk.obj", "-e", "-fit", "-d", "100"], stdout=subprocess.PIPE)
    logs += o.stdout
    size_line = [i for i in str(o.stdout).split("\r") if "got mesh bounding box" in i]
    if len(size_line) == 0:
        return
    size_line = size_line[0]
    matches = size_re.match(size_line)
    values = [float(matches.group(i)) for i in range(1, 7)]
    dim_x = values[3] - values[0]
    dim_y = values[4] - values[1]
    dim_z = values[5] - values[2]
    num_vox = min(int(max([dim_x, dim_y, dim_z])/resolution), 2500)
    effective_res = num_vox / max([dim_x, dim_y, dim_z])

    if measuring:
        metadata.append(",".join([area, str(dim_x), str(dim_y), str(dim_z), str(effective_res)]))
        return True
    os.remove(binvox_f)
    print(f"Running binvox with size: {num_vox}")
    o = subprocess.run(["binvox", f"{base_dir}/{area}/mesh.bk.obj", "-e", "-fit", "-d", f"{num_vox}"], stdout=subprocess.PIPE)
    logs += o.stdout
    print(f"Running binvox2colorbt")
    o = subprocess.run([binvox2colorbt, "--mark-free", binvox_f], stdout=subprocess.PIPE)
    logs += o.stdout
    print(o.stdout)
    if b"!" in o.stdout:
        return False
    return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("areas", type=str, nargs="+",
                        help="Number of threads to use.", default=20)

    args = parser.parse_args()
    failed = []
    for area in AREAS:
        print(f"Processing: {area}")
        if not voxelize(area):
            print(f"Failed to voxelize: {area}")
            failed.append(area)
        print(failed)
    with open("metadata.csv", "w") as f:
        f.write("\n".join(metadata))
