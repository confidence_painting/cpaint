import os
import shutil
import yaml
import argparse

job_dir = "./training_jobs"

parser = argparse.ArgumentParser()
parser.add_argument("config", default="4gpu_trainjob.yaml", type=str, help="Yaml file that specficies arguments for yaml file. Yes.")
args = parser.parse_args()

with open(args.config, "r") as f:
    config = yaml.load(f)

# Create dir if it doesn't exist
if os.path.exists(job_dir):
    shutil.rmtree(job_dir)
os.makedirs(job_dir, exist_ok=True)
os.makedirs(os.path.join(job_dir, "Forests"), exist_ok=True)

with open("./templates/train.yaml", "r") as f:
    template = "".join(f.readlines())

# Create master
for key, val in config.items():
    template = template.replace("{{{{{}}}}}".format(key), str(val))

with open("./areas.txt", "r") as f:
    areas = [[l.strip() for l in s.split(" ")] for s in f.readlines()]

for (area, ram) in areas:
    with open(os.path.join(job_dir, f"{area.replace('_', '-')}.yaml"), "w+") as f:
        ram = str(int(2*float(ram)+1) + config["RAM"])
        temp = template.replace("{{AREA}}", area)
        temp = temp.replace("{{MAX_RAM}}", ram)
        temp = temp.replace("{{NAME}}", f"{area.replace('_', '-')}".lower())
        temp = temp.replace("{{RUN_NAME}}", f"{config['RUN_PREFIX']}-{area.replace('_', '-')}".lower())
        f.write(temp)
