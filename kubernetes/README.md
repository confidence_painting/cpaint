
# Why Cluster Computation?
Many of the processing steps for this pipeline are extremely compute intensive,
especially CPU and RAM intensive. As a result, if you want to experiment with
new datasets and methods, I would recommend trying to gain access to a cluster 
computer as many of the jobs might take up to a week to run without one. 
Fortunately, most of them only need to be performed once and I will try to
release the results of the computation so you need not perform them yourself.

## Why Kubernetes
It's what the Pacific Research Platform uses, which is the cluster I used for
this work.

## Use
Kubernetes does not save the 
contents of the computers used for computation. Instead, a more permanent 
location is needed. I used CephFS, but you might have a different system. Make 
sure to mount the filesystem at `/root/data`. Here was the layout of my storage 
container:

For detection:
- `/root/data/superpoint_out/outputs`: The result directory. The tars containing
results end up here.
- `/root/data/gibson_views_{split_num}.tar.gz`: The different tars containing 
areas of the dataset. These must correspond to the split.txt at the base of the 
kubernetes directory

I have written template files for each of the intensive jobs, mainly 
confidence painting itself and overlap detection. 
You will need to modify the template files for each of these jobs before running
them yourself. The RAM requirement is not negotiable. Make sure to clean out 
completed.txt. This file will come in handy in case you think you can lower the 
RAM requirement, or when some of the pods finish before the others. I recommend 
reworking this code so there is a server that determines which computers handle 
which areas because of the difference in processing time.

# Notes to Future Users
Please rewrite this section it's got so much code duplication
