Goals: 
- Create a ceph storage container for our training data
- Create a Docker image with our training code
- Host the image on a registry
- Access our data from our Docker image
- Parallelize training across multiple machines

Boilerplate:
  apiVersion: v1
  kind: Pod
  metadata:
    name: kubernetes_guide # name it something sexy, like 
  spec:
    port:
      - containerPort: 6006 # port forwarding for tensorboard
    replicas: 1 # number of copies of this pod to run
    selector:
      matchLabels:
        app: kubernetes_guide # name to find pod
    containers: # These are a list of the docker programs we want running on our instance
    - name: gpu-container # whatever yah want
      image: gitlab-registry.nautilus.optiputer.net/prp/jupyterlab:latest # link to image on repo
      command: ["sh", "/root/training_code/train.sh"] # command to run on start up
      resources:
        limits:
          nvidia.com/gpu: 8 # 8 is the max if you have a max of 8 GPUs per a machine, which we do
    imagePullSecrets:
    - name: regcred # this is the secret for you credentials to pull a private image

Cloud Storage:
  Ceph: https://ucsd-prp.gitlab.io/userdocs/storage/ceph/#ceph_shared
  - Since we are using our data on multiple machines, we will be using the Ceph Filesystem.
  - You will need a CEPH_KEY from your admin
  - kubectl create secret -n <namespace> generic <name of ceph secret> --from-literal=key=<ceph key>
  - Add this snippet to your .yaml
    volumes:
    - name: fs-store
      flexVolume:
        driver: ceph.rook.io/rook
        fsType: ceph
        options:
          clusterNamespace: rook
          fsName: nautilusfs
          path: /data # name this path whatever
          mountUser: USER # your username?
          mountSecret: <name of ceph secret>

Container:
  Docker:
  - You will want to start with an image from nvidia that comes with CUDA
  - https://hub.docker.com/r/nvidia/cuda
    Commands:
    - https://docs.docker.com/engine/reference/builder/
      Some common ones:
      - ADD . /<code dir> to add your code to the image
      - RUN <command> to run bash commands
      - USER <user> to set user
      - ENV VAR=<stuff> to set environment variables
      - COPY <X> <Y>
      - EXPOSE <port>
      - ENTRYPOINT ["executable", "param1", "param2", "..."] # this is run when the container is run
  - When building a docker image, docker will copy the entire directory. You probably don't want this, so add a .dockerignore
  - ADD will always run again when building a container, and since docker caches sequentially you want to run it as late as possible to avoid rerunning commands
  - Each time you run a command, docker makes a backup of the container, which means you want to reduce the number of commands you run
  SSH:
  - docker ps # get the name of the container
  #OR
  - kubectl get po 
  - docker exec -it <container name> /bin/bash
  #OR
  - kubectl exec -it <container name> -- /bin/bash
  Image:
    Creating: # run the following to build your image
    - docker build -t <name> .
    - docker tag <OPTIONS> IMAGE:<TAG> <REGISTRYHOST>/<USERNAME>/NAME:<TAG>
    - docker login gitlab-registry.nautilus.optiputer.net # you probably want to use pass for passwords
    - docker build -t gitlab-registry.nautilus.optiputer.net/prp/jupyterlab .
    - docker push gitlab-registry.nautilus.optiputer.net/prp/jupyterlab
      NamingOptions: # alternative naming conventions
      - gitlab-registry.nautilus.optiputer.net/prp/jupyterlab:tag
      - gitlab-registry.nautilus.optiputer.net/prp/jupyterlab/optional-image-name:tag
      - gitlab-registry.nautilus.optiputer.net/prp/jupyterlab/optional-name/optional-image-name:tag
    Private:
      - If you want your image to be private, you are going to have to create a secret that has your credential connected to it
      - https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/

Finale:
  - kubectl create -n your_namespace -f <link to raw yaml>
  - kubectl delete -n your_namespace -f <link to raw yaml>
  docker to kubernetes commands: https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/
