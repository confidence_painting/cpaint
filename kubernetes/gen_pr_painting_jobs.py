import os
import shutil
import yaml
import argparse
import json
from cpaint.core import paths


def gen_ram_map():
    with open("./areas.txt", "r") as f:
        areas = [[l.strip() for l in s.split(" ")] for s in f.readlines()]

    ram_map = {}
    for (area, ram) in areas:
        ram = str(int(2*float(ram)+1) + config["RAM"] + 10)
        ram_map[area] = ram
    return ram_map

def configure_templates(job_conf, run_dat, config, ram_map):
    temp_path = job_conf["template"]
    job_dir = job_conf["job_dir"]
    # Create dir if it doesn't exist
    if os.path.exists(job_dir):
        shutil.rmtree(job_dir)
    print(job_dir)
    os.makedirs(job_dir, exist_ok=True)

    with open(temp_path, "r") as f:
        template = "".join(f.readlines())

    # Create master
    for key, val in config.items():
        template = template.replace("{{{{{}}}}}".format(key), str(val))


    for dat in run_dat:
        area = dat["area"]
        run_name = paths.name_prefix(dat["prefix"], dat["run_num"])
        ram = ram_map[area]

        with open(os.path.join(job_dir, f"{run_name}.yaml"), "w+") as f:
            temp = template.replace("{{AREA}}", area)
            temp = temp.replace("{{MAX_RAM}}", ram)
            temp = temp.replace("{{NAME}}", f"{run_name}".lower().replace("_", "-"))
            temp = temp.replace("{{RUN_NAME}}", run_name)
            f.write(temp)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("config", default="4gpu_trainjob.yaml", type=str, help="Yaml file that specficies arguments for yaml file. Yes.")
    parser.add_argument("run_config", default="../run_configs/run0.json", type=str, help="Json file that specifices the details about the runs")
    args = parser.parse_args()

    with open(args.config, "r") as f:
        config = yaml.load(f)

    with open(args.run_config, "r") as f:
        run_dat = json.load(f)
    JOBS = [
        {
            "template": "./templates/pr_painting.yaml",
            "job_dir": "./pr_painting_jobs"
        },
        {
            "template": "./templates/precomp.yaml",
            "job_dir": "./precomp_jobs"
        },
    ]
    ram_map = gen_ram_map()
    for job_conf in JOBS:
        configure_templates(job_conf, run_dat, config, ram_map)
