import os
import shutil
import argparse

job_dir = "./process_dataset"

parser = argparse.ArgumentParser()
parser.add_argument("area", type=str, help="Area to spin off job for")
args = parser.parse_args()

#  if os.path.exists(job_dir):
#      shutil.rmtree(job_dir)
os.makedirs(job_dir, exist_ok=True)

with open("./templates/process_dataset.yaml", "r") as f:
    template = "".join(f.readlines())

name = args.area.replace('/', '-').lower().replace('_', '-')

job_txt = template.replace("{{AREA}}", args.area)
job_txt = job_txt.replace("{{i}}", name)
with open(os.path.join(job_dir, f"process_{name}.yaml"), "w+") as f:
    f.write(job_txt)
