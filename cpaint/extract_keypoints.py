# Extracts:
# Keypoints: N x (x, y, score, scale, orientation)
# Descriptors: N x D 

# Expected directory structure:
# dataset_dir/FM-Bench/Dataset/{area}
# dataset_dir/d2-net
# dataset_dir/2020VisualLocalization/Aachen-Day-Night

import os
import cv2
import toml
import torch
import argparse
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt

from cpaint.test_datasets import DATASETS
from cpaint import models
from cpaint.core import detectors, config, descriptors
from cpaint.models import multiscale

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("computer_config", type=str, help="Config for computer")
    parser.add_argument("net_config", type=str, help="Config for model")
    parser.add_argument("area", type=str, help="Area to run and export test on")
    parser.add_argument("tag", type=str, help="Name of run")
    parser.add_argument("--dataset_dir", type=str, help="Check source for details on directory structure")
    parser.add_argument("--threshold", type=float, help="The threshold")
    parser.add_argument("--descriptor", default='', choices=list(descriptors.DESCRIPTORS.keys()), help="The descriptor to be used for the detector")
    parser.add_argument("--checkpoint", type=str, default="checkpoints/first_sota_cpainted.pth", help="Checkpoint for run, if one is to be used")
    parser.add_argument("--display", action="store_true", default=False, help="Display results for debugging")
    parser.add_argument("--multiscale", action="store_true", default=False, help="Extract keypoints with different sizes")
    parser.add_argument("--use_pretrained", action="store_true", default=False, help="Use load default_state_dict")

    # Parse
    args = parser.parse_args()
    config.augment_args(args, args.computer_config)
    config.augment_args(args, args.net_config)
    print(f"Threshold: {args.threshold}")
    if args.threshold is not None:
        args.threshold = float(args.threshold)

    if args.model in detectors.DETECTORS:
        detector = detectors.DETECTORS[args.model]()
    else:
        # Init torch
        torch.manual_seed(args.seed)
        device = torch.device("cuda")
        kwargs = {'num_workers': 4, 'pin_memory': True}

        # Init model
        model_cfg = models.MODELS[args.model]
        loss_fn = model_cfg["loss"]
        model = model_cfg["net"]().to(device)
        if (args.checkpoint != ''):
            ckpt = torch.load(args.checkpoint)
            if "state_dict" in ckpt:
                state_dict = ckpt["state_dict"]
            else:
                state_dict = ckpt
            state_dict = {".".join(k.split(".")[1:]): v for k, v in state_dict.items()}
            model.load_state_dict(state_dict)
        else:
            model.load_default_state_dict()
        model.eval()

        #  feat_ext = models.model_to_feature_extractor(model, model_cfg["color_input"], args.threshold, model_cfg["input_size_multiple"])
        detector = models.model_to_detector(model, model_cfg["color_input"], args.threshold, model_cfg["input_size_multiple"])
        #  detector = detectors.DETECTORS['superpoint']()
        #  detector = detectors.doublewrap(detector)()

    if args.descriptor != '':
        desc = descriptors.DESCRIPTORS[args.descriptor]()
        feat_ext = descriptors.combine_desc_det(detector, desc)
        #  det = models.model_to_detector(model, model_cfg["color_input"], model_cfg["input_size_multiple"])
        #  det = detectors.doublewrap(det)()
        #  desc = models.model_to_descriptor(model, model_cfg["color_input"], model_cfg["input_size_multiple"])
    else:
        feat_ext = detector

    # Init dataset
    ds = DATASETS[args.area](args.dataset_dir, args.tag)

    # Extract
    print(f"Extracting features to {ds.output_dir}")
    with torch.no_grad():
        for i, img in tqdm(enumerate(ds), total=len(ds)):
            #  (Nx3) pts = (row, col, score) keypoints in image coordinates
            #  (NxD) desc = corresponding descriptors
            if args.multiscale:
                pts = multiscale.detect_multiscale(feat_ext, img, color_input=model_cfg["color_input"], base_size=5)
                # Doesn't check if it lots keypoints
                _, desc = feat_ext.compute(img, pts)
            else:
                pts, desc = feat_ext.detectAndCompute(img)

            # Pad to fit into test dataset api, then save
            ppts = np.zeros((pts.shape[0], 5))
            ppts[:, 0] = pts[:, 1]
            ppts[:, 1] = pts[:, 0]
            ppts[:, 2] = pts[:, 2]
            ppts[:, 3] = pts[:, 3]
            ds.save_kps(ppts, desc, i)

            if args.display:
                key_pts = [cv2.KeyPoint(p[1], p[0], _size=int(p[3])) for p in pts]
                kkpts_img = img.copy()
                kkpts_img = cv2.drawKeypoints(img, key_pts, kkpts_img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
                #  kkpts_img = cv2.drawKeypoints(img, key_pts, kkpts_img)

                fig = plt.figure(0)
                plt.imshow(kkpts_img)
                plt.show()
            else:
                plt.clf()

    ds.close()
