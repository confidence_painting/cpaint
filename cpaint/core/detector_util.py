import math
import torch
from torch import nn
import numpy as np
import torch.nn.functional as F
import matplotlib.pyplot as plt
import time

cross_correlation = torch.tensor([
    [0., 0., 1., 0., 0.],
    [0., 0.,-8., 0., 0.],
    [0., 0., 0., 0., 0.],
    [0., 0., 8., 0., 0.],
    [0., 0.,-1., 0., 0.]]).view(1, 1, 5, 5).float()/12

#  cross_correlation = torch.tensor([
#      [0., 0., 0., 0., 0.],
#      [0., 0.,-1., 0., 0.],
#      [0., 0., 0., 0., 0.],
#      [0., 0., 1., 0., 0.],
#      [0., 0.,-0., 0., 0.]]).view(1, 1, 5, 5).float()/12

dii_filter = torch.tensor([
    [1., -2., 1.],
    [2., -4., 2.],
    [1., -2., 1.]]).view(1, 1, 3, 3).float()

dij_filter = 0.25*torch.tensor([
    [1., 0., -1.],
    [0., 0., 0.],
    [-1., 0., 1.]]).view(1, 1, 3, 3).float()

djj_filter = torch.tensor([
    [1., 2., 1.],
    [-2., -4., -2.],
    [1., 2., 1.]]).view(1, 1, 3, 3).float()

di_filter = torch.tensor([
    [1., 0., -1.],
    [2., 0., -2.],
    [1., 0., -1.]]).view(1, 1, 3, 3).float()

dj_filter = torch.tensor([
    [1., 2., 1.],
    [0., 0., 0.],
    [-1., -2., -1.]]).view(1, 1, 3, 3).float()

# pytorch
def mask_border(score_map, border=4):
    batch = score_map.view(-1, 1, score_map.shape[-2], score_map.shape[-1])

    N, _, H, W = batch.shape
    mask = torch.ones((N, 1, H-border*2, W-border*2)).float().to(score_map.device)
    mask = F.pad(mask, [border]*4, mode="constant", value=0).view(N, H, W)
    return mask

# pytorch
def mask_max(score_map, radius=8):
    batch = score_map.view(-1, 1, score_map.shape[-2], score_map.shape[-1])
    N, _, H, W = batch.shape

    padded = F.pad(batch, [radius] * 4, mode='constant', value=0.)
    l_max = F.max_pool2d(
            padded,
            radius*2+1, stride=1
        )
    mask = l_max == batch
    return mask.view(N, H, W)

def depth_to_space(batch, cell_size=8):
    N, d, Hc, Wc = batch.shape
    H, W = Hc*cell_size, Wc*cell_size

    batch = batch.permute(0, 2, 3, 1)  # N x Hc x Wc
    heatmap = batch.reshape([N, Hc, Wc, cell_size, cell_size])
    heatmap = heatmap.permute(0, 1, 3, 2, 4)  # N x Hc x c x Wc x c
    heatmap = heatmap.reshape([N, 1, H, W])
    return heatmap

def space_to_depth(batch, cell_size=8):
    N, _, H, W = batch.shape
    Hc, Wc = H//cell_size, W//cell_size
    cells = batch.view(-1, Hc, cell_size, Wc, cell_size) \
                 .permute(0, 2, 4, 1, 3) \
                 .reshape(-1, cell_size*cell_size, Hc, Wc)
    return cells

def mask_max_cellwise(score_map, cell_size=8):
    batch = score_map.view(-1, 1, score_map.shape[-2], score_map.shape[-1])
    N, _, H, W = batch.shape
    cells = space_to_depth(batch)
    maxv = torch.max(cells, dim=1).values.unsqueeze(1)
    maximal = cells == maxv
    return depth_to_space(maximal)

# pytorch
def edgyness(score_map):
    global gf1
    batch = score_map.view(1, 1, score_map.shape[-2], score_map.shape[-1]).float()
    #  batch = gf1(batch)
    #  dii = F.conv2d(batch, dii_filter, padding=3, dilation=3, stride=1)
    #  dij = F.conv2d(batch, dij_filter, padding=3, dilation=3, stride=1)
    #  djj = F.conv2d(batch, djj_filter, padding=3, dilation=3, stride=1)
    kern = cross_correlation
    Ix = F.conv2d(batch, kern, padding=kern.shape[-1]//2, stride=1)
    Iy = F.conv2d(batch, kern.transpose(2, 3), padding=kern.shape[-1]//2, stride=1)

    w = 1
    s = w*2+1
    w_ix2 = F.avg_pool2d(Ix*Ix, s, stride=1, padding=w)*s*s
    w_iy2 = F.avg_pool2d(Iy*Iy, s, stride=1, padding=w)*s*s
    w_ixiy = F.avg_pool2d(Ix*Iy, s, stride=1, padding=w)*s*s
    tr = w_ix2 + w_iy2
    det = w_ix2*w_iy2 - w_ixiy**2
    J0 = (tr - torch.sqrt(tr**2 - 4*det))/2
    J0[torch.isnan(J0)] = 0

    return J0.squeeze(0)

# pytorch
def harris(score_map):
    global gf1
    batch = score_map.view(1, 1, score_map.shape[-2], score_map.shape[-1]).float()
    #  batch = gf1(batch)
    #  dii = F.conv2d(batch, dii_filter, padding=3, dilation=3, stride=1)
    #  dij = F.conv2d(batch, dij_filter, padding=3, dilation=3, stride=1)
    #  djj = F.conv2d(batch, djj_filter, padding=3, dilation=3, stride=1)
    kern = cross_correlation
    Ix = F.conv2d(batch, kern, padding=kern.shape[-1]//2, stride=1)
    Iy = F.conv2d(batch, kern.transpose(2, 3), padding=kern.shape[-1]//2, stride=1)

    w = 1
    s = w*2+1
    w_ix2 = F.avg_pool2d(Ix*Ix, s, stride=1, padding=w)*s*s
    w_iy2 = F.avg_pool2d(Iy*Iy, s, stride=1, padding=w)*s*s
    w_ixiy = F.avg_pool2d(Ix*Iy, s, stride=1, padding=w)*s*s
    tr = w_ix2 + w_iy2
    det = w_ix2*w_iy2 - w_ixiy**2
    R = det - k*tr**2
    J0 = (tr - torch.sqrt(tr**2 - 4*det))/2
    J0[torch.isnan(J0)] = 0

    return J0.squeeze(0)

# pytorch
def mask_edgyness(score_map, r=10):
    emap = edgyness(score_map)
    threshold = (r+1)**2/r
    return emap > threshold

def mask_max_ensure_ratio(score_map, radius=8, ratio=0.7):
    batch = score_map.view(-1, 1, score_map.shape[-2], score_map.shape[-1])
    N, _, H, W = batch.shape

    l_max1 = F.max_pool2d(
            F.pad(batch, [radius] * 4, mode='constant', value=0.),
            radius*2+1, stride=1
        )
    mask = l_max1 == batch

    # enlarge mask
    enlarge_rad = 1
    enlarged_mask = F.max_pool2d(
            F.pad(mask.float(), [enlarge_rad] * 4, mode='constant', value=0.),
            enlarge_rad*2+1, stride=1
        ) > 0

    masked_score = batch*(~enlarged_mask.view(N, 1, H, W))

    l_max2 = F.max_pool2d(
            F.pad(masked_score, [radius] * 4, mode='constant', value=0.),
            radius*2+1, stride=1
        )
    right_ratio = l_max1 > (l_max2/ratio)
    return right_ratio * mask

# pytorch
def peakiness(score_map):
    b = 1
    soft_local_max_size = 5
    batch = score_map.view(1, 1, score_map.shape[0], score_map.shape[1])
    max_per_sample = torch.max(batch.view(b, -1), dim=1)[0]
    #  exp = torch.exp(batch / max_per_sample.view(b, 1, 1, 1))
    exp = batch
    #  exp = torch.exp(batch)
    sum_exp = (
        soft_local_max_size ** 2 *
        F.avg_pool2d(
        #  F.max_pool2d(
            F.pad(exp, [soft_local_max_size//2] * 4, mode='constant', value=1.),
            soft_local_max_size, stride=1
        )
    )
    local_max_score = exp / sum_exp
    local_max_score[torch.isnan(local_max_score)] = 0
    local_max_score = local_max_score - torch.min(local_max_score)
    return local_max_score.squeeze()

def mask_local_max(score_map, eps=0.1):
    global gf1
    batch = score_map.view(1, 1, score_map.shape[-2], score_map.shape[-1]).float()
    #  batch = gf1(batch)
    di = F.conv2d(batch, di_filter, padding=1, dilation=1, stride=1)
    dj = F.conv2d(batch, dj_filter, padding=1, dilation=1, stride=1)
    return (torch.abs(di) < eps) & (torch.abs(dj) < eps)

def mask_second_order_max(score_map, eps=1e-4):
    global gf1
    batch = score_map.view(1, 1, score_map.shape[-2], score_map.shape[-1]).float()
    batch = gf1(batch)
    dii = F.conv2d(batch, dii_filter, padding=1, dilation=1, stride=1)
    dij = F.conv2d(batch, dij_filter, padding=1, dilation=1, stride=1)
    djj = F.conv2d(batch, djj_filter, padding=1, dilation=1, stride=1)
    # We want to know if the hessian is psd
    # This is only the case if the trace and det are > 0
    det = dii*djj - dij * dij  # determinate
    tr = dii + djj  # trace
    return (det > -eps) & (tr > -eps), dii, dij, djj, det, tr

def score_gaussian_peaks(score_map):
    global gf1
    batch = score_map.view(1, 1, score_map.shape[-2], score_map.shape[-1]).float()
    batch = gf1(batch)
    dii = F.conv2d(batch, dii_filter, padding=1, dilation=1, stride=1)
    djj = F.conv2d(batch, djj_filter, padding=1, dilation=1, stride=1)
    di = F.conv2d(batch, di_filter, padding=1, dilation=1, stride=1)
    dij = F.conv2d(di, dj_filter, padding=1, dilation=1, stride=1)
    s = torch.min(dij, -dij)
    #  score = torch.min(torch.min(-dii, -djj), torch.abs(dij))/2
    score = torch.min(-dii, -djj)/2
    return s


def gaussian_filter(kernel_size, sigma, channels=1):
    # Create a x, y coordinate grid of shape (kernel_size, kernel_size, 2)
    x_cord = torch.arange(kernel_size)
    x_grid = x_cord.repeat(kernel_size).view(kernel_size, kernel_size)
    y_grid = x_grid.t()
    xy_grid = torch.stack([x_grid, y_grid], dim=-1)

    mean = (kernel_size)/2.
    variance = sigma**2.

    # Calculate the 2-dimensional gaussian kernel which is
    # the product of two gaussian distributions for two different
    # variables (in this case called x and y)
    gaussian_kernel = (1./(2.*math.pi*variance)) *\
                      torch.exp(
                          -torch.sum((xy_grid - mean)**2., dim=-1) /\
                          (2*variance)
                      )
    # Make sure sum of values in gaussian kernel equals 1.
    gaussian_kernel = gaussian_kernel / torch.sum(gaussian_kernel)

    # Reshape to 2d depthwise convolutional weight
    gaussian_kernel = gaussian_kernel.view(1, 1, kernel_size, kernel_size)
    gaussian_kernel = gaussian_kernel.repeat(channels, 1, 1, 1)

    gaussian_filter = nn.Conv2d(in_channels=channels, out_channels=channels, padding=kernel_size//2,
                                kernel_size=kernel_size, groups=channels, bias=False)

    gaussian_filter.weight.data = gaussian_kernel
    gaussian_filter.weight.requires_grad = False
    return gaussian_filter

# pytorch
gf1 = gaussian_filter(3, 0.5)
gf2 = gaussian_filter(5, 2)

# numpy
def nms_score_map(score_map, radius=2):
    # this is maximal edge detection lmao
    tens = torch.from_numpy(score_map).unsqueeze(0)
    mask1 = mask_max(tens, radius)
    mask2 = mask_border(tens)
    return score_map*np.array(mask1*mask2).squeeze()

# numpy
def score_map_to_kpts(score_map, top_n=1000, threshold=None, nms=2, point_mask=None, max_keypoints = 4096):
    # Returns keypoints in shape (N, 3)
    #  sm = nms_score_map(score_map)

    sm = torch.tensor(score_map).unsqueeze(0).unsqueeze(0)
    #  mask1 = mask_local_max(sm).int()
    mask2 = mask_border(sm).int()
    mask3 = mask_max(sm, radius=nms).int()

    mask = mask2*mask3
    if point_mask is not None:
        mask *= point_mask

    sm *= mask
    sm = sm.numpy().squeeze()
    #  detections = score_map > thres
    if threshold is not None:
        locs = np.where(sm > threshold)
        if len(locs[0]) > max_keypoints:
            return score_map_to_kpts(score_map, top_n=max_keypoints, nms=nms, point_mask=point_mask)
        vals = sm[locs]
        val_pts = np.concatenate([np.array(locs).T, vals[:, None]], axis=1).T # Shape: (3, N)
        return val_pts.T
    else:
        locs = np.where(sm > 0)
        vals = sm[locs]
        val_pts = np.concatenate([np.array(locs).T, vals[:, None]], axis=1).T # Shape: (3, N)
        # sort
        sorted_pts = val_pts[:, np.argsort(-vals)]
        return sorted_pts.T[:top_n]
