import toml

def augment_args(args, config):
    # First, load defaults
    with open(config, "r") as f:
        cfg = toml.load(f)
    for k, v in cfg.items():
        if k not in args.__dict__ or args.__dict__[k] is None:
            args.__dict__[k] = v
    if "model" in args.__dict__ and args.model in args.__dict__ and "batch_size" in args.__dict__[args.model]:
        args.__dict__["batch_size"] = args.__dict__[args.model]["batch_size"]
