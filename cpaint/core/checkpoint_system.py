import os
import subprocess
import datetime

def init_checkpoint(checkpoint_path, name):
    metadata_path = os.path.join(checkpoint_path, name, "metadata")
    os.makedirs(metadata_path, exist_ok=True)
    subprocess.call(["git", "diff", ">", os.path.join(metadata_path, "gitdiff")])
    subprocess.call(["git", "show", "--oneline", "-s", ">", os.path.join(metadata_path, "gitlog")])
    with open(os.path.join(metadata_path, "datetime"), "w") as f:
        f.write(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
