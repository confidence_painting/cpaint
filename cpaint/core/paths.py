# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import shutil
import os
import re
import subprocess
import datetime
import sys

def get_painted_dir(output_dir, dataset_name, prefix):
    return os.path.join(output_dir, "painted", dataset_name, prefix)

def get_processed_paths(i, painted_dir):
    reproj_path = os.path.join(painted_dir, f"reproj_score_{i:05d}.exr")
    counts_path = os.path.join(painted_dir, f"data_counts_{i:05d}.exr")
    points_path = os.path.join(painted_dir, f"points_{i:05d}.exr")
    return reproj_path, counts_path, points_path

def get_detection_path(output_dir, name, area, i, legacy=True):
    heatmap = os.path.join(output_dir, "exported", name, f"{i:05d}.npy")
    if legacy:
        heatmap = os.path.join(output_dir, "exported", name, area, f"{i:05d}.exr")
    return {
        "heatmap": heatmap,
    }

def gen_checkpoint_path(output_dir, name, i):
    path = os.path.join(output_dir, "checkpoints", name, "models")
    return os.path.join(path, f"checkpoint{i:03d}.pth")

def latest_checkpoint_path(output_dir, name):
    checkpoint_dir_names = ["checkpoints", "checkpoints_phase1", "checkpoints_phase2"]
    for dir_name in checkpoint_dir_names:
        path = os.path.join(output_dir, dir_name, name, "models")
        if not os.path.exists(path):
            continue
        ckpts = [os.path.join(path, p) for p in os.listdir(path) if "checkpoint" in p]
        ckpts.sort(key=os.path.getctime)

        return ckpts[-1]
    raise Exception(f"Failed to find checkpoint in dir {output_dir}/{checkpoint_dir_names} with name {name}")

def loss_log_path(output_dir, name):
    return os.path.join(output_dir, "checkpoints", name)

def init_checkpoint(output_dir, name, config_path):
    metadata_path = os.path.join(output_dir, "checkpoints", name, "metadata")
    os.makedirs(metadata_path, exist_ok=True)
    shutil.copyfile(config_path, os.path.join(metadata_path, "config.toml"))
    os.system(f"git diff > {os.path.join(metadata_path, 'gitdiff')}")
    os.system(f"git show --oneline -s > {os.path.join(metadata_path, 'gitlog')}")
    with open(os.path.join(metadata_path, "meta"), "w") as f:
        date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        f.write(f"Run at: {date}\n")
        f.write(f"With args: {' '.join(sys.argv)}\n")

def get_config_file(output_dir, name):
    return os.path.join(output_dir, "checkpoints", name, "metadata", "config.toml")

def name_prefix(preprefix, i):
    return f"{preprefix}_{i:02d}"

def get_pr_out_path(output_dir, dataset_name, run_name):
    return os.path.join(output_dir, "pr", dataset_name, f"{run_name}.npz")
