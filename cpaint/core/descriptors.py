# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import cv2
import torch
import math
from kornia.feature import hardnet
import torch.nn.functional as F
import time

DISABLE_NONFREE = True

"""
def create_superpoint():
    import sys
    import os
    tp_path = os.path.join(os.path.dirname(__file__), '../../third_party')
    sys.path.append(tp_path)
    from third_party.superpoint.model import SuperPointFrontend
    from cpaint.models import prep_input
    from cpaint.core.image_ops import unpad_multiple
    weights_path = os.path.abspath(os.path.join(
        tp_path, "superpoint/superpoint_v1.pth"))

    # Load model
    fe = SuperPointFrontend(weights_path=weights_path,
                            nms_dist=4,
                            conf_thresh=0,
                            nn_thresh=0.7,
                            cuda=True)
    fe.net.eval()

    # Now wrap this descriptor
    def _compute(im, points):
        #  pts = np.array([(kpt.pt[1], kpt.pt[0]) for kpt in points])
        pts = points[:, :2].T
        # Pad image to correct size
        H, W = im.shape[:2]
        semi, coarse_desc = fe.net(im)

        data, old_size = prep_input(im, False, 8)
        semi, desc = fe.net.forward(data.to(device))
        heatmap = fe.net.semi_to_heatmap(semi, data.shape).unsqueeze(1)
        desc = fe.net.get_desc_at_pt(coarse_desc, pts, W, H)

        # get descriptors for points
        return desc.T

    descriptor = type('Superpoint', (object,), {})()
    descriptor.compute = _compute

    return descriptor
"""

def create_superpoint():
    from cpaint import models
    threshold = 0.007

    device = torch.device("cuda")
    # Init model
    model_cfg = models.MODELS['superpoint']
    model = model_cfg["net"]().to(device)
    model.load_default_state_dict()
    detector = models.model_to_descriptor(model, model_cfg["color_input"], model_cfg["input_size_multiple"])
    return detector

def create_hardnet():
    device = torch.device("cuda:0")
    model = hardnet.HardNet(pretrained=True)
    model.to(device)
    model.eval()
    ws = 32
    def _compute(im, pts):
        start_time = time.time()
        win = ws//2
        wx, wy = torch.meshgrid(torch.linspace(-win, ws-win, ws),
                                torch.linspace(-win, ws-win, ws))
        wx = wx.to(device)
        wy = wy.to(device)

        if len(im.shape) == 3:
            im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im = torch.tensor(im).float().to(device)
        H, W = im.shape[-2:]
        C = 1
        def extract_patch(image, pt):
            # image: (1, C, H, W)
            # pt: (2) in row col coords
            # First, construct the points
            # these have shape (self.ws, self.ws)
            nx = (wy + pt[1])/W*2 - 1
            ny = (wx + pt[0])/H*2 - 1
            points = torch.stack((nx, ny), dim=-1).view(1, 1, -1, 2)

            patch = F.grid_sample(image, points, mode="bilinear", align_corners=False)
            patch = patch.view(1, C, ws, ws)
            # normalize patch
            return (patch - patch.mean())/patch.std()

        # Run: Extract patches
        pts = torch.tensor(pts).float().to(device) # (N, 2), column, row
        if len(pts.shape) == 3:
            pts = pts.squeeze(0)
        patches = []
        for pt in pts:
            patches.append(extract_patch(im.reshape(1, 1, H, W), pt))
        print(len(patches))
        if len(patches) == 0:
            return pts, None
        patches = torch.stack(patches)
        print(f"Extract: {time.time()-start_time}")
        e = time.time()-start_time
        start_time = time.time()
        desc = model(patches.reshape(-1, 1, ws, ws)).cpu().numpy()
        print(f"Describe: {time.time()-start_time}")
        d = time.time()-start_time
        print((e+d)/len(patches))
        return pts, desc
    hn = type('HardNet', (object,), {})()
    hn.compute = _compute
    return hn


def combine_desc_det(detector, descriptor):
    combined = type('Combined Wrapper', (object,), {})()
    combined.detector = detector
    combined.descriptor = descriptor
    def _detectAndCompute(gray):
        pts = combined.detector.detect(gray)
        _, desc = combined.descriptor.compute(gray, pts)
        return pts, desc
    combined.detectAndCompute = _detectAndCompute
    combined.detect = detector.detect
    combined.compute = descriptor.compute
    combined.predict = detector.predict
    if hasattr(combined, 'threshold'):
        combined.threshold = detector.threshold
    return combined

def compute_orientations(image, radius):
    batch = image.view(-1, 1, image.shape[-2], image.shape[-1])
    B, _, H, W = batch.shape
    # generate kernel
    x, y, = torch.meshgrid(
        torch.linspace(-radius-0.5, radius+0.5, radius*2+1),
        torch.linspace(-radius-0.5, radius+0.5, radius*2+1)
    )
    x = x.view(1, 1, radius*2+1, radius*2+1).to(batch.device)
    y = y.view(1, 1, radius*2+1, radius*2+1).to(batch.device)
    m_10 = F.conv2d(batch, x, padding=radius)
    m_01 = F.conv2d(batch, y, padding=radius)
    #  m_00 = F.conv2d(image, torch.ones_like(x), pad=radius)
    angle = torch.atan2(m_01, m_10)
    angle[torch.isnan(angle)] = 0
    return angle.squeeze().cpu().numpy() * 180 / math.pi

def wrap_cv(cvdesc):
    def wrap():
        def _compute(im, pts):
            if len(im.shape) == 3:
                im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

            size = 32
            orientations = compute_orientations(torch.tensor(im).float()/255, size//2)
            # convert to keypoints
            kpts = []
            for pt in pts:
                kpts.append(cv2.KeyPoint(pt[1], pt[0], _size=size, _angle=orientations[int(pt[0]), int(pt[1])]))

            return cvdesc.compute(im, kpts)
        descriptor = type('CV Wrapper', (object,), {})()
        descriptor.compute = _compute
        return descriptor
    return wrap


DESCRIPTORS = {
    "orb": wrap_cv(cv2.ORB_create()),
    "superpoint": create_superpoint,
    "hardnet": create_hardnet,
}

if not DISABLE_NONFREE:
    DETECTORS["sift"] = cv2.xfeatures2d.SIFT_create
    DETECTORS["surf"] = cv2.xfeatures2d.SURF_create

# Assign norm functions
NORM_TYPES = {
    "orb": cv2.NORM_HAMMING,
    "brief": cv2.NORM_HAMMING,
    "hardnet": cv2.NORM_L2,
    "brisk": cv2.NORM_HAMMING,
    "sift": cv2.NORM_L2,
    "surf": cv2.NORM_L2,
    "superpoint": cv2.NORM_L2,
}
