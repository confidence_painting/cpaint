import torch
from torchvision import transforms
import numpy as np
from PIL import Image
import cv2
from cpaint.core import image_ops
import random
import torch.nn.functional as F
import torchvision.transforms.functional as TF

class Morpholgical:
    # for binary images valued 0 or 1
    def __init__(self, iterations):
        self.kernel = torch.ones(1, 1, iterations*2+1, iterations*2+1)
        self.iterations = iterations

    def dilate(self, images):
        images = images.view(-1, 1, images.shape[-2], images.shape[-1])
        l_max = F.max_pool2d(
                F.pad(images, [self.iterations] * 4, mode='constant', value=0.),
                self.iterations*2+1, stride=1
            )
        return l_max > 0

    def erode(self, images):
        images = images.view(-1, 1, images.shape[-2], images.shape[-1])
        l_max = -F.max_pool2d(
                F.pad(-images, [self.iterations] * 4, mode='constant', value=0.),
                self.iterations*2+1, stride=1
            )
        return l_max > 0
