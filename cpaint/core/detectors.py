# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import torch
import cv2
import numpy as np
import os
import skimage.feature
#  from cpaint.core.superpoint.util import nms_fast
import matplotlib.pyplot as plt
from cpaint.core import detector_util

# Brisk has a binary descriptor
def doublewrap(detector):
    """ Wraps a detector so it exports keypoints [[x, y, score]] """
    def create():

        def _detect(im, top_n=1000, threshold=None):
            """ Takes the keypoints and converts them to a score map """
            score_map = detector.predict(im)
            kpts = detector_util.score_map_to_kpts(score_map, top_n=top_n, threshold=threshold)
            return kpts

            #  bord = 4
            #  toremoveW = np.logical_or(kpts[0, :] < bord, kpts[0, :] >= (W-bord))
            #  toremoveH = np.logical_or(kpts[1, :] < bord, kpts[1, :] >= (H-bord))
            #  toremove = np.logical_or(toremoveW, toremoveH)
            #  return kpts[:, ~toremove]
        detector.detect = _detect

        return detector
    return create


def wrap_opencv(detector_cv, descriptor=None, max_kpts=1e6, root_kernel=False):
    """ Wraps an opencv detector so it exports a score map instead of keypoints """
    def create():
        detector = type('OpenCV Wrapper', (object,), {})()
        detector.detector = detector_cv
        if create.descriptor is None:
            create.descriptor = detector_cv
        detector.color_input = False

        def _predict(im):
            """ Takes the keypoints and converts them to a score map """
            kps = detector_cv.detect(im, None)
            score_map = np.zeros(im.shape[:2], np.float32)
            for kp in kps:
                score_map[int(kp.pt[1]+0.5), int(kp.pt[0]+0.5)] = kp.response
            return score_map

        def _detect(im):
            kpts = detector_cv.detect(im, None)
            l_pts = []
            for kp in kpts:
                l_pts.append([kp.pt[1], kp.pt[0], kp.response, kp.size])
            pts = np.array(l_pts)
            n = min(max_kpts, pts.shape[0]-1)
            threshold = -np.sort(-pts[:, 2])[n]
            pts = pts[np.where(pts[:, 2]>threshold), :]
            return pts.squeeze(0)

        def _detectAndCompute(im):
            kpts = detector_cv.detect(im, None)
            l_pts = []
            for kp in kpts:
                l_pts.append([kp.pt[1], kp.pt[0], kp.response, kp.size])
            pts = np.array(l_pts)
            threshold = -np.sort(-pts[:, 2])[min(max_kpts, len(l_pts)-1)]
            pts = pts[np.where(pts[:, 2]>threshold), :]
            kpts = [kpt for kpt in kpts if kpt.response > threshold]
            kpts, descs = create.descriptor.compute(im, kpts)
            if root_kernel:
                eps = 1e-7
                descs /= (descs.sum(axis=1, keepdims=True) + eps)
                descs = np.sqrt(descs)

            # Reconvert
            l_pts = []
            for kp in kpts:
                l_pts.append([kp.pt[1], kp.pt[0], kp.response, kp.size])
            pts = np.array(l_pts)
            return pts, descs
        detector.predict = _predict
        detector.detect = _detect
        detector.detectAndCompute = _detectAndCompute

        return detector
    create.descriptor = descriptor
    return create


def create_gftt():
    """ Creates the gftt corner detector """
    detector = type('GFTT Detector', (object,), {})()

    def _detect(img):
        if len(img.shape) == 3 and img.shape[2] == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gftt = cv2.goodFeaturesToTrack(img, 2000, 0.01, 10)
        gftt_corners = np.zeros(img.shape[:2], np.uint8)
        corners = np.squeeze(np.int0(gftt), axis=1)
        gftt_corners[corners[:, 1], corners[:, 0]] = 1
        return gftt_corners
    detector.detect = _detect
    detector.color_input = False

    return detector


def create_harris():
    """ Creates the harris corner detector """
    detector = type('Harris Detector', (object,), {})()

    def _predict(img):
        if len(img.shape) == 3 and img.shape[2] == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        harris = cv2.cornerHarris(img, 4, 3, 0.04)
        return harris
    detector.predict = _predict
    detector.color_input = False

    return detector


def create_superpoint():
    from cpaint import models
    threshold = 0.007

    device = torch.device("cuda")
    # Init model
    model_cfg = models.MODELS['superpoint']
    model = model_cfg["net"]().to(device)
    model.load_default_state_dict()
    detector = models.model_to_detector(model, model_cfg["color_input"], threshold, model_cfg["input_size_multiple"])
    return detector

def amalgamate(detectors):
    """ Returns an amalgamated detector
    Has an additional function called "predict" that returns a floating point
    score map with a confidence for each point
    """

    detector = type('Detector Wrapper', (object,), {})()

    def _predict(gray):
        feature_maps = None
        for detector in detectors:
            fts = detector.predict(gray).astype(np.float32)
            # Normalize the detections
            fts_norm = fts / max(np.sum(fts), 1)
            if feature_maps is None:
                feature_maps = fts_norm
            else:
                feature_maps += fts_norm
        return feature_maps

    #  detector.detect = _detect
    detector.predict = _predict
    detector.color_input = False

    return detector

# Signal source detectors

def create_dog(f1=3, f2=5):

    def _predict(gray):
        blur_big = cv2.GaussianBlur(gray, (f1, f1), 0)
        blur_smol = cv2.GaussianBlur(gray, (f2, f2), 0)

        dog = blur_big - blur_smol
        plt.imshow(dog)
        plt.show()
        return dog

    detector = type('Detector Wrapper', (object,), {})()

    #  detector.detect = _detect
    detector.predict = _predict
    detector.color_input = False

    return detector

def create_perlin(scale=10):
    # https://stackoverflow.com/questions/42147776/producing-2d-perlin-noise-with-numpy
    def perlin(x,y,seed=0):
        # permutation table
        a = max(np.max(x), np.max(y))+1
        p = np.arange(a, dtype=int)
        np.random.shuffle(p)
        p = np.stack([p,p]).flatten()
        # coordinates of the top-left
        xi = x.astype(int)
        yi = y.astype(int)
        # internal coordinates
        xf = x - xi
        yf = y - yi
        # fade factors
        u = fade(xf)
        v = fade(yf)
        # noise components
        n00 = gradient(p[p[xi]+yi],xf,yf)
        n01 = gradient(p[p[xi]+yi+1],xf,yf-1)
        n11 = gradient(p[p[xi+1]+yi+1],xf-1,yf-1)
        n10 = gradient(p[p[xi+1]+yi],xf-1,yf)
        # combine noises
        x1 = lerp(n00,n10,u)
        x2 = lerp(n01,n11,u) # FIX1: I was using n10 instead of n01
        return lerp(x1,x2,v) # FIX2: I also had to reverse x1 and x2 here

    def lerp(a,b,x):
        "linear interpolation"
        return a + x * (b-a)

    def fade(t):
        "6t^5 - 15t^4 + 10t^3"
        return 6 * t**5 - 15 * t**4 + 10 * t**3

    def gradient(h,x,y):
        "grad converts h to the right gradient vector and return the dot product with (x,y)"
        vectors = np.array([[0,1],[0,-1],[1,0],[-1,0]])
        g = vectors[h%4]
        return g[:,:,0] * x + g[:,:,1] * y

    def _predict(gray):
        gray = gray.squeeze()
        xi = np.linspace(0, scale, gray.shape[0])
        yi = np.linspace(0, scale, gray.shape[1])
        x, y = np.meshgrid(xi, yi)
        noise = perlin(x, y, seed=int(gray.mean()*100))
        return noise
    detector = type('Perlin', (object,), {})()
    #  detector.detect = _detect
    detector.predict = _predict
    detector.color_input = False
    return detector

def create_bw():
    def _predict(gray):
        return gray.squeeze()
    detector = type('Mirror Signal', (object,), {})()
    #  detector.detect = _detect
    detector.predict = _predict
    detector.color_input = False
    return detector



DETECTORS = {
    "harris": create_harris,
    #  "gftt": create_gftt,
    "sift": wrap_opencv(cv2.SIFT_create()),
    "rootsift": wrap_opencv(cv2.SIFT_create(), root_kernel=True),
    "orb": wrap_opencv(cv2.ORB_create()),
    #  "orb": wrap_opencv(cv2.ORB_create(nfeatures=1000)),
    #  "superpoint": create_superpoint,
    "dog": create_dog,
    "perlin": create_perlin,
    "bw": create_bw,
}

if hasattr(cv2, "xfeatures2d"):
    DETECTORS["fast+brief"] = wrap_opencv(cv2.FastFeatureDetector_create(), cv2.xfeatures2d.BriefDescriptorExtractor_create())
    DETECTORS["surf"] = wrap_opencv(cv2.xfeatures2d.SURF_create())
    DETECTORS["censure"] = wrap_opencv(cv2.xfeatures2d.StarDetector_create())

def create_all_detector():
    detectors = []
    for det_name in DETECTORS:
        if det_name == "all":
            continue
        detectors.append(DETECTORS[det_name]())
    return amalgamate(detectors)


#  DETECTORS["all"] = doublewrap(create_all_detector())
