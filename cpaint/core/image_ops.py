# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np
import cv2
import torch.nn.functional as F
import torch
from PIL import Image
import random
import scipy.ndimage
import math

KERNEL3 = np.ones((3, 3), np.float32)
KERNEL5 = np.ones((5, 5), np.float32)
SHARPEN = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])

def pad_to_square(image, pad_value):
    size = max(image.shape[:2])
    sq_shape = list(image.shape)
    sq_shape[:2] = (size, size)
    sqimg = np.ones(sq_shape, image.dtype) * pad_value
    (nh, nw) = image.shape[:2]
    oh, ow = (size-nh)//2, (size-nw)//2
    sqimg[oh:oh+nh, ow:ow+nw] = image
    return sqimg


def resize_with_crop(image, shape):
    #  scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    #  image = cv2.resize(image,
    #                     (int(scale*image.shape[0]), int(scale*image.shape[1])),
    #                     interp)
    (h, w) = image.shape[:2]
    (nh, nw) = shape[:2]
    oh, ow = (h-nh)//2, (w-nw)//2
    return image[oh:oh+nh, ow:ow+nw]


def scale_then_crop(image, shape, interp=cv2.INTER_LINEAR):
    #  image = cv2.resize(image, (max(shape), max(shape)), interp)
    scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    image = cv2.resize(image,
                       (int(scale*image.shape[1]), int(scale*image.shape[0])),
                       interp)

    image = resize_with_crop(image, shape)
    return image


def resize_with_pad(image, shape):
    out = np.zeros(shape, dtype=image.dtype)
    if image.shape[0] == shape[0]:
        assert(shape[1] >= image.shape[1])
        offset = (shape[1] - image.shape[1]) // 2
        out[:, offset:offset+image.shape[1]] = image
    elif image.shape[1] == shape[1]:
        assert(shape[0] >= image.shape[0])
        offset = (shape[0] - image.shape[0]) // 2
        out[offset:offset+image.shape[0], :] = image
    else:
        print("resize_with_pad(image, shape) must take in an image that matches\
the shape in at least one dimension")
        assert(False)
    return out


def scale_then_pad(image, shape):
    scale = max(shape[0]/image.shape[0], shape[1]/image.shape[1])
    image = cv2.resize(image, None, fx=scale, fy=scale,
                       interpolation=cv2.INTER_LINEAR)
    image = resize_with_pad(image, shape)
    return image


def draw_keypoints(binary_score_map, img):
    """ Draws keypoints from a binary score map onto the img without
    changing the original image
    """
    points = np.array(np.where(binary_score_map)).T
    kps = [cv2.KeyPoint(int(kp[1]+0.5), int(kp[0]+0.5), 1) for kp in points]
    ret = img.copy()
    ret = cv2.drawKeypoints(ret, kps, np.array([]))
    return ret

def noisy(noise_typ, image):
    """
    https://stackoverflow.com/questions/22937589/how-to-add-noise-gaussian-salt-and-pepper-etc-to-image-in-python-with-opencv
    """
    if len(image.shape) == 2:
        row, col = image.shape
        ch = 1
    elif len(image.shape) == 3:
        row, col, ch = image.shape

    if noise_typ == "gauss":
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        gauss = gauss.reshape(row, col, ch)
        noisy = image.reshape(row, col, ch) + gauss
        noisy = noisy.squeeze()
    elif noise_typ == "s&p":
        s_vs_p = 0.5
        amount = 0.004
        noisy = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                for i in image.shape[:2]]
        noisy[tuple(coords)] = 1

        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        if ch == 1:
            coords = [np.random.randint(0, i - 1, int(num_pepper))
                    for i in image.shape[:2]]
        else:
            coords = [np.random.randint(0, i - 1, int(num_pepper))
                    for i in image.shape]
        noisy[tuple(coords)] = 0
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
    elif noise_typ =="speckle":
        gauss = np.random.randn(row, col, ch)
        gauss = gauss.reshape(row, col, ch)
        noisy = image + image * gauss

    return noisy

def fast_interp(image, output_size, local_min=True):
    assert(image.shape[0] == image.shape[1])
    assert(output_size[0] == output_size[1])
    assert(image.shape[0] % output_size[0] == 0)
    input_size = image.shape[0]
    output_size = output_size[0]
    channels = 1 if len(image.shape) == 2 else image.shape[2]
    image = image.reshape(input_size, input_size, channels)
    bin_size = input_size // output_size
    small_image = image.reshape((output_size, bin_size,
                                 output_size, bin_size, channels))
    if local_min:
        small_image = small_image.min(3).min(1)
    else:
        small_image = small_image.max(3).max(1)
    return small_image.squeeze()

def batch_fast_interp(image, output_size, local_min=True):
    assert(image.shape[2] == image.shape[3])
    assert(output_size[2] == output_size[3])
    assert(image.shape[2] % output_size[2] == 0)
    input_size = image.shape[2]
    output_size = output_size[2]
    C = image.shape[1]
    N = image.shape[0]
    image = image.reshape(N, C, input_size, input_size)
    bin_size = input_size // output_size
    small_image = image.reshape((N, C, output_size, bin_size,
                                       output_size, bin_size))
    if local_min:
        small_image = small_image.min(5).values.min(3).values
    else:
        small_image = small_image.max(5).values.max(3).values
    return small_image


def random_motion_blur(img):
    ch = img.shape[2]
    max_kernel_size = 9
    size = np.random.choice(list(range(1, max_kernel_size+1, 2)))
    if size == 1:
        return img

    # Generate the kernel
    kernel_motion_blur = np.zeros((size, size))
    kernel_motion_blur[int((size-1)/2), :] = np.ones(size)
    kernel_motion_blur = kernel_motion_blur / size

    # Rotate the kernel
    M = cv2.getRotationMatrix2D((size/2, size/2), np.random.randint(0, 90), 1)
    rot_kernel = cv2.warpAffine(kernel_motion_blur, M, (size, size))
    if ch != 1:
        rot_kernel = np.repeat(rot_kernel.reshape(size, size, 1), ch, axis=2)

    # applying the kernel to the input image
    return scipy.ndimage.convolve(img.squeeze(), rot_kernel)

def get_padding_for_multiple(imshape, multiple):
    N, C, H, W = imshape
    # New size
    nH = math.ceil(H/multiple)*multiple
    nW = math.ceil(W/multiple)*multiple
    # Paddings
    tp = math.ceil((nH-H)/2)
    bp = math.floor((nH-H)/2)

    lp = math.ceil((nW-W)/2)
    rp = math.floor((nW-W)/2)
    return lp, rp, tp, bp

# pytorch
def unpad_multiple(images, old_shape, multiple):
    _, _, nH, nW = images.shape
    lp, rp, tp, bp = get_padding_for_multiple(old_shape, multiple)
    return images[:, :, tp:nH-bp, lp:nW-rp]


# pytorch
def pad_to_multiple(images, multiple, mode="reflect"):
    padding = get_padding_for_multiple(images.shape, multiple)
    padded = F.pad(images, padding, mode=mode)
    return padded
