# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#  - Each process reads these images and thresholds them using it's predefined threshold
#  - It then runs the painter using these images and retrieves the output
#  - Average output across each viewpoints' detections

import os
import cv2
import numpy as np
import torch
import torch.nn.functional as F

import argparse

from tqdm import tqdm

#  import matplotlib.pyplot as plt

from cpaint.core.detector_util import mask_max, mask_border
#  from cpaint import overlap_lib as painter
import pyoctree as painter
#  import pyoctree as painter
from cpaint import datasets
from cpaint.core import paths, config


def save_several(tensors, num, prefix):
    for i in range(num):
        im = tensors[i]
        if type(im) == torch.Tensor:
            im = im.numpy()
        name = f"debug_images/{prefix}_{i}.exr"
        cv2.imwrite(name, im.squeeze())


def sum_kernel(summant, radius):
    summant = torch.tensor(summant).float()
    batch = summant.view(1, 1, summant.shape[-2], summant.shape[-1])
    diameter = 2*radius + 1
    summed = (
        diameter ** 2 * F.avg_pool2d(
            F.pad(batch, [radius] * 4, mode='constant', value=0.),
            diameter, stride=1
        )
    ).squeeze()
    return summed.numpy()


def get_precision_recall(dataset, output_dir, run_name, octomap_dir, num_tests, num_radii_test):
    # num_tests: number of times to run painter. Resolution of graph
    # Load poses and get voxel size masks
    voxel_masks = []
    poses = []
    print("Loading depth images")
    for i, (rgb_path, depth_path, pose_path, basename) in tqdm(enumerate(dataset), total=len(dataset)):
        poses.append(torch.tensor(dataset.load_pose(i)).float())
        dep = dataset.load_depth_image(i)
        voxel_masks.append(dataset.get_voxel_mask(dep))

    # Load precomputation and collect a min/max range for thresholds
    min_v = 9999
    max_v = -9999
    precomp = []
    print("Loading heatmaps")
    for i in tqdm(range(len(dataset))):
        exp_paths = paths.get_detection_path(args.output_dir, run_name, dataset.name, i)
        heatmap = cv2.imread(exp_paths["heatmap"], -1)
        min_v = min(heatmap.min(), min_v)
        max_v = max(heatmap.max(), min_v)
        #  precomp.append(torch.tensor(heatmap))

        th_score = torch.tensor(heatmap).view(1, 1, heatmap.shape[-2], heatmap.shape[-1]).float()
        mask1 = mask_max(th_score, radius=args.maxpool_radius)
        mask2 = mask_border(th_score)

        th_pooled = mask1*mask2*th_score
        precomp.append(th_pooled.squeeze())
    print(f"Values lie in [{min_v}, {max_v}]")

    # Create thresholds
    thresholds = np.linspace(min_v-0.001, max_v, num_tests+1)[1:]

    # Iterate over thresholds
    inter_pr_res = 20
    total_accums = []
    total_counts = []
    total_point_accums = []
    total_point_counts = []
    num_dets = []

    true_pos = np.zeros((num_tests, inter_pr_res), dtype=np.int)
    false_pos = np.zeros((num_tests, inter_pr_res), dtype=np.int)
    true_neg = np.zeros((num_tests, inter_pr_res), dtype=np.int)
    false_neg = np.zeros((num_tests, inter_pr_res), dtype=np.int)
    print("Running test")
    for i, thres in tqdm(enumerate(thresholds)):
        # Threshold images
        thresholded = [(p > thres).float() for p in precomp]
        #  save_several(thresholded, 5, f"thresholded_{thres}")

        # Run the painter using these images and retrieves the output
        octomap_path = dataset.get_octomap_path(octomap_dir)
        output = [tensor.numpy() for tensor in painter.reproject_score_maps(
            thresholded, poses, octomap_path, dataset.get_calib())]
        accums = output[:len(output)//2]
        counts = output[len(output)//2:]

        #  Average output across each viewpoints' detections
        total_accum = 0
        total_count = 0
        num_det = 0
        total_point_accum = np.zeros((num_radii_test), np.int)
        total_point_count = np.zeros((num_radii_test), np.int)

        #  scores = []
        #  for (accum, count) in zip(accums, counts):
        #      count = np.where(count < 0.5, 1, count)
        #      score = accum/count
        #      score[np.isnan(score)] = 0
        #      scores.append(score)

        #  save_several(scores, 5, f"scores_{thres}")
        #  save_several(counts, 5, f"counts_{thres}")
        #  save_several(accums, 5, f"accums_{thres}")

        for _, (accum, count, points, voxel_mask) in enumerate(zip(accums, counts, thresholded, voxel_masks)):
            points_np = points.bool().numpy()
            mask = (count > 1) & voxel_mask

            count = np.where(count < 0.5, 1, count)
            score = accum/count
            score[np.isnan(score)] = 0

            num_det += (mask * points_np).sum()

            # Here, we find another PR curve according to how we threshold the
            # map probability and pass the difficulty of interpretting this
            # result further down
            total_count += (count * mask).sum()
            total_accum += (accum * mask).sum()

            for radius in range(num_radii_test):
                if radius != 0:
                    l_count = sum_kernel(count, radius)
                    l_accum = sum_kernel(accum, radius)
                else:
                    l_count = count
                    l_accum = accum
                # Sum over radius
                total_point_count[radius] += (l_count * mask * points_np).sum()
                total_point_accum[radius] += (l_accum * mask * points_np).sum()

        # What exactly does the avg score represent?
        # It is the probability that a point detected by the detector will be
        # detected in every other viewpoint
        total_counts.append(total_count)
        total_accums.append(total_accum)
        total_point_counts.append(total_point_count)
        total_point_accums.append(total_point_accum)
        num_dets.append(num_det/len(counts))
    return {
        "total_counts": total_counts,
        "total_accums": total_accums,
        "total_point_counts": total_point_counts,
        "total_point_accums": total_point_accums,
        "num_dets": num_dets,
        "true_pos": true_pos,
        "false_pos": false_pos,
        "true_neg": true_neg,
        "false_neg": false_neg,
        "thresholds": thresholds,
        "dataset_len": len(dataset)
    }


def main(args, dataset):
    assert(hasattr(args, "num_tests"))

    dat = get_precision_recall(
        dataset, args.output_dir, args.run_name, args.dataset_dir,
        args.num_tests, args.num_radii_test)

    res_path = paths.get_pr_out_path(args.output_dir, dataset.name, args.run_name)
    os.makedirs(os.path.dirname(res_path), exist_ok=True)
    with open(res_path, "wb") as f:
        np.savez(f, **dat)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("computer_config", type=str, help="Path to computer specific config")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument("run_name", type=str, help="Name of export run")

    parser.add_argument("--display", action="store_true",
                        help="Display the results and don't save them")
    parser.add_argument("--num_tests", type=int, default=10, help="Amount of times to run painter. Expensive, but gives additional accuracy to PR curve.")
    parser.add_argument("--maxpool_radius", type=int, default=4, help="Radius of maxpool for determining ground truth")
    parser.add_argument("--num_radii_test", type=int, default=8, help="Number of radii to check that points were contained within")
    args = parser.parse_args()
    config.augment_args(args, args.computer_config)

    for dataset in tqdm(datasets.DatasetList(args.dataset_dir, args.dataset)):
        print(f"Running PR test on: {dataset.name}")
        main(args, dataset)
