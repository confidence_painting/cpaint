import os
import cv2
import argparse
import torch
import numpy as np

from tqdm import tqdm

from cpaint import datasets
from cpaint.core import paths, config
from cpaint import train_util
import torch.distributed as dist
from cpaint.models import MODELS, model_to_detector

def parse_args():
    parser = argparse.ArgumentParser(description='Confidence Painter Trainer')
    parser.add_argument("computer_config", type=str, help="Path to computer specific config")
    parser.add_argument("net_config", type=str, help="Path to net config")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Training set")
    parser.add_argument('run_name', type=str, help='Name of export if num_runs = 1, else it is the preprefix')
    parser.add_argument('--num_runs', type=int, default=1, help='Number of runs to export on, if there are multiple.')
    parser.add_argument('--checkpoint', type=str, default="", help="Optional checkpoint to use if run doesn't exist")

    args = parser.parse_args()
    config.augment_args(args, args.computer_config)
    config.augment_args(args, args.net_config)
    args.cache_dataset = False
    return args

def main(args, run_name):
    args.train_descriptors = False
    if args.checkpoint == "":
        args.checkpoint = paths.latest_checkpoint_path(args.output_dir, run_name)

    print(f"Loading checkpoint from {args.checkpoint}")
    rank = 0
    world_size = 1

    # Init model
    os.environ["MASTER_ADDR"] = args.MASTER_ADDR
    os.environ["MASTER_PORT"] = args.MASTER_PORT
    dist.init_process_group("gloo", rank=rank, world_size=world_size)
    model, _, _ = train_util.load_model(args, rank)
    model.eval()

    model_cfg = MODELS[args.model]
    detector = model_to_detector(model, model_cfg["color_input"])

    for dataset in datasets.DatasetList(args.dataset_dir, args.dataset):

        exp_paths = paths.get_detection_path(args.output_dir, run_name, dataset.name, 0)
        out_dir = os.path.dirname(exp_paths["heatmap"])
        print(f"Exporting to: {out_dir}")
        os.makedirs(out_dir, exist_ok=True)

        for i, (rgb_path, depth_path, pose_path, basename) in tqdm(enumerate(dataset), total=len(dataset)):
            im = dataset.load_rgb_image(i)
            if not detector.color_input:
                im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
            result = {
                "heatmap": detector.predict(im),
            }

            to_export = ['heatmap']
            exp_paths = paths.get_detection_path(args.output_dir, run_name, dataset.name, i)
            for item in to_export:
                extension = exp_paths[item].split(os.extsep)[-1]
                if extension == "exr":
                    cv2.imwrite(exp_paths[item], result[item])
                elif extension == "npy":
                    with open(exp_paths[item], "wb") as f:
                        np.save(f, result[item])
                else:
                    raise Exception(f"Extension {extension} has no save method")

    dist.destroy_process_group()

if __name__ == "__main__":
    args = parse_args()
    if args.num_runs > 1:
        for run_num in range(1, args.num_runs):
            run_name = paths.name_prefix(args.run_name, run_num)
            main(args, run_name)
    else:
        main(args, args.run_name)
