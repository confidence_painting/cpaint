# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import cv2
import numpy as np
import torch

from pathlib import Path
import argparse

from tqdm import tqdm
from icecream import ic

import matplotlib.pyplot as plt

from cpaint.core import image_ops
from cpaint import datasets
from cpaint.core import paths

def paint_maps(dataset, painted_path, octomap_dir, detector, threshold=None, cudatree=True):
    # ==================== Grab paths and load results =====================
    poses = []
    augmented_result_maps = []
    os.makedirs(painted_path, exist_ok=True)
    result_shape = dataset.get_largest_square()
    dataset.desired_size = result_shape

    for i, (rgb_path, depth_path, pose_path, basename) in enumerate(dataset):
        im = dataset.load_rgb_image(i)
        if not detector.color_input:
            im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
            #  gray = image_ops.scale_then_crop(gray, result_shape)
        score_map = torch.tensor(detector.predict(im))
        if threshold is not None:
            score_map = (score_map > threshold).to(torch.float32)
        poses.append(torch.tensor(dataset.load_pose(i)).float())
        augmented_result_maps.append(score_map.cpu().float())
        del score_map

        #  plt.imshow(score_map)
        #  plt.show()

    # Run dat C++ code

    if cudatree:
        import pyoctree as painter
    else:
        from cpaint import overlap_lib as painter
    octomap_path = dataset.get_octomap_path(octomap_dir)
    output = [tensor.numpy() for tensor in painter.reproject_score_maps(
        augmented_result_maps, poses, octomap_path, dataset.get_calib())]
    new_score_maps = output[:len(output)//2]
    new_total_counts = output[len(output)//2:]

    # Convert score maps to keypoints
    print(f"Saving to {painted_path}")
    for i, new_score_map in enumerate(new_score_maps):
        new_score_map = image_ops.scale_then_crop(
                new_score_map, result_shape)
        data_counts = image_ops.scale_then_crop(
                new_total_counts[i], result_shape)

        reproj_path, counts_path, _ = paths.get_processed_paths(i, painted_path)

        cv2.imwrite(reproj_path, new_score_map)
        cv2.imwrite(counts_path, data_counts)
    del output, augmented_result_maps, poses


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("prefix", type=str, default="test", help="Saving prefix for output. Becomes gt_source argument")

    parser.add_argument("--threshold", default=None,
                        help="Threshold applied")
    parser.add_argument("--display", action="store_true",
                        help="Display the results and don't save them")
    args = parser.parse_args()


    for dataset in tqdm(datasets.DatasetList(args.dataset_dir, args.dataset)):
        print(f"Painting: {dataset.name}")
        paint_maps(dataset, paths.get_painted_dir(args.output_dir, dataset.name, args.prefix), args.dataset_dir, detector, args.threshold)
        print(points.shape)
