import numpy as np
import matplotlib.pyplot as plt


import argparse
from tqdm import tqdm
from cpaint.core import paths


def graph(args, dataset_name, run_names):
    fig, ax = plt.subplots()
    min_dets = 9999
    max_dets = -9999
    max_repeatability = -9999
    for run_name in run_names:
        res_path = paths.get_pr_out_path(args.output_dir, dataset_name, run_name)
        print(res_path)
        f = open(res_path, "rb")
        dat = np.load(f)

        i = 0
        for j in range(4):
            num_dets = dat["num_dets"][i:]
            #  total_counts = dat["total_point_counts"][i:, j]
            total_counts = dat["total_counts"][i:]
            total_accums = dat["total_point_accums"][i:, j] - num_dets
            #  else:
            #      total_counts = dat["total_counts"][i:]
            #      total_accums = dat["total_accums"][i:] - num_dets
            #      if j > 0:
            #          break
            avg_scores = total_accums/total_counts
            thresholds = dat["thresholds"][i:]

            # shapes: (num_tests, inter_pr_res)
            #  (num_tests, inter_pr_res) = precision.shape

            #  p = precision.mean(axis=1)
            # integrate precision over recall

            # This mean is meaningful because it is the expected value of the recall
            # The probability that the detection will show up in the viewpoint
            # What is the probability that a point that shows up in x% of viewpoints will show up in this one?
            # It should just be linear. x%. Therefore, it is basically just asking what the 50% thing is
            # Is this test meaningful?

            # Should I be plotting repeatability vs number of detections?
            # What is repeatability? Is it the probability that a detection will be repeated?
            # If I take the mean over all frames, certain samples will be overrepresented.
            #  r = recall.sum(axis=1) / inter_pr_res
            #  ax.scatter(r, p)
            #  ax.plot(r, p, label=run_name)
            a = np.stack((total_counts/1100, total_accums/1100, num_dets), axis=0).T.astype(np.int)
            print("Counts", "Accum", "Num", "Thres")
            print(a)
            ax.scatter(num_dets, avg_scores)
            ax.plot(num_dets, avg_scores, label=f"{run_name} radius {j}")
            min_dets = min(np.min(num_dets), min_dets)
            max_dets = max(np.max(num_dets), max_dets)
            max_repeatability = max(np.max(avg_scores), max_repeatability)
    ax.set_xlim(max_dets+50, min_dets-50)
    ax.set_xlabel("Mean #detections")
    ax.set_ylabel("Repeatability")
    ax.set_ylim(0, max_repeatability+0.01)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", type=str,
                        help="Dataset to graph on")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("run_names", type=str, nargs="+", metavar="N", help="Name of export run")

    parser.add_argument("--display", action="store_true",
                        help="Display the results and don't save them")
    args = parser.parse_args()

    graph(args, args.dataset, args.run_names)
