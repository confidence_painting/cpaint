import cv2
import toml
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from cpaint.core import paths, config
from cpaint import datasets

def parse_args():
    parser = argparse.ArgumentParser(description='Confidence Painter Trainer')
    parser.add_argument("computer_config", type=str, help="Path to computer specific config")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Training set")
    parser.add_argument("preprefix", type=str, help="Preprefix to name run.")

    args = parser.parse_args()
    config.augment_args(args, args.computer_config)
    return args

# This thing needs to handle an entire run
class DataIter:
    def __init__(self, args):
        self.dataset = datasets.DatasetList(args.dataset_dir, args.dataset).next()
        self.args = args

    def __getitem__(self, key):
        run_num = key["run_num"]
        index = key["index"]

        prefix = paths.name_prefix(self.args.preprefix, run_num)
        painted_dir = paths.get_painted_dir(args.output_dir, self.dataset.name, prefix)
        # Get paths
        reproj_p, counts_p, _ = paths.get_processed_paths(index, painted_dir)
        exp_paths = paths.get_detection_path(self.args.output_dir, prefix, self.dataset.name, index)
        heatmap_p = exp_paths["heatmap"]

        # Load images
        rgb = self.dataset.load_rgb_image(index)
        heatmap = cv2.imread(heatmap_p, -1)
        reproj = cv2.imread(reproj_p, -1)
        counts = cv2.imread(counts_p, -1)
        if reproj is None:
            print(f"Could not load counts from {reproj_p}")
        if counts is None:
            print(f"Could not load counts from {counts_p}")

        # Simple op to convert counts + reproj -> score map
        counts = np.where(counts < 0.5, 1, counts)
        painted = reproj/counts
        painted[np.isnan(painted)] = 0

        # Load metadata
        with open(paths.get_config_file(self.args.output_dir, prefix), "r") as f:
            config = toml.load(f)
            if "iteration" in config:
                config = config["iteration"]
        # Grab desired_num_detections
        var_range = np.array(config["varieties"])
        dnd_range = np.repeat(var_range, config["iters"])

        return {
            "Input": rgb,
            "Network Output": heatmap,
            "Map": painted,
            "desired_num_detections": dnd_range[run_num],
        }

def graph(data_iter, image_inds, run_nums, tags):
    indices = []
    for i in image_inds:
        for r in run_nums:
            indices.append({"run_num": r, "index": i})

    imsize = 1.5
    w = len(indices)
    h = len(tags)

    tm = 0.10
    sm = 0.03
    fig = plt.figure(figsize=(w*imsize*(1-2*tm), h*imsize*(1-2*sm)), dpi=150)
    gs1 = gridspec.GridSpec(h, w)
    gs1.update(wspace=0, hspace=0, left=sm, right=1-sm, top=1-tm, bottom=tm)
    print("Graphing")
    for i, ind in enumerate(indices):
        dat = data_iter[ind]
        for j, tag in enumerate(tags):
            f = fig.add_subplot(gs1[j*w+i])
            f.set_xticks([])
            f.set_yticks([])
            f.imshow(dat[tag])
            if i == 0:
                f.set_ylabel(tag)
            if j == 0:
                f.set_title(dat["desired_num_detections"])
    return fig

if __name__ == "__main__":
    args = parse_args()
    data_iter = DataIter(args)
    fig = graph(data_iter, range(1), range(1, 9), ["Network Output", "Map"])
    plt.show()
