# Extracts:
# Keypoints: N x (x, y, score, scale, orientation)
# Descriptors: N x D 

# Expected directory structure:
# dataset_dir/FM-Bench/Dataset/{area}
# dataset_dir/d2-net
# dataset_dir/2020VisualLocalization/Aachen-Day-Night

import os
import cv2
import toml
import torch
import argparse
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import time

from cpaint.test_datasets import DATASETS
from cpaint import models
from cpaint.core import detectors, config, descriptors, image_ops
from cpaint.models import multiscale

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("net_config", type=str, help="Config for model")
    parser.add_argument("--dataset_dir", type=str, help="Check source for details on directory structure")
    parser.add_argument("--threshold", type=float, help="The threshold")
    parser.add_argument("--descriptor", default='', choices=list(descriptors.DESCRIPTORS.keys()), help="The descriptor to be used for the detector")
    parser.add_argument("--checkpoint", type=str, default="", help="Checkpoint for run, if one is to be used")
    parser.add_argument("--display", action="store_true", default=False, help="Display results for debugging")
    parser.add_argument("--multiscale", action="store_true", default=False, help="Extract keypoints with different sizes")

    # Parse
    args = parser.parse_args()
    config.augment_args(args, args.net_config)
    print(f"Threshold: {args.threshold}")
    if args.threshold is not None:
        args.threshold = float(args.threshold)

    if args.model in detectors.DETECTORS:
        detector = detectors.DETECTORS[args.model]()
    else:
        # Init torch
        device = torch.device("cuda")
        kwargs = {'num_workers': 4, 'pin_memory': True}

        # Init model
        model_cfg = models.MODELS[args.model]
        loss_fn = model_cfg["loss"]
        model = model_cfg["net"]().to(device)
        if (args.checkpoint != ''):
            ckpt = torch.load(args.checkpoint)
            if "state_dict" in ckpt:
                state_dict = ckpt["state_dict"]
            else:
                state_dict = ckpt
            state_dict = {".".join(k.split(".")[1:]): v for k, v in state_dict.items()}
            model.load_state_dict(state_dict)
        elif args.use_pretrained:
            model.load_default_state_dict()
        model.eval()

        #  feat_ext = models.model_to_feature_extractor(model, model_cfg["color_input"], args.threshold, model_cfg["input_size_multiple"])
        feat_ext = models.model_to_detector(model, model_cfg["color_input"], args.threshold, model_cfg["input_size_multiple"], use_subpixel=False)
        #  detector = detectors.DETECTORS['superpoint']()
        #  detector = detectors.doublewrap(detector)()

    if args.descriptor != '':
        desc = descriptors.DESCRIPTORS[args.descriptor]()
        feat_ext = descriptors.combine_desc_det(feat_ext, desc)
        #  det = models.model_to_detector(model, model_cfg["color_input"], model_cfg["input_size_multiple"])
        #  det = detectors.doublewrap(det)()
        #  desc = models.model_to_descriptor(model, model_cfg["color_input"], model_cfg["input_size_multiple"])


    # Extract
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print("Cannot open camera")
        exit()
    with torch.no_grad():
        while True:
            ret, img = cap.read()
            #  img = cv2.resize(img, (960, 540))
            img = image_ops.scale_then_crop(img, (480, 640))
            #  img = cv2.resize(img, (480, 270))
            if not ret:
                print("Can't receive frame (stream end?). Exiting ...")
                break
            #  if args.multiscale:
            #      pts = multiscale.detect_multiscale(feat_ext, img, color_input=model_cfg["color_input"], base_size=5)
            #      # Doesn't check if it lots keypoints
            #      _, desc = feat_ext.compute(img, pts)
            #  else:
            #      pts, desc = feat_ext.detectAndCompute(img)
            start_time = time.time()
            #  pts = feat_ext.detectAndCompute(img)
            pts, desc = feat_ext.detectAndCompute(img)
            #  pts = feat_ext.detect(img)
            print(f"Full: {time.time()-start_time}")

            key_pts = [cv2.KeyPoint(p[1], p[0], _size=5) for p in pts]
            kkpts_img = img.copy()
            kkpts_img = cv2.drawKeypoints(img, key_pts, kkpts_img, color=(0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            cv2.imshow("Keypoints", kkpts_img)
            #  cv2.imshow("Keypoints", img)
            cv2.waitKey(1)
