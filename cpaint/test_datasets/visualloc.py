# keypoints always come in (x, y, score, scale, orientation)
#
#  visual localisation: 
#    npz with the same name as the original file with the method appended, like image.jpg.d2net with the following
#      keypoints: (N, 2) x, y. x right, y down in pixels
#      descriptors: (N, D) L2 normalized descriptors

import os
import errno
import numpy as np
import cv2
from torch.utils.data import Dataset

class VisualLoc(Dataset):
    def __init__(self, dataset_dir, tag):
        self.dataset_path = os.path.join(dataset_dir, "2020VisualLocalization", "Aachen-Day-Night")
        self.tag = tag
        self.img_dir = os.path.join(self.dataset_path, "images", "images_upright")
        subdirs = ["query", "db", "sequences/gopro3_undistorted",
                   "query/night/milestone", "query/night/nexus5x", "query/night/nexus5x_additional_night",
                   "query/day/milestone", "query/day/nexus5x", "query/day/nexus4"]
        for i in range(1, 9):
            p = os.path.join("sequences", "nexus4_sequences", f"sequence_{i}")
            subdirs.append(p)

        self.images = []
        for s in subdirs:
            i = os.listdir(os.path.join(self.img_dir, s))[0]
            imgs = [os.path.join(self.img_dir, s, p)
                    for p in os.listdir(os.path.join(self.img_dir, s))
                    if len(os.path.splitext(p)) == 2 and os.path.splitext(p)[1] in [".jpg", ".png"]]
            self.images.extend(imgs)

    @property
    def output_dir(self):
        return os.path.dirname(self.get_image_paths(0))

    def __len__(self):
        return len(self.images)

    def get_image_paths(self, idx):
        return self.images[idx]

    def __getitem__(self, i):
        im = cv2.imread(self.get_image_paths(i))
        if im is None:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.get_image_paths(i))
        return im

    def save_kps(self, keypoints, descriptors, i):
        # Keypoints: N x (x, y, score, scale, orientation)
        # Descriptors: N x D 
        path = self.get_image_paths(i) + "." + self.tag
        np.savez(open(path, "wb"),
                 keypoints = keypoints[:, :2],
                 descriptors = descriptors)

    def close(self):
        pass
