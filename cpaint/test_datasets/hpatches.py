# keypoints always come in N x (x, y, score, scale, orientation)
#
#  hpatches (d2net): 
#    file name: os.path.join(dataset_path, seq_name, '%d.%s.%s' % (im_idx, extension, method)). extension = ppm by default
#    np.savez(open(outpath,'wb'), 
#      keypoints = xys[idxs], (Nx3) x right, y down in pixels, s = scale
#      descriptors = desc[idxs], (NxD)
#      scores = scores[idxs]) (N)
import os
import errno
import numpy as np
import cv2
from torch.utils.data import Dataset

class HPatches(Dataset):
    def __init__(self, dataset_dir, tag):
        self.d2net_path = os.path.join(dataset_dir, "d2-net")
        self.tag = tag
        with open(os.path.join(self.d2net_path, "image_list_hpatches_sequences.txt"), "r") as f:
            self.paths = [l.strip() for l in f.readlines()]

    @property
    def output_dir(self):
        return os.path.dirname(self.get_image_paths(0))

    def __len__(self):
        return len(self.paths)

    def get_image_paths(self, idx):
        return os.path.join(self.d2net_path, self.paths[idx])

    def __getitem__(self, i):
        im = cv2.imread(self.get_image_paths(i))
        if im is None:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.get_image_paths(i))
        return im

    def save_kps(self, keypoints, descriptors, i):
        # Keypoints: N x (x, y, score, scale, orientation)
        # Descriptors: N x D 
        path = self.get_image_paths(i) + "." + self.tag
        np.savez(open(path, "wb"),
                 keypoints = keypoints[:, (0, 1, 3)],
                 descriptors = descriptors,
                 scores = keypoints[:, 2])

    def close(self):
        pass
