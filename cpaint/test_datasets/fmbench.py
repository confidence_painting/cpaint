# keypoints always come in N x (x, y, score, scale, orientation)
#
# formats:
#  fm bench: 
#    read format: each image is either a left(l) or right(r) image in a pair and the output that is saved needs to indicate this in the file name. 
#                 the idx is the index of this pair
#    file format: transpose, np.tofile for use in matlab with the following binary files in a feature folder
#    {idx:04d}_{l or r}.keypoints (N, 4) for keypoints (x, y, scale, orientation) and 
#    {idx:04d}_{l or r}.descriptors (N, D) for descriptors, D = dimensionality of descriptors (128)
import os
import numpy as np
import cv2
import errno
from torch.utils.data import Dataset

AREAS = ["KITTI", "Tanks_and_Temples", "TUM", "CPC"]

class FMBench(Dataset):
    def __init__(self, dataset_dir, tag, area):
        self.dataset_path = os.path.join(dataset_dir, "FM-Bench", "Dataset", area)
        self.dest_dir = os.path.join(dataset_dir, "FM-Bench", "output", tag, "Features", area)
        os.makedirs(self.dest_dir, exist_ok=True)
        with open(os.path.join(self.dataset_path, "pairs_with_gt.txt"), "r") as f:
            self.pairs_with_gt = [[int(i) for i in l.split(' ')[:2]] for l in f.readlines()]
        with open(os.path.join(self.dataset_path, "pairs_which_dataset.txt"), "r") as f:
            self.pairs_with_which_dataset = [l.strip() for l in f.readlines()]
        # The way the indexing works is that odd indexes are the left images, even are right images

    @property
    def output_dir(self):
        return self.dest_dir

    def __len__(self):
        return len(self.pairs_with_gt)*2

    def get_image_paths(self, idx):
        i, j = self.pairs_with_gt[idx//2]
        l = os.path.join(self.dataset_path, self.pairs_with_which_dataset[idx//2], "Images", f"{i:08d}.jpg")
        r = os.path.join(self.dataset_path, self.pairs_with_which_dataset[idx//2], "Images", f"{j:08d}.jpg")
        return r if idx % 2 == 1 else l

    def __getitem__(self, i):
        im = cv2.imread(self.get_image_paths(i))
        if im is None:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.get_image_paths(i))
        return im

    def save_kps(self, keypoints, descriptors, i):
        # Keypoints: N x (x, y, score, scale, orientation)
        # Descriptors: N x D 
        idx = i // 2 + 1 # matlab offset
        side = "r" if i % 2 == 1 else "l"

        k_path = os.path.join(self.dest_dir, f"{idx:04d}_{side}.keypoints")
        d_path = os.path.join(self.dest_dir, f"{idx:04d}_{side}.descriptors")
        with open(k_path, "wb") as f:
            kpts = keypoints[:, (0, 1, 3, 4)]
            kpts[:, 0] += 1
            kpts[:, 1] += 1
            np.array(kpts.shape).astype(np.int32).tofile(f)
            kpts.astype(np.float32).tofile(f)
        with open(d_path, "wb") as f:
            np.array(descriptors.shape).astype(np.int32).tofile(f)
            descriptors.astype(np.float32).tofile(f)

    def close(self):
        pass
