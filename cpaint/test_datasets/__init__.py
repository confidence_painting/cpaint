# Provides a common interface for the test datasets. Pass dataset_dir, tag
from cpaint.test_datasets import fmbench
from cpaint.test_datasets.hpatches import HPatches
from cpaint.test_datasets.visualloc import VisualLoc

DATASETS = {
    "hpatches": HPatches,
    "visualloc": VisualLoc,

}

# Function is necessary for proper scope capture of area var
def wrap_fmbench(area):
    def fn(dataset_dir, tag):
        return fmbench.FMBench(dataset_dir, tag, area)
    return fn

for area in fmbench.AREAS:
    DATASETS[area] = wrap_fmbench(area)

