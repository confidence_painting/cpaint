# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import torch
import argparse
import numpy as np
import torch.optim as optim
import cv2
import random
import json
import copy

from cpaint.core import paths, config, timeout
from cpaint import datasets
from cpaint.models import MODELS
from cpaint.models.multi_task import MultiTaskLoss
from cpaint.find_threshold import find_painter_output_threshold

from torch.utils.tensorboard import SummaryWriter
from cpaint.core.image_transformer import Morpholgical

# Parallel
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP

# coloring maps
import torchvision.transforms.functional as TF
from matplotlib import cm


colormap = cm.get_cmap('magma')
ERODER = Morpholgical(2)


@timeout.timeout(30)
def flush(writer):
    writer.flush()


class Multiply(torch.nn.Module):
    def __init__(self, val):
        super(Multiply, self).__init__()
        alpha = torch.nn.Parameter(torch.ones((1,))*val)
        self.register_parameter("alpha", alpha)

    def forward(self, x):
        return x*self.alpha


def load_model(args, device):
    # Init model
    model_cfg = MODELS[args.model]
    model = model_cfg["net"]().to(device)

    # Load multi task loss if needed and pass it as arg
    run_kwargs = {}
    params = []
    if args.train_descriptors and args.use_multi_loss:
        multi_loss = MultiTaskLoss(3).to(device)
        params += multi_loss.parameters()
        run_kwargs["multi_loss"] = multi_loss

    if args.train_descriptors and args.desc_loss_fn == "similarity_loss":
        alpha = Multiply(1).to(device)
        params += alpha.parameters()
        run_kwargs["alpha"] = alpha

    # load state dicts for all items
    optim_sd = None
    if (args.checkpoint != ''):
        ckpt = torch.load(args.checkpoint)
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
            # wrapper for DDP
            if len([key for key in state_dict.keys() if "module" in key]) > 0:
                state_dict = {".".join(k.split(".")[1:]): v for k, v in state_dict.items()}
        else:
            state_dict = ckpt
        model.load_state_dict(state_dict)
        if "optim_state_dict" in ckpt:
            optim_sd = ckpt["optim_state_dict"]
        if "multi_loss_state_dict" in ckpt:
            multi_loss.load_state_dict(ckpt["multi_loss_state_dict"])
    elif args.use_pretrained:
        model.load_default_state_dict()

    model = DDP(model, device_ids=[device], find_unused_parameters=True)
    #  m = lambda x: x
    #  m.module = model
    #  model = m
    params += list(model.parameters())

    optimizer = optim.Adam(params, lr=args.lr, betas=(0.9, 0.999))

    if optim_sd is not None:
        optimizer.load_state_dict(optim_sd)

    return model, optimizer, run_kwargs


def train(rank, args, gt_source, run_name, threshold, world_size, train_iter, test_iter):
    # Init multiprocess control
    os.environ["MASTER_ADDR"] = args.MASTER_ADDR
    os.environ["MASTER_PORT"] = args.MASTER_PORT
    dist.init_process_group("gloo", rank=rank, world_size=world_size)

    # Init torch
    torch.manual_seed(args.seed)
    device = rank

    model, optimizer, run_kwargs = load_model(args, device)

    # Init dataset
    model_cfg = MODELS[args.model]
    loss_fn = model_cfg["loss"]
    args.scaling_steps = model.module.scaling_steps
    args.gt_source = gt_source
    args.binary_target = model_cfg["binary_target"]
    args.threshold = threshold
    args.color_input = model_cfg["color_input"]
    args.input_size_multiple = model_cfg["input_size_multiple"]
    args.cpu_max_pool = False

    ds_list = [d for d in datasets.TrainDatasetList(args.dataset, args)]
    dataset = torch.utils.data.ConcatDataset(ds_list)

    train_size = len(dataset)//100*99
    train_dataset, test_dataset = torch.utils.data.random_split(
        dataset, [train_size, len(dataset)-train_size])

    #  kwargs = {'num_workers': args.num_workers, 'pin_memory': True}
    kwargs = {'num_workers': args.num_workers, 'pin_memory': True}
    multi = 2 if args.train_descriptors else 1
    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size*world_size//multi,
        shuffle=True, drop_last=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=args.batch_size*world_size//multi,
        shuffle=False, **kwargs)

    # Init config system
    paths.init_checkpoint(args.output_dir, run_name, args.net_config)
    if rank == 0:
        writer = SummaryWriter(paths.loss_log_path(args.output_dir, run_name))
    else:
        writer = None

    if rank == 0 and not args.display:
        standardize_size(test_loader)
        test_iter(args, model, loss_fn, device, test_loader, 1, run_name, writer, **run_kwargs)

    # Make sure path to checkpoints exists
    path = paths.gen_checkpoint_path(args.output_dir, run_name, 0)
    os.makedirs(os.path.dirname(path), exist_ok=True)
    # Manage save frequency (significant bottleneck)
    epoch_per_checkpoint = args.epochs // args.num_checkpoints
    for epoch in range(1, args.epochs + 1):
        random_size(args.desired_size, train_loader)
        train_iter(args, model, loss_fn, device, train_loader, optimizer, epoch, run_name, writer, **run_kwargs)
        if rank == 0 and epoch % args.test_interval:
            standardize_size(test_loader)
            test_iter(args, model, loss_fn, device, test_loader, epoch+1, run_name, writer, **run_kwargs)

        checkpoint_num = epoch // epoch_per_checkpoint
        do_save = epoch % epoch_per_checkpoint == 0
        if rank == 0 and do_save:
            path = paths.gen_checkpoint_path(args.output_dir, run_name, checkpoint_num)
            print(f"Saving checkpoint to: {path}")
            torch.save({
                "state_dict": model.state_dict(),
                "optim_state_dict": optimizer.state_dict(),
            }, path)
            flush(writer)

    dist.destroy_process_group()


def parse_args():
    parser = argparse.ArgumentParser(description='Confidence Painter Trainer')
    parser.add_argument("computer_config", type=str, help="Path to computer specific config")
    parser.add_argument("net_config", type=str, help="Path to net config")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Training set")
    parser.add_argument('gt_source', type=str,
                        help='Type of source for the GT. Check datasets/__init__.py')
    parser.add_argument('run_name', type=str,
                        help='Name of run')
    parser.add_argument('--display', action='store_true', default=False,
                        help='Display debugging view')
    parser.add_argument('--test_interval', type=int, default=20,
                        help='Interval between test iterations')
    parser.add_argument('--continue', action='store_true', default=False,
                        help="Don't initialize metadata")
    parser.add_argument('--threshold', type=float, default=0.07,
                        help='Threshold for ground truth')
    parser.add_argument('--checkpoint', type=str, default='',
                        help='checkpoint to start from')
    parser.add_argument('--world_size', default=None, help="Number of gpus")
    parser.add_argument('--desc_loss_fn', default=None, help="Desc loss function type")

    args = parser.parse_args()
    config.augment_args(args, args.computer_config)
    config.augment_args(args, args.net_config)
    if args.display:
        args.cache_dataset = False

    # If using run config
    if args.gt_source[-4:] == "json":
        with open(args.gt_source, "r") as f:
            args.gt_source = json.load(f)
        args.dataset = "json"
        # Find the thresholds for each dataset
        if "num_detections" in args.gt_source[0]:
            for area_js in args.gt_source:
                largs = copy.deepcopy(args)
                largs.dataset = area_js["area"]
                prefix = paths.name_prefix(area_js["prefix"], area_js["run_num"])
                output_threshold = find_painter_output_threshold(largs, area_js["num_detections"], prefix)
                print(f"Found threshold: {output_threshold} for area {area_js['area']}")
                area_js["threshold"] = output_threshold

    return args


def loss_w_mask(x, target, loss_fn, mask=None):
    loss = loss_fn(x, target)
    if mask is None:
        mask = torch.ones_like(loss)
    loss = (loss * mask).sum() / (mask.sum() + 1e-10)
    return loss


def color_tensor(tens):
    N, _, H, W = tens.shape
    tens_np = tens.detach().cpu().numpy()
    tens_list = []
    for i in range(N):
        normed = tens_np[i].squeeze()
        vmin = normed.min()
        vmax = normed.max()
        normed = (normed-vmin)/(vmax-vmin)
        colored = colormap(normed)[:, :, :3]
        colored = TF.to_tensor(colored)
        tens_list.append(colored)
    return torch.stack(tens_list, 0).view(N, 3, H, W)


def draw_target(target, data):
    col = (color_tensor(data).numpy().squeeze(0)*255).astype(np.uint8)
    col = np.rollaxis(col, 0, 3).copy()
    #  col = cv2.cvtColor(col, cv2.COLOR_BGR2RGB)
    kpts = np.array(np.where(target.numpy().squeeze())).T
    for kp in kpts:
        col = cv2.circle(col, (kp[1], kp[0]), 3, (0, 255, 0), 1)
    return col


def zero_data_mask(data, mask, painted):
    global ERODER
    zero_data = (data == 0).float()
    zero_data = ERODER.erode(zero_data).float()
    zero_data = ERODER.dilate(zero_data).float()
    painted *= 1-zero_data
    mask = (mask + zero_data >= 1).float()
    return mask, painted


def random_size(desired_size, loader):
    if type(desired_size[0]) == list:
        # is multiresolution
        size_index = random.randint(0, len(desired_size)-1)
        for ds in loader.dataset.dataset.datasets:
            if hasattr(ds, "size_index"):
                ds.size_index = size_index
            elif hasattr(ds.ds, "size_index"):
                ds.ds.size_index = size_index


def standardize_size(loader):
    for ds in loader.dataset.dataset.datasets:
        if hasattr(ds, "size_to_largest"):
            ds.size_to_largest()
        else:
            ds.ds.size_to_largest()
