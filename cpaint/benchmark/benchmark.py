# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import matplotlib.pyplot as plt
import numpy as np
import argparse
import torch  # sorry bro
import precompute_pairs
import cv2
from tqdm import tqdm
from cpaint import datasets
from cpaint.core import image_ops
from cpaint import overlap_lib

from cpaint.core.superpoint.util import nms_fast


DISPLAY = False


def benchmark(dataset, detector, output_dir, top_k, use_nms):
    all_detections = []
    depth_paths, pose_paths = [], []
    det_res_dir = os.path.join(output_dir, "detector_out",
                               detector, dataset.name)
    if use_nms:
        top_k = 1000
    num_taken = 0
    for i, (_, depth_path, pose_path, basename) in enumerate(dataset):
        # load result and convert back to matrix
        if detector == "degenerate":
            detections = np.ones(image_ops.IMAGE_SIZE, dtype=np.int16)
        else:
            detections = np.zeros(image_ops.IMAGE_SIZE, dtype=np.int16)

            fname = os.path.join(det_res_dir, f"{basename}.csv")
            pts = np.genfromtxt(fname, delimiter=",").reshape(-1, 3)

            if use_nms:
                out, out_inds = nms_fast(pts.T, image_ops.IMAGE_SIZE[1],
                                         image_ops.IMAGE_SIZE[0], 4)
                pts = out.T

            # Threshold detections
            pts = pts[(-pts[:, 2]).argsort(), :]
            num_take = min(pts.shape[0], top_k)
            # round
            pts = (pts[:num_take] + 0.5)[:, :2].astype(np.int)
            detections[pts[:, 0], pts[:, 1]] = 1

            num_taken += num_take
        all_detections.append(torch.tensor(detections))
        pose_paths.append(pose_path)
        depth_paths.append(depth_path)
    print(f"Average taken {num_taken/len(dataset)} from total possible {top_k}")

    pair_map = precompute_pairs.retrieve_pair_map(
        output_dir, dataset.name) > 0.1
    num_pairs_compared = np.sum(np.triu(pair_map, 1))
    pair_map = torch.Tensor(pair_map).short()

    if DISPLAY:
        fig = plt.figure(figsize=(4, 2))
        for i, (rgb_path1, _, _, _) in enumerate(dataset):
            for j, (rgb_path2, _, _, _) in enumerate(dataset):
                fig.clf()
                rgb1 = cv2.imread(rgb_path1)
                rgb2 = cv2.imread(rgb_path2)
                fig.add_subplot(1, 2, 1)
                plt.imshow(rgb1)
                fig.add_subplot(1, 2, 2)
                plt.imshow(rgb2)
                plt.title("Pos" if pair_map[i, j] else "Neg")
                plt.pause(0.1)

    histogram = overlap_lib.detection_accuracy_histogram(
            all_detections, pair_map, depth_paths, pose_paths, 10,
            dataset.name, dataset.calib_path)
    return histogram.cpu().numpy().astype(np.float) / num_pairs_compared


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("detector", type=str,
                        help="Detector to use to export detections")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Sequence dataset to test on", default="scannet")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")
    parser.add_argument("--use_nms", action="store_true", help="Use nms.")

    args = parser.parse_args()

    if args.dataset == "all":
        dataset_names = datasets.DATASET_LIST
    else:
        dataset_names = [args.dataset]

    for dataset in tqdm(datasets.DatasetList(args.dataset_dir, args.dataset)):
        histogram = benchmark(dataset, args.detector, args.output_dir, 200,
                              args.use_nms)
        label_nms = "_nms" if args.use_nms else ""
        outdir = os.path.join(args.output_dir, "histograms",
                              f"{args.detector}{label_nms}")
        fname = os.path.join(
                outdir, f"{dataset.name}_histogram.csv")
        os.makedirs(os.path.dirname(fname), exist_ok=True)
        print(f"Saving output to '{fname}'")
        np.savetxt(fname, histogram, delimiter=",")
