# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import cv2
import argparse
from cpaint import datasets
from cpaint.core import image_ops
from cpaint.core.detectors import DETECTORS
from tqdm import tqdm
import numpy as np


def export(dataset, detector, output_dir):
    for i, (rgb_path, depth_path, pose_path, basename) in enumerate(dataset):
        # Run detection
        im = cv2.imread(rgb_path, cv2.COLOR_BGR2GRAY)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im = image_ops.scale_then_crop(im, image_ops.IMAGE_SIZE)

        detections = detector.detect(im)
        fname = os.path.join(output_dir, f"{basename}.csv")
        np.savetxt(fname, detections, delimiter=",")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("detector", type=str, choices=list(DETECTORS.keys()),
                        help="Detector to use to export detections")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")

    args = parser.parse_args()

    for dataset in tqdm(datasets.DatasetList(args.dataset_dir, args.dataset)):
        detector_dir = os.path.join(
                args.output_dir, "detector_out", args.detector, dataset.name)
        os.makedirs(detector_dir, exist_ok=True)
        export(dataset, DETECTORS[args.detector](), detector_dir)
