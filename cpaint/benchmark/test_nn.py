# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This is test code fo the writing of the function in CPP
import cv2
import math
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize, linewidth=sys.maxsize)

query = np.array(
    [[0, 0, 0, 1, 0, 0],
     [0, 0, 0, 0, 0, 0],
     [1, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0],
     [1, 0, 0, 0, 0, 1]], np.float64)

cand = np.array(
    [[1, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 1, 0],
     [0, 0, 0, 0, 0, 0],
     [1, 0, 0, 0, 0, 0]], np.float64)


def gen_filter(radius):
    filt = np.zeros((2*radius+1, 2*radius+1), np.float64)
    centerx, centery = radius, radius
    for i in range(filt.shape[0]):
        for j in range(filt.shape[1]):
            dist = math.sqrt((i-centery)**2 + (j-centerx)**2)
            if dist > radius-0.5 and dist < radius+0.5:
                filt[i, j] = 1
    return filt


filters = [gen_filter(i) for i in range(1, 4)]


# Offset distance by +1
def nearest_neighbor(query, cand):
    outputs = [np.where(query == 0, 255, query)]
    for i, filt in enumerate(filters):
        dist = cv2.filter2D(query, -1, filt)
        dist = np.where(dist == 0, 255, 1)*(i+1)
        outputs.append(dist)
    print(outputs)

    output = np.ones_like(outputs[0])*255
    for out in outputs:
        output = np.minimum(output, out)
    print(cand*output)
    return cand*output


nearest_neighbor(query, cand)
