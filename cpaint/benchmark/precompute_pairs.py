# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import cv2
import argparse
from tqdm import tqdm
import numpy as np

from cpaint import overlap_lib
from cpaint import datasets
from cpaint.core import image_ops

import torch
from multiprocessing import Pool


def retrieve_pair_map(output_dir, dataset_name):
    fname = os.path.join(
            output_dir, "pair_maps", f"{dataset_name}.exr")
    if os.path.exists(fname):
        return cv2.imread(fname, -1)
    else:
        print(f"Failed to find: {fname}")


def process_dataset(args):
    dataset, precomp_dir = args
    depth_tensors = []
    pose_paths = []
    poses = []
    print("Loading")
    for i, (_, depth_path, pose_path, _) in enumerate(dataset):
        # To handle multi season datasets
        if i > dataset.repeat_length:
            break
        im = dataset.load_depth_image(i) # assumed depth factor of 1
        dep_tens = torch.tensor(im).float()
        depth_tensors.append(dep_tens)
        poses.append(torch.tensor(dataset.load_pose(i)))

    pair_matrix = overlap_lib.compute_valid_masks(
            depth_tensors, poses, dataset.get_calib()) \
        .numpy() \
        .astype(np.float32)
    print(pair_matrix.shape)

    # Save result to output dir
    out_f = os.path.join(precomp_dir, f"{dataset.name}.exr")
    os.makedirs(os.path.dirname(out_f), exist_ok=True)
    print(f"Saving: {dataset.name}")
    cv2.imwrite(out_f, pair_matrix)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        type=str, help="Sequence dataset to test on",
                        default="scannet")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")

    parser.add_argument("--num_threads", type=int,
                        help="Number of threads to use.", default=20)

    args = parser.parse_args()
    precomp_dir = os.path.join(args.output_dir, "pair_maps")
    os.makedirs(precomp_dir, exist_ok=True)
    #  p = Pool(args.num_threads)

    #  pool_args = []
    for dataset in tqdm(datasets.DatasetList(args.dataset_dir, args.dataset)):
        #  pool_args.append((dataset, precomp_dir))
        try:
            process_dataset((dataset, precomp_dir))
        except Exception as e:
            print(f"Failed: {dataset.name}. Exception: {e}")
    #  p.map(process_dataset, pool_args)
