# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import cv2
import torch
import numpy as np
import matplotlib.pyplot as plt

from cpaint import models
from cpaint.models import loss_util
from cpaint import train_util
import cpaint.homography_pair_test as hpt
import cpaint.core.detector_util as det


#  import torch.multiprocessing as mp
torch.autograd.set_detect_anomaly(True)


def train_iter(args, model, loss_fn, device, train_loader, optimizer, epoch, run_name, writer, multi_loss=None, **kwargs):
    torch.cuda.empty_cache()
    model.train()
    desc_loss_fn = loss_util.LOSSES[args.desc_loss_fn]
    for batch_idx, batch in enumerate(train_loader):

        optimizer.zero_grad()
        (data1, data2, painted1, painted2, target1, target2, mask1, mask2, matches) = [b.to(device).float() for b in batch]
        N, C, H, W = data1.shape

        data = torch.cat((data1, data2), dim=0)
        result = model(data)
        heatmap = result["heatmap"]
        desc = result["raw_desc"]
        heatmap1 = heatmap[:N]
        heatmap2 = heatmap[N:]
        desc1 = desc[:N]
        desc2 = desc[N:2*N]

        _, D, gh, gw = desc1.shape
        assert (H//(2**model.module.scaling_steps) == gh)

        if args.display:
            #  for i in range(data1.shape[0]):
            for i in range(1):
                print(f"Num pos: {(matches[i] == 1).sum()}, num neg: {(matches[i] == -1).sum()}")
                fig = plt.figure(figsize=(4.5, 4.5), dpi=600)

                f = fig.add_subplot(2, 5, 1)
                f.set_title("Heatmap")
                f.imshow(heatmap1[i].detach().cpu().squeeze().numpy())

                f = fig.add_subplot(2, 5, 2)
                target_np = target1[i].detach().cpu()
                drawn = train_util.draw_target(target_np, data1[i].detach().cpu().unsqueeze(0))
                f.set_title("Target")

                f.imshow(drawn)

                f = fig.add_subplot(2, 5, 3)
                im = data1[i].detach().cpu().numpy().squeeze()
                if len(im.shape) == 3:
                    im = np.rollaxis(im, 0, 3)
                #  im = im.astype(np.uint8)
                f.set_title("Im")
                f.imshow((im).squeeze())

                f = fig.add_subplot(2, 5, 4)
                h = painted1[i].detach().cpu().squeeze()
                f.set_title("Map")
                f.imshow(h.squeeze())

                f = fig.add_subplot(2, 5, 5)
                h = mask1[i].detach().cpu().squeeze()
                f.imshow(h.squeeze())

                # =========================

                f = fig.add_subplot(2, 5, 6)
                f.imshow(heatmap2[i].detach().cpu().squeeze().numpy())

                f = fig.add_subplot(2, 5, 7)
                target_np = target2[i].detach().cpu().squeeze()
                drawn = train_util.draw_target(target_np, data2[i].detach().cpu().unsqueeze(0))
                f.imshow(drawn)

                f = fig.add_subplot(2, 5, 8)
                im = data2[i].detach().cpu().numpy().squeeze()
                if len(im.shape) == 3:
                    im = np.rollaxis(im, 0, 3)
                #  im = im.astype(np.uint8)
                f.imshow((im).squeeze())

                f = fig.add_subplot(2, 5, 9)
                h = painted2[i].detach().cpu().squeeze()
                f.imshow(h.squeeze())

                f = fig.add_subplot(2, 5, 10)
                h = mask2[i].detach().cpu().squeeze()
                f.imshow(h.squeeze())

                #  ip1 = 255*F.interpolate(data1, (gh, gw))[i].data.cpu()
                #  ip2 = 255*F.interpolate(data2, (gh, gw))[i].data.cpu()
                ip1 = 255*data1[i].data.cpu()
                ip2 = 255*data2[i].data.cpu()
                stacked = loss_util.draw_matches(ip1, ip2, np.array(matches[i].data.cpu()))
                fig = plt.figure(figsize=(4.5, 4.5), dpi=600)
                f = fig.add_subplot(1, 1, 1)
                f.imshow(stacked/np.max(stacked))
                plt.show()

        p, n = desc_loss_fn(desc1, desc2, matches, **kwargs)
        pos_desc_loss = args.lambda_p * p
        neg_desc_loss = args.lambda_n * n

        det_loss = train_util.loss_w_mask(heatmap1, target1, loss_fn, mask=mask1) + \
            train_util.loss_w_mask(heatmap2, target2, loss_fn, mask=mask2)
        det_loss *= args.lambda_d

        if multi_loss is not None:
            loss = multi_loss(pos_desc_loss + neg_desc_loss, det_loss)
        else:
            loss = pos_desc_loss + neg_desc_loss + det_loss
        if torch.isnan(loss):
            continue
        loss.backward()
        optimizer.step()

        if device == 0:
            writer.add_scalar("train/loss", loss, epoch*len(train_loader)+batch_idx)
            writer.add_scalar("train/pos_desc_loss", pos_desc_loss, epoch*len(train_loader)+batch_idx)
            writer.add_scalar("train/neg_desc_loss", neg_desc_loss, epoch*len(train_loader)+batch_idx)
            writer.add_scalar("train/det_loss", det_loss, epoch*len(train_loader)+batch_idx)
            if args.use_multi_loss:
                writer.add_scalar("multiloss/desc_weight", multi_loss.weight(0), epoch*len(train_loader)+batch_idx)
                writer.add_scalar("multiloss/detector_weight", multi_loss.weight(1), epoch*len(train_loader)+batch_idx)
                writer.add_scalar("multiloss/desc_bias", multi_loss.get_log_var(0), epoch*len(train_loader)+batch_idx)
                writer.add_scalar("multiloss/detector_bias", multi_loss.get_log_var(1), epoch*len(train_loader)+batch_idx)

        if batch_idx % args.log_interval == 0 and device == 0:
            print(f'Train Epoch: {epoch} \t[{batch_idx * N}/{len(train_loader.dataset)} ({100. * batch_idx / len(train_loader):.0f}%)]\tDet Loss: {det_loss.item()}\t Pos Desc Loss: {pos_desc_loss.item()}\t Neg Desc Loss: {neg_desc_loss.item()}')


def extract(target, dense_desc, threshold):
    # pts: N x 2
    # desc: N x D
    D = dense_desc.shape[1]
    H, W = target.shape
    kpts = np.array(np.where(target)).T  # 2 x N
    pts = torch.from_numpy(kpts).unsqueeze(0)

    # Get descriptors
    if pts.shape[1] == 0:
        sampled = torch.zeros(1, 0, D)
    else:
        sampled = models.sample_descriptors(dense_desc.unsqueeze(0), H, W, pts[:, :, :2])
    pts = pts.squeeze(0)[:, :2]
    desc = sampled.detach().float().cpu().numpy().squeeze(0)

    return pts, desc


def test_iter(args, model, loss_fn, device, test_loader, epoch, run_name, writer, multi_loss=None, **kwargs):
    threshold = 0.02

    model.eval()

    desc_loss_fn = loss_util.LOSSES[args.desc_loss_fn]
    K = test_loader.dataset.dataset.datasets[0].K
    norm_type = cv2.NORM_L2

    visuals, maps, outputs, targets = [], [], [], [], []
    test_loss = 0
    test_det_loss = 0
    test_desc_loss = 0
    with torch.no_grad():
        for batch_idx, batch in enumerate(test_loader):
            # Run model
            (data1, data2, painted1, painted2, target1, target2, mask1, mask2, matches) = [b.to(device).float() for b in batch]

            N, C, H, W = data1.shape

            data = torch.cat((data1, data2), dim=0)
            result = model(data)
            heatmap = result["heatmap"]
            desc = result["raw_desc"]
            heatmap1 = heatmap[:N]
            heatmap2 = heatmap[N:]
            desc1 = desc[:N]
            desc2 = desc[N:2*N]

            _, D, gh, gw = desc1.shape

            # Track losses

            p, n = desc_loss_fn(desc1, desc2, matches, **kwargs)
            p *= args.lambda_p
            n *= args.lambda_n
            desc_loss = p + n

            det_loss = train_util.loss_w_mask(heatmap1, target1, loss_fn, mask=mask1) + \
                train_util.loss_w_mask(heatmap2, target2, loss_fn, mask=mask2)
            det_loss *= args.lambda_d

            if multi_loss is not None:
                test_loss += multi_loss(p, n, test_det_loss)
            else:
                test_loss += p + n + det_loss

            test_det_loss += det_loss
            test_desc_loss += desc_loss

            # Match images from data1 to data2
            target1_c = np.array(target1.data.cpu().squeeze(1))
            target2_c = np.array(target2.data.cpu().squeeze(1))
            # Generate visuals
            for i in range(1):
                pts1, d1 = extract(target1_c[i], desc1[i], threshold)
                pts2, d2 = extract(target2_c[i], desc2[i], threshold)
                im1 = cv2.cvtColor(np.array(255*data1[i].data.cpu()).astype(np.uint8).squeeze(), cv2.COLOR_GRAY2BGR)
                im2 = cv2.cvtColor(np.array(255*data2[i].data.cpu()).astype(np.uint8).squeeze(), cv2.COLOR_GRAY2BGR)
                visuals.append(hpt.visualize_matches(d1, d2, pts1, pts2, im1, im2, K, norm_type, crossCheck=True))
                maps.append(train_util.color_tensor(painted1[i].view(1, 1, H, W)))
                hp = heatmap1[i].view(1, 1, H, W)
                mask = det.mask_border(hp)
                outputs.append(train_util.color_tensor(hp*mask))

                #  images.append(data1[i])
                drawn1 = train_util.draw_target(target1[i].cpu(), data1[i].detach().cpu().unsqueeze(0)).squeeze()
                targets.append(drawn1)

    visuals = np.stack(visuals, axis=0)
    #  images = torch.stack(images, dim=0)
    maps = torch.cat(maps, dim=0)
    outputs = torch.cat(outputs, dim=0)
    targets = np.stack(targets, axis=0)

    writer.add_images("matches", visuals, dataformats='NHWC', global_step=epoch)
    #  writer.add_images("data", images, dataformats='NCHW', global_step=epoch)
    writer.add_images("map", maps, dataformats='NCHW', global_step=epoch)
    writer.add_images("output", outputs, dataformats='NCHW', global_step=epoch)
    writer.add_images("label", targets, dataformats='NHWC', global_step=epoch)

    writer.add_scalar("test/det_loss", test_det_loss, epoch)
    writer.add_scalar("test/desc_loss", test_desc_loss, epoch)
    writer.add_scalar("test/loss", test_loss, epoch)


if __name__ == '__main__':
    args = train_util.parse_args()
    args.train_descriptors = True

    train_util.train(0, args, args.gt_source, args.run_name, args.threshold, args.world_size, train_iter, test_iter)
    #  mp.spawn(train_util.train, args=
    #          (args, args.gt_source, args.run_name, args.threshold, args.world_size, train_iter, test_iter),
    #          nprocs=args.world_size, join=True)
