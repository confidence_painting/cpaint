import torch
import numpy as np
import torch.nn.functional as F
import itertools
import random

# Debug tools
from icecream import ic
import matplotlib.pyplot as plt
import cv2
from math import log, sqrt

np.set_printoptions(linewidth=580)

def intersection_difference(t1, t2):
    # Credit to Olivier on https://stackoverflow.com/questions/55110047/finding-non-intersection-of-two-pytorch-tensors
    combined = torch.cat((t1, t2))
    uniques, counts = combined.unique(return_counts=True)
    difference = uniques[counts == 1]
    intersection = uniques[counts > 1]
    return intersection, difference

def img_to_pts(img):
    i, j = torch.where(img)
    index = img[i, j]
    pts = torch.stack([i, j, index.long()], 0)
    #  Step 6: Assert uniqueness of these new points (numpy.unique along axis)
    _, inverse = torch.unique(pts[2, :], return_inverse=True)
    inds = torch.unique(inverse)
    return pts[:, inds]

def align_inds(inds1, inds2):
    # relies on the inds being sorted
    N = len(inds1)
    M = len(inds2)
    left = torch.zeros(N).long()
    right = torch.zeros(M).long()
    n = 0
    left_off = 0
    for i in range(N):
        for j in range(left_off, M):
            if inds1[i] < inds2[j]:
                break
            if inds1[i] == inds2[j]:
                left_off = j+1
                left[n] = i
                right[n] = j
                n += 1
                break
    return left[:n], right[:n]

def create_pairs(points1_im, points2_im, gw, gh, radius=4):
    # points1_im : (gw, gh)
    # points2_im : (gw, gh)

    indexed_pts1 = img_to_pts(points1_im)
    indexed_pts2 = img_to_pts(points2_im)

    # first, sort
    N = indexed_pts1.shape[1]
    M = indexed_pts2.shape[1]

    indexed_pts1 = indexed_pts1[:, torch.argsort(indexed_pts1[2, :])]
    indexed_pts2 = indexed_pts2[:, torch.argsort(indexed_pts2[2, :])]

    # Then align
    inds1 = indexed_pts1[2, :]
    inds2 = indexed_pts2[2, :]

    l, r = align_inds(inds1, inds2)
    L = len(l)
    aligned_pts1 = indexed_pts1[:2, l] # 2 x L
    aligned_pts2 = indexed_pts2[:2, r] # 2 x L

    # The resulting aligned points are pairs of points that are aligned

    # All matches are negative by default
    matches = torch.ones((gw*gh, gw*gh)).float()*-1

    # Mask out matches based on areas that are not being used
    mask1_inds, = torch.where(points1_im.flatten() != 0)
    mask2_inds, = torch.where(points2_im.flatten() != 0)
    matches[mask1_inds, :] = 0
    matches[:, mask2_inds] = 0

    # okay, so sometimes a point should project to multiple points on image 2 but it doesn't because of sparsity
    # For the positive pairs, this is not really that big of a deal
    # For the selection of negative pairs, we need to make sure that we don't create bad ones because of this sparsity
    # We don't do any of this because it doesn't seem to help

    # for each point, the negatives are the points that are a certain distance from it's corresponding point
    # the positives are the points that are a certain distance within it's corresponding point
    #  dist2 = torch.norm(aligned_pts2.view(2, L, 1).float() - aligned_pts2.view(2, 1, L).float(), p=2, dim=0)
    #  local_clusters2 = dist2 < radius

    """
    all_inds = torch.arange(0, L, 1).to(dist2.device)
    for i in range(L):
        cluster2, = torch.where(local_clusters2[i])
        # Now we find the points we can create negative pairs with
        # Because of how the indices are aligned, all points in the
        # cluster are ones we cannot form negative pairs with
        _, avail_inds = intersection_difference(all_inds, cluster2)
        if len(avail_inds) == 0:
            continue

        ind1 = (aligned_pts1[0, avail_inds] * gw + aligned_pts1[1, avail_inds]).long().view(-1, 1)
        ind2 = (aligned_pts2[0, i] * gw + aligned_pts2[1, i]).long().view(-1, 1)

        ind_pairs = list(zip(*list(itertools.product(ind1, ind2))))
        matches[ind_pairs[0], ind_pairs[1]] = -1
    """

    # convert row, col to index into descriptors for the positive pairs
    # 0 = i (row), 1 = j (col)
    ind1 = (aligned_pts1[0, :] * gw + aligned_pts1[1, :]).long()
    ind2 = (aligned_pts2[0, :] * gw + aligned_pts2[1, :]).long()

    # Add positives
    matches[ind1, ind2] = 1

    return matches

def draw_matches(im_pts1, im_pts2, matches):
    if len(im_pts1.shape) == 3:
        C, H, W = im_pts1.shape
    else:
        H, W = im_pts1.shape
        C = 1

    gh = gw = int(sqrt(matches.shape[0]))
    s = 8*5
    S = (s * gw) // W

    stacked = np.concatenate((im_pts1.squeeze(), im_pts2.squeeze()), axis=1).reshape(H, 2*W, C)
    if C == 1:
        stacked = np.repeat(stacked, 3, axis=2)
    stacked = cv2.resize(stacked, (2*S*W, S*H), interpolation=cv2.INTER_NEAREST)
    lines_im = np.zeros((S*H, 2*S*W, 3), np.uint8)
    intensity = int(min(np.max(stacked), 255))

    # Draw negative match
    num_neg = len(np.where(matches==-1)[0])
    for n, (i, j) in enumerate(zip(*np.where(matches==-1))):

        x1 = i % gw
        y1 = i // gw
        x2 = j % gw + gw
        y2 = j // gw

        ox = (3*n) % s
        oy = (5*n + 7) % s

        cv2.circle(
                lines_im,
                (x1*s+ox, y1*s+oy),
                0,
                (0, intensity, intensity))
        cv2.circle(
                lines_im,
                (x2*s+ox, y2*s+oy),
                0,
                (intensity, 0, intensity))

        if random.random() < 15/num_neg:
            cv2.line(lines_im, (x1*s+ox, y1*s+oy), (x2*s+ox, y2*s+oy), (intensity, 0, 0))

    # Draw positive match
    for n, (i, j) in enumerate(np.array(np.where(matches==1)).T):

        x1 = i % gw
        y1 = i // gw
        x2 = j % gw + gw
        y2 = j // gw

        ox = (3*n) % s
        oy = (5*n + 7) % s

        lines_im = cv2.circle(
                lines_im,
                (x1*s+ox, y1*s+oy),
                3,
                (intensity, 0, intensity))
        lines_im = cv2.circle(
                lines_im,
                (x2*s+ox, y2*s+oy),
                3,
                (0, intensity, intensity))

        lines_im = cv2.line(
                lines_im,
                (x1*s+ox, y1*s+oy),
                (x2*s+ox, y2*s+oy),
                (0, intensity, 0))

    return stacked.astype(np.uint16) + lines_im.astype(np.uint16)

def pre_loss(desc1, desc2, matches, dist_type):
    # desc1, desc2: (batch_size, num_desc, desc_size) (all of them) assumed normalized
    # matches: (batch_size, num_desc, num_desc), 1 = pos, 0 = nothing, -1 = neg
    N, D, gw, gh = desc1.shape

    flat_d1 = desc1.permute(0, 2, 3, 1).reshape(N, -1, D).contiguous()
    flat_d2 = desc2.permute(0, 2, 3, 1).reshape(N, -1, D).contiguous()

    # Use cosine distance because it is more memory efficient
    # This is just squared euclidean distance
    if dist_type == "cosine":
        dist = torch.matmul(flat_d1, flat_d2.transpose(1, 2))
    elif dist_type == "approx_l2":
        d = F.relu(1-torch.matmul(flat_d1, flat_d2.transpose(1, 2)))
        dist = torch.sqrt(2*d + 1e-8)
    norm = (matches != 0).sum()
    norm = torch.max(norm, torch.ones_like(norm))
    return matches, dist, norm


def hard_constrative_loss(desc1, desc2, matches, mp=0.4, mn=1, **kwargs):
    # desc1, desc2: (batch_size, num_desc, desc_size) (all of them) assumed normalized
    # matches: (batch_size, num_desc, num_desc), 1 = pos, 0 = nothing, -1 = neg
    matches, dist, norm = pre_loss(desc1, desc2, matches, "approx_l2")

    pos_loss = F.relu(dist*(matches==1) - mp)
    dist_multi = (matches == -1).float()
    dist_multi[matches != -1] = 10

    # For each descriptor
    # Search for the negative pair that is closest
    neg_dist1 = torch.min(dist*dist_multi, dim=1).values  # (batch_size, num_desc)
    neg_dist2 = torch.min(dist*dist_multi, dim=2).values  # (batch_size, num_desc)

    neg_correspondances = torch.min(neg_dist1, neg_dist2)
    neg_loss = F.relu(mn - neg_correspondances)

    return pos_loss.sum()/norm, neg_loss.sum()/norm


def hinge_loss(desc1, desc2, matches, mp=1, mn=0.2, **kwargs):
    matches, dist, norm = pre_loss(desc1, desc2, matches, "cosine")

    pos_loss = F.relu(mp - dist)*(matches==1)
    neg_loss = F.relu(dist - mn)*(matches==-1)

    return pos_loss.sum()/norm, neg_loss.sum()/norm

def similarity_loss(desc1, desc2, target1, target2, matches, p=1e-6, **kwargs):
    # desc1, desc2: (batch_size, num_desc, desc_size) (all of them) assumed normalized
    # matches: (batch_size, num_desc, num_desc), 1 = pos, 0 = nothing, -1 = neg
    # dist: (batch_size, num_desc, num_desc)
    matches, dist, norm = pre_loss(desc1, desc2, target1, target2, matches, "approx_l2")
    if "alpha" in kwargs:
        x = kwargs["alpha"](2-dist)
    else:
        x = (2-dist)

    # Masking
    x[matches==0] = -100

    Sc = F.log_softmax(x, dim=1)
    Sr = F.log_softmax(x, dim=2)
    Sc = Sc.clamp(log(p), log(1-p))
    Sr = Sr.clamp(log(p), log(1-p))
    loss = -0.5*(Sc[matches==1].sum() + Sr[matches==1].sum())
    return loss / norm, torch.tensor(0.)

def triplet_margin_loss(desc1, desc2, matches, m=1, **kwargs):
    # desc1, desc2: (batch_size, num_desc, desc_size) (all of them) assumed normalized
    # matches: (batch_size, num_desc, num_desc), 1 = pos, 0 = nothing, -1 = neg
    matches, dist, norm = pre_loss(desc1, desc2, matches, "approx_l2")

    dist_multi = (matches == -1).float()
    dist_multi[matches != -1] = 10

    # For each descriptor
    # Search for the negative pair that is closest
    neg_dist1 = torch.min(dist*dist_multi, dim=1).values  # (batch_size, num_desc)
    neg_dist2 = torch.min(dist*dist_multi, dim=2).values  # (batch_size, num_desc)

    # Matches should be an elementary matrix
    diff = dist*(matches==1) - torch.min(neg_dist1, neg_dist2)
    loss = F.relu(m + diff)

    return loss.sum()/norm, torch.tensor(0.)


LOSSES = {
    "hard_constrative_loss": hard_constrative_loss,
    "hinge_loss": hinge_loss,
    "similarity_loss": similarity_loss,
    "triplet_margin_loss": triplet_margin_loss,
}
