# Key net adaptation

import math
import numpy as np
import torch
from torch import nn
import torch.nn.functional as F

def relu_sigmoid(x):
    return 2/(1+torch.exp(-F.relu(x)))-1

def gaussian_multiple_channels(num_channels, sigma):

    r = 2*sigma
    size = 2*r+1
    size = int(math.ceil(size))
    x = torch.arange(start=0, end=size, step=1, dtype=float)
    y = x[:, np.newaxis]
    x0 = y0 = r

    gaussian = ((np.exp(-1 * (((x - x0) ** 2 + (y - y0) ** 2) / (2 * (sigma ** 2))))) / ((2 * math.pi * (sigma ** 2))**0.5)).float()
    weights = torch.zeros((num_channels, num_channels, size, size)).float()

    for i in range(num_channels):
        weights[i, i, :, :] = gaussian

    return weights


def ones_multiple_channels(size, num_channels):

    ones = torch.ones((size, size))
    weights = torch.zeros((num_channels, num_channels, size, size), dtype=np.float32)

    for i in range(num_channels):
        weights[i, i, :, :] = ones
    print(weights.squeeze())

    return weights


def grid_indexes(size):

    weights = torch.zeros((size, size, 1, 2), dtype=np.float32)

    columns = []
    for idx in range(1, 1+size):
        columns.append(torch.ones((size))*idx)
    columns = torch.tensor(columns)

    rows = []
    for idx in range(1, 1+size):
        rows.append(torch.asarray(range(1, 1+size)))
    rows = torch.tensor(rows)

    weights[:, :, 0, 0] = columns
    weights[:, :, 0, 1] = rows
    print(weights)

    return weights


def get_kernel_size(factor):
    """
    Find the kernel size given the desired factor of upsampling.
    """
    return 2 * factor - factor % 2


def linear_upsample_weights(half_factor, number_of_classes):
    """
    Create weights matrix for transposed convolution with linear filter
    initialization.
    """

    filter_size = get_kernel_size(half_factor)

    weights = torch.zeros((filter_size,
                           filter_size,
                           number_of_classes,
                           number_of_classes)).float()

    upsample_kernel = torch.ones((filter_size, filter_size))
    for i in range(number_of_classes):
        weights[:, :, i, i] = upsample_kernel

    return weights


def create_derivatives_kernel():
    # Sobel derivative 3x3 X
    kernel_filter_dx_3 = torch.tensor([[-1, 0, 1],
                                       [-2, 0, 2],
                                       [-1, 0, 1]]).float().view(1, 1, 3, 3)

    # Sobel derivative 3x3 Y
    kernel_filter_dy_3 = torch.tensor([[-1, -2, -1],
                                       [0, 0, 0],
                                       [1, 2, 1]]).float().view(1, 1, 3, 3)

    return kernel_filter_dx_3, kernel_filter_dy_3

class Keynet(nn.Module):
    def __init__(self):
        super(Keynet, self).__init__()
        self.scaling_steps = 0
        self.pyramid_levels = 3
        self.factor_scaling = 1.2
        self.num_blocks = 3
        self.conv_kernel_size = 5
        self.ksize = 15
        self.num_filters = 8
        self.msip_sizes = [8, 16, 24, 32, 40]
        self.msip_factor_loss = [256., 64., 16., 4., 1.]

        # Smooth Gausian Filter
        gaussian_avg = gaussian_multiple_channels(1, 1.5)
        self.register_buffer("gaussian_avg", gaussian_avg)

        # Sobel derivatives
        kernel_x, kernel_y = create_derivatives_kernel()
        self.register_buffer("kernel_x", kernel_x)
        self.register_buffer("kernel_y", kernel_y)

        self.levels = nn.ModuleList([])
        for i in range(self.pyramid_levels):
            # add layers
            layers = []
            for i in range(self.num_blocks):
                input_dim = 10 if i == 0 else self.num_filters
                layers.append(nn.ReplicationPad2d([self.conv_kernel_size//2]*4))
                layers.append(nn.Conv2d(input_dim, self.num_filters, self.conv_kernel_size, stride=1, padding=0))
                layers.append(nn.BatchNorm2d(self.num_filters))
                layers.append(nn.ReLU(inplace=True))

            layer = nn.Sequential(*layers)
            self.levels.append(layer)
        self.last_layers = nn.Sequential(
            nn.BatchNorm2d(self.num_filters*self.num_blocks),
            nn.ReplicationPad2d([self.conv_kernel_size//2]*4),
            nn.Conv2d(self.num_filters*self.num_blocks, 1, self.conv_kernel_size, stride=1, padding=0),
        )

    def compute_handcrafted_features(self, x):
        x_pad = F.pad(x, [1] * 4, mode='replicate')
        dx = F.conv2d(x_pad, self.kernel_x, padding=0, dilation=1, stride=1)
        dy = F.conv2d(x_pad, self.kernel_y, padding=0, dilation=1, stride=1)

        dx_pad = F.pad(dx, [1] * 4, mode='replicate')
        dxx = F.conv2d(dx_pad, self.kernel_x, padding=0, dilation=1, stride=1)
        dx2 = dx**2

        dy_pad = F.pad(dy, [1] * 4, mode='replicate')
        dyy = F.conv2d(dy_pad, self.kernel_y, padding=0, dilation=1, stride=1)
        dy2 = dy**2

        dxy = F.conv2d(dx_pad, self.kernel_y, padding=0, dilation=1, stride=1)
        dxdy = dx*dy
        dxxdyy = dxx*dyy
        dxy2 = dxy**2
        features_t = torch.cat(
                [dx, dx2, dxx, dy, dy2, dyy, dxdy, dxxdyy, dxy, dxy2], dim=1)

        return features_t

    def local_norm_image(self, x, k_size=65, eps=1e-10):
        pad = k_size // 2
        x_pad = F.pad(x, [pad]*4, mode='reflect')
        x_mean = F.avg_pool2d(x_pad, k_size, stride=1)
        x2_mean = F.avg_pool2d(x_pad**2, k_size, stride=1)
        x_std = torch.sqrt(torch.abs(x2_mean - x_mean**2)) + eps
        return x - x_mean / (1.+x_std)

    def forward(self, x):
        N, C, H, W = x.shape

        features = []
        for i in range(self.pyramid_levels):
            if i == 0:
                smooth_x = x
            else:
                pad_x = F.pad(x, [1] * 4, mode='replicate')
                smooth_x = F.conv2d(pad_x, self.gaussian_avg, padding=0, dilation=1, stride=1)
            # rescale
            r_x = F.interpolate(smooth_x, size=(int(H/self.factor_scaling**i), int(W/self.factor_scaling**i)), mode="bilinear", align_corners=True)
            n_x = self.local_norm_image(r_x)
            hand_crafted = self.compute_handcrafted_features(n_x)
            score_map = self.levels[i](hand_crafted)
            # rescale
            r_score_map = F.interpolate(score_map, size=(H, W), mode="bilinear", align_corners=True)
            features.append(r_score_map)
        features = torch.cat(features, dim=1)
        output = self.last_layers(features)
        #  out_fn = nn.ReLU(inplace=True)
        out_fn = relu_sigmoid
        output = out_fn(output)
        return {
            "heatmap": output,
        }

    def load_default_state_dict(self):
        rel_path = "checkpoints/d2_tf.pth"
        proj_path = os.path.join(os.path.dirname(__file__), "../../")
        #  ckpt = torch.load(os.path.join(proj_path, rel_path))
        #  if "state_dict" in ckpt:
        #      state_dict = ckpt["state_dict"]
        #  else:
        #      state_dict = ckpt
        #  self.load_state_dict(state_dict)
