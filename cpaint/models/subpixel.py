import torch
import numpy as np
import torch.nn.functional as F
import matplotlib.pyplot as plt

def localize(heatmap, kpts, radius):
    # kpts: (N, 3) in row col score format
    W, H = heatmap.shape[-2:]
    heatmap = heatmap.view(1, 1, W, H)
    x, y = torch.meshgrid(
            torch.linspace(0, W, W),
            torch.linspace(0, H, H))
    x = x.to(heatmap.device)
    y = y.to(heatmap.device)

    N = (2*radius+1)**2
    x_map = F.avg_pool2d(heatmap * x, 2*radius+1, stride=1, padding=radius) * N
    y_map = F.avg_pool2d(heatmap * y, 2*radius+1, stride=1, padding=radius) * N
    sum_map = F.avg_pool2d(heatmap, 2*radius+1, stride=1, padding=radius) * N
    subpix_x_m = x_map / sum_map
    subpix_y_m = y_map / sum_map

    # calculate size of patch
    varx = F.avg_pool2d((x-subpix_x_m)**2*heatmap, 2*radius+1, stride=1, padding=radius) * N / sum_map
    vary = F.avg_pool2d((y-subpix_y_m)**2*heatmap, 2*radius+1, stride=1, padding=radius) * N / sum_map
    #  print(x2_map)
    #  print(x_map)
    #  sdx = torch.sqrt((x2_map/sum_map**2/N - (subpix_x_m/N)**2)*N/(N-1))
    #  sdy = torch.sqrt((y2_map/sum_map**2/N - (subpix_y_m/N)**2)*N/(N-1))
    sdx = torch.sqrt(varx)
    sdy = torch.sqrt(vary)

    subpix_x = subpix_x_m[0, 0, kpts[:, 0], kpts[:, 1]]
    subpix_y = subpix_y_m[0, 0, kpts[:, 0], kpts[:, 1]]
    coords = torch.stack((subpix_x, subpix_y), dim=1)
    size = torch.min(sdx, sdy)[0, 0, kpts[:, 0], kpts[:, 1]] * 10
    #  print(coords[:10])
    #  print(kpts[:10])
    #  plt.figure()
    #  plt.imshow(heatmap.squeeze())
    #  plt.figure()
    #  plt.imshow(subpix_x_m.squeeze())
    #  plt.show()
    full_kpts = torch.cat([coords, kpts[:, 2:3], size[:, None]], dim=1)
    return full_kpts
