import torch
from torch import nn
import torch.nn.functional as F

class MultiTaskLoss(nn.Module):
    def __init__(self, num_losses, initial_est=None):
        super(MultiTaskLoss, self).__init__()
        self.num_losses = num_losses
        if initial_est is None:
            # Initialize to 0 like in paper
            for i in range(self.num_losses):
                log_var = nn.Parameter(torch.zeros((1,)))
                self.register_parameter(f"log_var_{i}", log_var)

    def forward(self, *losses):
        loss = 0
        for i, l in enumerate(losses):
            loss += self.weight(i)*l + self.get_log_var(i)
        return loss

    def weight(self, i):
        return torch.exp(-self.get_log_var(i))

    def get_log_var(self, i):
        return getattr(self, f"log_var_{i}")

    @property
    def std(self):
        stds = []
        for i in range(self.num_losses):
            stds.append(torch.exp(self.get_log_var(i))**0.5)
        return stds


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from torch import optim
    import numpy as np
    # Run same test as in paper demo
    class Net(nn.Module):
        def __init__(self, input_size, hidden_size, output1_size, output2_size):
            super(Net, self).__init__()
            self.fc1 = nn.Linear(input_size, hidden_size)
            self.relu = nn.ReLU()
            self.fc2 = nn.Linear(hidden_size, output1_size)
            self.fc3 = nn.Linear(hidden_size, output2_size)
        def forward(self, x):
            out = self.fc1(x)
            out = self.relu(out)
            out1 = self.fc2(out)
            out2 = self.fc3(out)
            return out1, out2
    N = 100
    nb_epoch = 2000
    batch_size = 20
    nb_features = 1024
    Q = 1
    nb_output = 2  # total number of output
    D1 = 1  # first output
    D2 = 1  # second output

    def gen_data(N):
        X = np.random.randn(N, Q)
        w1 = 2.
        b1 = 8.
        sigma1 = 1e1  # ground truth
        Y1 = X.dot(w1) + b1 + sigma1 * np.random.randn(N, D1)
        w2 = 3
        b2 = 3.
        sigma2 = 1e0  # ground truth
        Y2 = X.dot(w2) + b2 + sigma2 * np.random.randn(N, D2)
        return X, Y1, Y2

    model = Net(Q, nb_features, D1, D2)
    multi_loss = MultiTaskLoss(2)
    optimizer = optim.Adam(list(model.parameters()) + list(multi_loss.parameters()))

    X, Y1, Y2 = gen_data(N)
    # convert data into torch from numpy array
    X = X.astype('float32')
    Y1 = Y1.astype('float32')
    Y2 = Y2.astype('float32')

    def shuffle_data(X,Y1,Y2):
        s = np.arange(X.shape[0])
        np.random.shuffle(s)
        return X[s], Y1[s], Y2[s]

    loss_history = np.zeros(nb_epoch)

    for i in range(nb_epoch):

        epoch_loss = 0
        X, Y1, Y2 = shuffle_data(X, Y1, Y2)
        for j in range(N//batch_size):
            optimizer.zero_grad()

            inp = torch.from_numpy(X[(j*batch_size):((j+1)*batch_size)])
            target1 = torch.from_numpy(Y1[(j*batch_size):((j+1)*batch_size)])
            target2 = torch.from_numpy(Y2[(j*batch_size):((j+1)*batch_size)])

            out = model(inp)
            l1 = torch.mean((out[0] - target1)**2.)
            l2 = torch.mean((out[1] - target2)**2.)
            #  loss = criterion(out, [target1, target2], [log_var_a, log_var_b])
            loss = multi_loss(l1, l2)

            epoch_loss += loss.item()
            loss.backward()
            optimizer.step()

        loss_history[i] = epoch_loss * batch_size / N

    plt.plot(loss_history)
    plt.show()
    # Found standard deviations (ground truth is 10 and 1):
    print(multi_loss.std)
