import os
import torch
from torch import nn
import torch.nn.functional as F
from cpaint.core.image_ops import pad_to_multiple


def block(in_ch, out_ch, pool=True, pad=True):
    layers = [
        nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1, padding=1 if pad else 0),
        nn.BatchNorm2d(out_ch),
        nn.ReLU(inplace=True),
        nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1),
        nn.BatchNorm2d(out_ch),
        nn.ReLU(inplace=True),
    ]
    if pool:
        layers.append(nn.MaxPool2d(kernel_size=2, stride=2))

    return layers


class SuperPointNet(nn.Module):
    def __init__(self):
        super(SuperPointNet, self).__init__()
        self.scaling_steps = 3
        self.cell = 2**self.scaling_steps

        self.common = nn.Sequential(
            nn.ReflectionPad2d([1]*4),
            # block 1
            *block(1, 64, pad=False),
            # block 2
            *block(64, 64),
            # block 3
            *block(64, 128),
        )

        self.det_head = nn.Sequential(
            # block 4
            *block(128, 128, pool=False),
            # Rest of normal det head
            nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, self.cell**2+1, kernel_size=1, stride=1, padding=0),
            nn.BatchNorm2d(self.cell**2+1)
        )

        self.desc_head = nn.Sequential(
            # block 4
            *block(128, 128, pool=False),
            # Rest of normal desc head
            nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=1, stride=1, padding=0),
            nn.BatchNorm2d(256)
        )

        #  N, D, H, W = 64, 256, 30, 30
        #  desc = nn.Parameter(torch.rand(N, D, H, W))
        #  self.register_parameter("desc", desc)

    def _forward(self, x):
        # semi: Output point pytorch tensor shaped N x 65 x H/8 x W/8.
        # desc: Output descriptor pytorch tensor shaped N x 256 x H/8 x W/8.

        x = self.common(x)
        semi = self.det_head(x)  # (N, 65, H/8, W/8)
        desc = self.desc_head(x)  # (N, 256, H/8, W/8)
        #  desc = self.desc[:N]
        desc = F.normalize(desc, p=2, dim=1)
        return semi, desc


class SuperPoint(SuperPointNet):

    def forward(self, x):
        # The input size must be a multiple of 8
        N, _, H, W = x.shape

        semi, desc = self._forward(x)
        heatmap = self.semi_to_heatmap(semi, (N, 1, H, W))
        return {
            "heatmap": heatmap,
            "raw_desc": desc,
        }

    def semi_to_heatmap(self, semi, net_input_shape):
        N, _, H, W = net_input_shape
        # Reshape detector head
        #  dense = torch.exp(semi) # Softmax.
        #  print(dense.shape)
        #  print((torch.sum(dense, dim=1)+.00001).shape)
        #  dense = dense / (torch.sum(dense, dim=1)+.00001) # Should sum to 1.
        dense = F.softmax(semi, dim=1)
        # Remove dustbin.
        nodust = dense[:, :-1, :, :]  # N x 64 x Hc x Wc
        # Reshape to get full resolution heatmap.
        Hc = int(H / self.cell)
        Wc = int(W / self.cell)
        nodust = nodust.permute(0, 2, 3, 1)  # N x Hc x Wc
        heatmap = nodust.reshape([N, Hc, Wc, self.cell, self.cell])
        heatmap = heatmap.permute(0, 1, 3, 2, 4)
        heatmap = heatmap.reshape([N, 1, H, W])
        return heatmap

    def load_default_state_dict(self):
        #  rel_path = "cpaint/core/superpoint/superpoint_v1.pth"
        rel_path = "checkpoints/fourth_sota_cpainted.pth"
        proj_path = os.path.join(os.path.dirname(__file__), "../../")
        ckpt = torch.load(os.path.join(proj_path, rel_path))
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
        else:
            state_dict = ckpt
        self.load_state_dict(state_dict)


class SpatialSuperPoint(SuperPointNet):
    def __init__(self, radius=3):
        super(SpatialSuperPoint, self).__init__()

    def forward(self, x):
        N, _, H, W = x.shape

        semi, desc = self._forward(x)
        heatmap = self.spatial_softmax_conversion(semi, (N, 1, H, W))
        return {
            "heatmap": heatmap,
            "raw_desc": desc,
        }

    def spatial_softmax_conversion(self, semi, net_input_shape):
        N, _, H, W = net_input_shape
        # semi shape: N x 65 x H/8 x W/8.
        # Remove dustbin.
        nodust = semi[:, :-1, :, :]  # N x 64 x Hc x Wc
        # Reshape to get full resolution heatmap.
        Hc = int(H / self.cell)
        Wc = int(W / self.cell)
        nodust = nodust.permute(0, 2, 3, 1)  # N x Hc x Wc
        heatmap = nodust.reshape([N, Hc, Wc, self.cell, self.cell])
        heatmap = heatmap.permute(0, 1, 3, 2, 4)
        heatmap = heatmap.reshape([N, 1, H, W])
        #  return relu_sigmoid(heatmap)
        return torch.sigmoid(heatmap)

    def load_default_state_dict(self):
        #  rel_path = "checkpoints/zero_spatial_superpoint.pth"
        rel_path = "checkpoints/spatial_superpoint.pth"
        proj_path = os.path.join(os.path.dirname(__file__), "../../")
        ckpt = torch.load(os.path.join(proj_path, rel_path))
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
        else:
            state_dict = ckpt
        self.load_state_dict(state_dict)
