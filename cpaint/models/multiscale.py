import cv2
import numpy as np
import torch
import torch.nn.functional as F
from cpaint.core.image_ops import pad_to_multiple, unpad_multiple, get_padding_for_multiple
from cpaint.core.detector_util import score_map_to_kpts, mask_border
from cpaint.models import subpixel
import matplotlib.pyplot as plt

@torch.no_grad()
def detect_multiscale(detector, im, color_input=False, scales=[1, 2, 3, 4], base_size=11):
    # image: (B, C, H, W)
    # detector: a model that has been wrapped to have the detect and predict functions
    # scales: 1/2**s is the final size of the image after scaling
    # We assume that all points are detected at the highest resolution

    # CODE DUPLICATION FROM cpaint/models/__init__.py
    H, W = im.shape[:2]
    # Handle channels
    if not color_input:
        if len(im.shape) == 3:
            im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
            im = np.expand_dims(im, 0)
        c = 1
    else:
        # rotate channels
        im = np.rollaxis(im, 2, 0)
        c = 3
    # END

    image = torch.tensor(im).view(1, c, H, W).float()/255.

    scales.sort() # make sure scales are in right order
    # obtain initial detections: (N, 4) in row col score size format
    kpts = detector.detect(image)
    N = kpts.shape[0]
    th_kpts = torch.from_numpy(kpts).float()
    grid = torch.zeros((N, 2))
    grid[:, 0] = th_kpts[:, 1]/W*2 - 1
    grid[:, 1] = th_kpts[:, 0]/H*2 - 1
    grid = grid.view(1, 1, N, 2)

    # initialize keypoint size to smallest possible size
    # rescale and test when detections stop being detected
    for s in scales[1:]:
        resized = F.interpolate(image, scale_factor=1/s, mode="bilinear", align_corners=False)
        hr, wr = resized.shape[-2:]
        heatmap = detector.predict(resized)
        heatmap = torch.tensor(heatmap).view(1, -1, hr, wr)
        # Sample heatmap with keypoints from largest image
        scores = F.grid_sample(heatmap, grid, mode="bilinear", align_corners=False).view(-1)
        #  print(heatmap.min(), heatmap.max())

        # see if they show up in the new size. If they do, assign them the new largest size
        inds = np.where(scores.numpy() > detector.threshold)
        print(len(inds[0]))
        smol_kpts = kpts[inds].copy()
        smol_kpts[:, :2] /= s
        smol_kpts = subpixel.localize(heatmap, smol_kpts, 1)
        kpts[inds, 3] = smol_kpts[:, 3]*s*base_size

        # Mask existing keypoints
        mask = np.ones(heatmap.shape[2:])
        print(kpts.astype(np.int)[:, 0])
        print(mask.shape)
        x = (kpts/s).astype(np.int)[:, 0]
        y = (kpts/s).astype(np.int)[:, 1]
        mask[x, y] = 0
        mask = cv2.erode(mask, np.ones((3, 3), np.uint8), iterations=1)
        new_kpts = score_map_to_kpts(heatmap[0, 0], threshold=detector.threshold, point_mask=mask)
        new_kpts = subpixel.localize(heatmap, new_kpts, 1)
        new_kpts[:, :2] *= s
        new_kpts[:, 3] *= s

        # add keypoints
        kpts = np.concatenate([kpts, new_kpts], axis=0)

        #  key_pts = [cv2.KeyPoint(p[1]/s, p[0]/s, _size=2) for p in kpts]
        #  kkpts_img = (heatmap.numpy().squeeze().copy()/0.03*255).astype(np.uint8)
        #  kkpts_img = cv2.drawKeypoints(kkpts_img, key_pts, kkpts_img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        #  plt.figure()
        #  plt.imshow(kkpts_img)
        #  plt.figure()
        #  kkpts_img = (resized.numpy().squeeze().copy()*255).astype(np.uint8)
        #  kkpts_img = cv2.drawKeypoints(kkpts_img, key_pts, kkpts_img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        #  plt.imshow(kkpts_img)
        #  plt.show()
        del heatmap, resized, scores, smol_kpts
    return kpts
