import torch
from torch import nn
import torch.nn.functional as F

class L2Net(nn.Module):
    def __init__(self, in_ch=3, out_dim=128):
        affine = False
        self.net = nn.Sequential(
            # conv1
            nn.Conv2d(in_ch, 32, 3, padding=1),
            nn.BatchNorm2d(32, affine=affine),
            nn.ReLU(inplace=True, padding=1),

            # conv2
            nn.Conv2d(32, 32, 3, padding=1),
            nn.BatchNorm2d(32, affine=affine),
            nn.ReLU(inplace=True),

            # conv3
            nn.Conv2d(32, 64, 3, stride=2, padding=1),
            nn.BatchNorm2d(64, affine=affine),
            nn.ReLU(inplace=True),

            # conv4
            nn.Conv2d(64, 64, 3, padding=1),
            nn.BatchNorm2d(64, affine=affine),
            nn.ReLU(inplace=True),

            # conv5
            nn.Conv2d(64, 128, 3, stride=2, padding=1),
            nn.BatchNorm2d(128, affine=affine),
            nn.ReLU(inplace=True),

            # conv6
            nn.Conv2d(128, 128, 3, padding=1),
            nn.BatchNorm2d(128, affine=affine),
            nn.ReLU(inplace=True),
            nn.Dropout2d(p=0.3),

            # conv7
            nn.Conv2d(128, out_dim, 8),
            nn.BatchNorm2d(128, affine=affine),
            )
        # there is supposed to be an LRN layer but I don't care to implement it
        # I'm not using this code.

        def forward(self, x):
            x = self.net(x)
            return F.normalize(x, p=2, dim=1)
