from . import superpoint
from . import superpoint_bn
import torch.nn as nn
import torch
import numpy as np
import cv2
import torch.nn.functional as F
import matplotlib.pyplot as plt
from . import unet
import time

from cpaint.core.image_ops import pad_to_multiple, unpad_multiple, get_padding_for_multiple
from cpaint.core.detector_util import score_map_to_kpts, mask_border
from cpaint.models import subpixel

def lazy_import_aslfeat(*args, **kwargs):
    from .aslfeat import ASLFeat
    return ASLFeat(*args, **kwargs)

def lazy_import_r2d2(*args, **kwargs):
    from .r2d2 import Quad_L2Net_ConfCFS as R2D2
    return R2D2(*args, **kwargs)

def lazy_import_d2net(*args, **kwargs):
    from .d2net import D2Net
    return D2Net(*args, **kwargs)

def lazy_import_keynet(*args, **kwargs):
    from .keynet import Keynet
    return Keynet(*args, **kwargs)

MODELS = {
    "miniunet": {
        "net": lambda: unet.MiniUNet(2),
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 16,
    },
    "unet": {
        "net": lambda: unet.UNet(2),
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 2,
    },
    "superpoint": {
        "net": superpoint.SuperPoint,
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 8,
    },

    "superpoint_bn_l2": {
        "net": superpoint_bn.SuperPoint,
        "color_input": False,
        "loss": nn.MSELoss(reduction='none'),
        "binary_target": False,
        "input_size_multiple": 8,
    },

    "superpoint_bn": {
        "net": superpoint_bn.SuperPoint,
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 8,
    },

    "spatial_superpoint": {
        "net": superpoint.SpatialSuperPoint,
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 8,
    },
    "spatial_superpoint_bn": {
        "net": superpoint_bn.SpatialSuperPoint,
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 8,
    },
    "r2d2": {
        "net": lazy_import_r2d2,
        "color_input": True,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 1,
    },
    "d2net": {
        "net": lazy_import_d2net,
        "color_input": True,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 1,
    },
    "aslfeat": {
        "net": lazy_import_aslfeat,
        "color_input": True,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 1,
    },
    "keynet": {
        "net": lazy_import_keynet,
        "color_input": False,
        "loss": nn.BCELoss(reduction='none'),
        "binary_target": True,
        "input_size_multiple": 1,
    },
}

def prep_input(im, color_input, input_size_multiple):
    if len(im.shape) != 4:
        H, W = im.shape[:2]
        # Handle channels
        if not color_input:
            if len(im.shape) == 3:
                im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
                im = np.expand_dims(im, 0)
            c = 1
        else:
            # rotate channels
            im = np.rollaxis(im, 2, 0)
            c = 3

        data = torch.tensor(im).view(1, c, H, W).float()/255.
    else:
        data = im
    old_size = data.shape

    # Handle odd shapes and padding
    data = pad_to_multiple(data, input_size_multiple)
    return data, old_size


def model_to_detector(model, color_input, threshold=None, input_size_multiple=1, use_subpixel=True):
    detector = type('Detector Wrapper', (object,), {})()
    device = torch.device("cuda")
    def _predict(im):
        data, old_size = prep_input(im, color_input, input_size_multiple)

        # Just transfer data
        #  t = data.to(device)
        #  heatmap = torch.zeros_like(t)

        result = model(data.to(device))
        heatmap = result["heatmap"].detach().cpu()

        mask = mask_border(heatmap, border=4)
        heatmap *= mask
        heatmap = unpad_multiple(heatmap, old_size, input_size_multiple)
        heatmap = heatmap.squeeze().numpy()
        #  cv2.imshow("heatmap", heatmap)
        #  plt.imshow(heatmap)
        #  plt.show()
        return heatmap

    def _detect(im):
        heatmap = _predict(im)
        # Convert heatmap to points
        # Shape: (N, 3)
        kpts = score_map_to_kpts(heatmap, threshold=threshold)
        if use_subpixel:
            th_kpts = torch.tensor(kpts).long()
            th_heatmap = torch.tensor(heatmap)
            full_kpts = subpixel.localize(th_heatmap, th_kpts, 1)
            return full_kpts.numpy()
        else:
            return kpts

    detector.threshold = threshold
    detector.detect = _detect
    detector.predict = _predict
    detector.color_input = color_input
    return detector

def sample_descriptors(descriptor, H, W, pts, padding=[0,0,0,0], normalize=True):
    # descriptor (N, C, H, W)
    # pts: (N, M, 2) in image coordinates (row col)
    # padding: lp, rp, tp, bp
    N, M, _ = pts.shape
    tens_pts = pts.clone().to(descriptor.device).float()
    norm_pts = torch.zeros_like(tens_pts) # (N, M, 2)
    norm_pts[:, :, 0] = (tens_pts[:, :, 1]+padding[0])/W*2 - 1
    norm_pts[:, :, 1] = (tens_pts[:, :, 0]+padding[2])/H*2 - 1
    grid = norm_pts.view(N, 1, M, 2)

    descs = F.grid_sample(descriptor, grid, mode="bilinear", align_corners=False)
    # descs has shape (N, C, 1, M)
    if normalize:
        descs = F.normalize(descs, p=2, dim=1)
    return descs.view(N, -1, M).transpose(1, 2)

def model_to_descriptor(model, color_input, input_size_multiple=1):
    detector = type('Descriptor Wrapper', (object,), {})()
    device = torch.device("cuda")
    def _compute(im, kpts):
        data, old_size = prep_input(im, color_input, input_size_multiple)

        # padding: lp, rp, tp, bp
        padding = get_padding_for_multiple(old_size, input_size_multiple)
        if type(kpts) == torch.Tensor:
            pts = kpts[:, :, :2]
        elif type(kpts) == np.ndarray:
            pts = torch.tensor(kpts[:, :2]).unsqueeze(0)
        else:
            pts = torch.tensor([(kpt.pt[1], kpt.pt[0]) for kpt in kpts]).float().view(1, -1, 2)
        if pts.shape[1] == 0:
            return pts.squeeze(0).numpy(), None

        _, C, H, W = old_size

        #  start_time = time.time()
        result = model(data.to(device))
        desc = result["raw_desc"]
        sampled = sample_descriptors(desc, H, W, pts, padding)
        #  print(f"Sample: {time.time()-start_time}")

        return pts.squeeze(0).numpy(), sampled.detach().float().cpu().numpy().squeeze(0)
    detector.compute = _compute
    detector.color_input = color_input
    return detector

def model_to_feature_extractor(model, color_input, threshold, input_size_multiple=1):
    detector = type('Feature Extractor Wrapper', (object,), {})()
    device = torch.device("cuda")
    def _detectAndCompute(im):
        # Returns 
        #  (Nx3) pts = (row, col, score) keypoints in image coordinates
        #  (NxD) desc = corresponding descriptors
        data, old_size = prep_input(im, color_input, input_size_multiple)
        _, C, H, W = old_size
        # padding: lp, rp, tp, bp
        padding = get_padding_for_multiple(old_size, input_size_multiple)

        # Run model
        result = model(data.to(device))
        heatmap = result["heatmap"]
        desc = result["raw_desc"]
        D = desc.shape[1]
        mask = mask_border(heatmap, border=4)
        heatmap *= mask
        heatmap = unpad_multiple(heatmap, old_size, input_size_multiple)

        # Convert heatmap to points
        # Shape: (3, N)
        score_map = np.array(heatmap.data.cpu().squeeze())
        kpts = score_map_to_kpts(score_map, threshold=threshold, nms=8)
        pts = torch.from_numpy(kpts).unsqueeze(0)

        # Get descriptors
        if pts.shape[1] == 0:
            sampled = torch.zeros(1, 0, D)
        else:
            sampled = sample_descriptors(desc, H, W, pts[:, :, :2], padding)

        return pts.squeeze(0).numpy(), sampled.detach().float().cpu().numpy().squeeze(0)

    detector.detectAndCompute = _detectAndCompute
    detector.color_input = color_input
    detector.threshold = threshold
    return detector
