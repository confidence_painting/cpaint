import cv2
import math
import torch
import sift_ori
import numpy as np
import torch.nn.functional as F

def get_num_octaves(im_shape, firstOctave):
    return int(math.log( min(im_shape) ) / math.log(2.) - 2 + 0.5) - firstOctave

def get_dog_values(image, kpts, firstOctave=-1):
    # kpts: (N, 2) row col (numpy)
    # image: (H, W, C?) (numpy)
    # firstOctave: -1 for doubling the size of the first image, 0 otherwise. -1 is default on sift

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    actualNOctaves = get_num_octaves(gray.shape, firstOctave)
    ret = sift_ori.dog_pyramid(gray, firstOctave, actualNOctaves)
    values = []
    H, W = gray.shape
    N = kpts.shape[0]
    for layer in ret:
        hr, wr = layer.shape[-2:]
        grid = np.zeros((N, 2))
        grid[:, 0] = kpts[:, 0]/W*2 - 1
        grid[:, 1] = kpts[:, 1]/H*2 - 1
        grid = torch.tensor(grid).view(1, 1, N, 2).float()
        layer_th = torch.tensor(layer).view(1, -1, hr, wr).float()
        vals = F.grid_sample(layer_th, grid, mode="bilinear", align_corners=False).view(-1)
        values.append(vals)
        # scipy grid_sample per a layer to retrieve a list of values for each keypoints
    values = torch.stack(values, dim=0)
    return values

def assign_kpt_octave(image, kpts, firstOctave=-1):
    # Input
    #  kpts: (N, 2) row col (numpy)
    #  image: (H, W, C?) (numpy)
    #  firstOctave: -1 for doubling the size of the first image, 0 otherwise. -1 is default on sift
    # Output
    #  [x, y, size, layer, octave]
    #  octave cannot have negative values here
    N = kpts.shape[0]
    dog_values = get_dog_values(image, kpts, firstOctave)
    indices = [] # TODO
    sizes = [] # TODO

    # convert index to octave and layer
    actualNOctaves = get_num_octaves(image.shape[:2], firstOctave)
    w = actualNOctaves+3
    new_kpts = np.zeros((N, 5))
    new_kpts[:, :2] = kpts[:, :2]
    for i, (size, ind) in enumerate(zip(sizes, indices)):
        layer = ind % w
        octave = ind // w
        new_kpts[i, 2] = size
        new_kpts[i, 3] = layer
        new_kpts[i, 4] = octave
    return new_kpts

def compute_sift_descriptors(image, kpts, firstOctave=-1):
    # Run my custom code to get size and ori
    kpts = assign_kpt_octave(image, kpts, firstOctave=-1)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    actualNOctaves = get_num_octaves(gray.shape, firstOctave)
    new_kpts = sift_ori.sift_desc(gray, kpts, firstOctave, actualNOctaves)
    # Convert to opencv keypoints
    # Pass through OpenCV sift descriptor
    cvkpts = []
    for i in range(new_kpts.shape[0]):
        octave = kpts[i, 4]
        layer = kpts[i, 3]
        encoded = ((octave+firstOctave) & 255) | (layer << 8)
        cvkpts.append(
            cv2.KeyPoint(new_kpts[i, 0], new_kpts[i, 1], _size=new_kpts[i, 2], _angle=kpts[i, 5], _octave=encoded))

    kpts, desc = self.descriptor.compute(gray, cvkpts)
    return desc
