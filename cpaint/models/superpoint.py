import os
import torch
import torch.nn.functional as F
from cpaint.core.image_ops import pad_to_multiple

def relu_sigmoid(x):
    return 2/(1+torch.exp(-F.relu(x)))-1

class SuperPointNet(torch.nn.Module):
    """ Pytorch definition of SuperPoint Network with spatial softmax
        Adapted from official pytorch SuperPoint code
    """
    def __init__(self):
        super(SuperPointNet, self).__init__()
        self.scaling_steps = 3
        self.relu = torch.nn.ReLU(inplace=True)
        self.pool = torch.nn.MaxPool2d(kernel_size=2, stride=2)
        c1, c2, c3, c4, c5, d1 = 64, 64, 128, 128, 256, 256
        self.pad = torch.nn.ReflectionPad2d([1]*4)
        # Shared Encoder.
        self.conv1a = torch.nn.Conv2d(1, c1, kernel_size=3, stride=1, padding=0)
        self.conv1b = torch.nn.Conv2d(c1, c1, kernel_size=3, stride=1, padding=1)
        self.conv2a = torch.nn.Conv2d(c1, c2, kernel_size=3, stride=1, padding=1)
        self.conv2b = torch.nn.Conv2d(c2, c2, kernel_size=3, stride=1, padding=1)
        self.conv3a = torch.nn.Conv2d(c2, c3, kernel_size=3, stride=1, padding=1)
        self.conv3b = torch.nn.Conv2d(c3, c3, kernel_size=3, stride=1, padding=1)
        self.conv4a = torch.nn.Conv2d(c3, c4, kernel_size=3, stride=1, padding=1)
        self.conv4b = torch.nn.Conv2d(c4, c4, kernel_size=3, stride=1, padding=1)
        # Detector Head.
        self.convPa = torch.nn.Conv2d(c4, c5, kernel_size=3, stride=1, padding=1)
        self.convPb = torch.nn.Conv2d(c5, 65, kernel_size=1, stride=1, padding=0)
        # Descriptor Head.
        self.convDa = torch.nn.Conv2d(c4, c5, kernel_size=3, stride=1, padding=1)
        self.convDb = torch.nn.Conv2d(c5, d1, kernel_size=1, stride=1, padding=0)

        self.cell = 8 # Size of each output cell. Keep this fixed.

    def _forward(self, x):
        """ Forward pass that jointly computes unprocessed point and descriptor
        tensors.
        Input
            x: Image pytorch tensor shaped N x 1 x H x W.
        Output
            semi: Output point pytorch tensor shaped N x 65 x H/8 x W/8.
            desc: Output descriptor pytorch tensor shaped N x 256 x H/8 x W/8.
        """
        N, _, H, W = x.shape

        # Shared Encoder.
        x = self.pad(x)
        x = self.relu(self.conv1a(x))
        x = self.relu(self.conv1b(x))
        x = self.pool(x)
        x = self.relu(self.conv2a(x))
        x = self.relu(self.conv2b(x))
        x = self.pool(x)
        x = self.relu(self.conv3a(x))
        x = self.relu(self.conv3b(x))
        x = self.pool(x)
        x = self.relu(self.conv4a(x))
        x = self.relu(self.conv4b(x))
        # Detector Head.
        cPa = self.relu(self.convPa(x))
        semi = self.convPb(cPa)
        # Descriptor Head.
        cDa = self.relu(self.convDa(x))
        desc = self.convDb(cDa)
        dn = torch.norm(desc, p=2, dim=1) # Compute the norm.
        desc = desc.div(torch.unsqueeze(dn, 1)) # Divide by norm to normalize.
        return semi, desc

class SuperPoint(SuperPointNet):

    def forward(self, x):
        # The input size must be a multiple of 8
        N, _, H, W = x.shape

        semi, desc = self._forward(x)
        heatmap = self.semi_to_heatmap(semi, (N, 1, H, W))
        return {
            "heatmap": heatmap,
            "raw_desc": desc,
        }

    def semi_to_heatmap(self, semi, net_input_shape):
        N, _, H, W = net_input_shape
        # Reshape detector head
        #  dense = torch.exp(semi) # Softmax.
        #  print(dense.shape)
        #  print((torch.sum(dense, dim=1)+.00001).shape)
        #  dense = dense / (torch.sum(dense, dim=1)+.00001) # Should sum to 1.
        dense = F.softmax(semi, dim=1)
        # Remove dustbin.
        nodust = dense[:, :-1, :, :] # N x 64 x Hc x Wc
        # Reshape to get full resolution heatmap.
        Hc = int(H / self.cell)
        Wc = int(W / self.cell)
        nodust = nodust.permute(0, 2, 3, 1) # N x Hc x Wc
        heatmap = nodust.reshape([N, Hc, Wc, self.cell, self.cell])
        heatmap = heatmap.permute(0, 1, 3, 2, 4) # N x Hc x c x Wc x c
        heatmap = heatmap.reshape([N, 1, H, W])
        return heatmap

    def load_default_state_dict(self):
        rel_path = "third_party/superpoint/superpoint_v1.pth"
        proj_path = os.path.join(os.path.dirname(__file__), "../../")
        ckpt = torch.load(os.path.join(proj_path, rel_path))
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
        else:
            state_dict = ckpt
        self.load_state_dict(state_dict)


class SpatialSuperPoint(SuperPointNet):
    def __init__(self, radius=3):
        super(SpatialSuperPoint, self).__init__()

    def forward(self, x):
        N, _, H, W = x.shape

        semi, desc = self._forward(x)
        heatmap = self.spatial_softmax_conversion(semi, (N, 1, H, W))
        return {
            "heatmap": heatmap,
            "raw_desc": desc,
        }

    def spatial_softmax_conversion(self, semi, net_input_shape):
        N, _, H, W = net_input_shape
        # semi shape: N x 65 x H/8 x W/8.
        # Remove dustbin.
        nodust = semi[:, :-1, :, :] # N x 64 x Hc x Wc
        # Reshape to get full resolution heatmap.
        Hc = int(H / self.cell)
        Wc = int(W / self.cell)
        nodust = nodust.permute(0, 2, 3, 1) # N x Hc x Wc
        heatmap = nodust.reshape([N, Hc, Wc, self.cell, self.cell])
        heatmap = heatmap.permute(0, 1, 3, 2, 4)
        heatmap = heatmap.reshape([N, 1, H, W])
        #  return relu_sigmoid(heatmap)
        return torch.sigmoid(heatmap)

        """
        # Apply spatial softmax
        exp_heatmap = torch.exp(heatmap)
        #  exp_heatmap = heatmap
        #  padded = F.pad(exp_heatmap, (self.radius, self.radius, self.radius, self.radius), "constant", 1)
        #  norm = F.conv2d(padded, self.sum_kern, padding=0)
        norm = (
            self.radius ** 2 *
            F.avg_pool2d(
                F.pad(exp_heatmap, [self.pad] * 4, mode='constant', value=1.),
                self.radius, stride=1
            )
        )
        softmaxed = exp_heatmap/(norm)
        """
        softmaxed = self.det_module(heatmap)
        return softmaxed.view([N, H, W])

    def load_default_state_dict(self):
        #  rel_path = "checkpoints/zero_spatial_superpoint.pth"
        rel_path = "checkpoints/spatial_superpoint.pth"
        proj_path = os.path.join(os.path.dirname(__file__), "../../")
        ckpt = torch.load(os.path.join(proj_path, rel_path))
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
        else:
            state_dict = ckpt
        self.load_state_dict(state_dict)
