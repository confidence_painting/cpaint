import torch
from torch import nn
import torch.nn.functional as F
import os

class MovingAverageNorm(nn.Module):
    def __init__(self, decay):
        super(MovingAverageNorm, self).__init__()
        self.decay = decay
        variable = torch.Tensor([1.0]).float()
        self.register_buffer("variable", variable)
        self.variable.requires_grad = False

    def forward(self, x):
        value = torch.max(x).detach()
        if self.training:
            self.variable = self.variable * self.decay + value * (1 - self.decay)
        return x/self.variable

class PeakinessLayer(nn.Module):
    def __init__(self, kernel_size, norm, num_channels):
        super(PeakinessLayer, self).__init__()
        self.pad = kernel_size//2
        self.kernel_size = kernel_size
        self.norm = norm
        if norm:
            self.man = MovingAverageNorm(0.99)
        self.max_pool = nn.MaxPool3d((num_channels, 1, 1))

    def forward(self, y):
        B, C, H, W = y.shape
        return F.sigmoid(torch.mean(y, dim=1))
        if self.norm:
            y = self.man(y)
        beta = F.softplus(y - torch.mean(y, dim=1, keepdim=True))
        #  beta = F.sigmoid(y - torch.mean(y, dim=1).reshape(B, 1, H, W))

        # Alpha calculation
        sum_y = (
            F.avg_pool2d(
                F.pad(y, [self.pad] * 4, mode='constant', value=1.),
                self.kernel_size, stride=1
            )
        )
        alpha = F.softplus(y - sum_y)
        scores = torch.max(alpha*beta, dim=1).values.clamp(0, 1)
        return scores

class ASLFeat(nn.Module):
    def __init__(self, in_ch=3, out_dim=128, deformable=False):
        super(ASLFeat, self).__init__()
        self.scaling_steps = 2
        self.out_dim = out_dim
        # Original code:
        #  (self.feed('data')
        #   .conv_bn(3, 32, 1, name='conv0')
        #   .conv(3, 32, 1, biased=False, relu=False, name='conv1')
        #   .batch_normalization(relu=True, name='conv1/bn')
        #   .conv_bn(3, 64, 2, name='conv2')
        #   .conv(3, 64, 1, biased=False, relu=False, name='conv3')
        #   .batch_normalization(relu=True, name='conv3/bn')
        #   .conv_bn(3, 128, 2, name='conv4')
        #   .conv_bn(3, 128, 1, name='conv5')
        #   .conv_bn(3, 128, 1, name='conv6_0')
        #   .conv_bn(3, 128, 1, name='conv6_1')
        #   .conv(3, 128, 1, biased=False, relu=False, name='conv6'))
        self.conv1 = nn.Sequential(
            # conv0
            nn.Conv2d(in_ch, 32, 3, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),

            # conv1
            nn.Conv2d(32, 32, 3, padding=1),
        )
        self.conv3 = nn.Sequential(
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),

            # conv2
            nn.Conv2d(32, 64, 3, stride=2, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),

            # conv3
            nn.Conv2d(64, 64, 3, padding=1)
        )
        self.conv5 = nn.Sequential(
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),

            # conv4
            nn.Conv2d(64, 128, 3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),

            # conv5
            nn.Conv2d(128, 128, 3, stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),
        )
        if deformable:
            import DCN
            self.conv6 = nn.Sequential(
                # conv6_0
                DCN.DCN(128, 128, 3, 1, 0),
                nn.BatchNorm2d(128),
                nn.ReLU(inplace=True),

                # conv6_1
                DCN.DCN(128, 128, 3, 1, 0),
                nn.BatchNorm2d(128),
                nn.ReLU(inplace=True),

                # conv6
                DCN.DCN(128, 128, 3, 1, 0)
            )
        else:
            self.conv6 = nn.Sequential(
                # conv6_0
                nn.Conv2d(128, 128, 3, stride=1, padding=1),
                nn.BatchNorm2d(128),
                nn.ReLU(inplace=True),

                nn.Conv2d(128, 128, 3, stride=1, padding=1),
                # conv6_1
                nn.BatchNorm2d(128),
                nn.ReLU(inplace=True),

                # conv6
                nn.Conv2d(128, 128, 3, stride=1, padding=1),
            )
        #  self.register_parameter("conv1", conv1)
        #  self.register_parameter("conv3", conv3)
        #  self.register_parameter("conv6", conv6)
        self.score_weights = torch.Tensor([1, 2, 3]).float()/6
        self.peak1 = PeakinessLayer(9, True, 32)
        self.peak2 = PeakinessLayer(7, True, 64)
        self.peak3 = PeakinessLayer(3, True, 128)
        #  for i, l in enumerate(self.peakiness_layers):
        #      self.register_parameter(f"peakiness_layer{i}", l)

    def forward(self, x):
        B, C, H, W = x.shape
        # Normalize
        x = x/255.

        x = self.conv1(x)
        scores = self.score_weights[0]*self.peak1(x).unsqueeze(1)
        comb = scores

        x = self.conv3(x)
        scores = self.score_weights[1]*self.peak2(x).unsqueeze(1)
        scores = F.interpolate(scores, size=(H, W), mode="bilinear")
        #  print(scores.requires_grad)
        #  print(scores.max())
        #  print(scores.min())
        #  comb += scores

        x = self.conv5(x)
        x = self.conv6(x)
        scores = self.score_weights[2]*self.peak3(x).unsqueeze(1)
        scores = F.interpolate(scores, size=(H, W), mode="bilinear")
        #  comb += scores

        desc = x/torch.norm(x, p=2, dim=1, keepdim=True)

        return {
            "heatmap": comb,
            "raw_desc": desc,
        }

    def load_default_state_dict(self):
        rel_path = "checkpoints/d2_tf.pth"
        proj_path = os.path.join(os.path.dirname(__file__), "../../")
        #  ckpt = torch.load(os.path.join(proj_path, rel_path))
        #  if "state_dict" in ckpt:
        #      state_dict = ckpt["state_dict"]
        #  else:
        #      state_dict = ckpt
        #  self.load_state_dict(state_dict)
