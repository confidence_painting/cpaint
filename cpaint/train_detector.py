# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# To be clear, test = validation set.

import torch
import numpy as np

from cpaint import train_util

# Debugging
import cpaint.core.detector_util as det
import matplotlib.pyplot as plt


def train_iter(args, model, loss_fn, device, train_loader, optimizer, epoch, run_name, writer):
    model.train()
    avg_loss = 0
    for batch_idx, batch in enumerate(train_loader):
        #  torch.cuda.empty_cache()
        (data, painted, target, mask) = [b.to(device).float() for b in batch]
        N = data.shape[0]

        optimizer.zero_grad()
        result = model(data)
        heatmap = result["heatmap"]
        if args.display:
            #  for i in range(data.shape[0]):
            for i in range(1):
                fig = plt.figure(figsize=(4.5, 1.5), dpi=600)
                print(f"Found {torch.sum(target[i]*mask[i])} points")

                f = fig.add_subplot(1, 5, 1)
                f.set_title("Heatmap")
                f.imshow(heatmap[i].detach().cpu().squeeze())

                f = fig.add_subplot(1, 5, 2)
                drawn = train_util.draw_target((target[i]*mask[i]).cpu(), data[i].detach().cpu().unsqueeze(0)).squeeze()
                f.set_title("Target")

                f.imshow(drawn)

                f = fig.add_subplot(1, 5, 3)
                f.set_title("Image")
                im = data[i].detach().cpu().numpy().squeeze()
                if len(im.shape) == 3:
                    im = np.rollaxis(im, 0, 3)
                #  im = im.astype(np.uint8)
                #  im = image_ops.draw_keypoints(masked_target > args.threshold, im)
                f.imshow(im)

                f = fig.add_subplot(1, 5, 4)
                f.set_title("Map")
                #  f.imshow(mask[i].detach().cpu().squeeze(0))
                #  h = heatmap[i].detach().cpu().squeeze()
                h = (painted[i]*mask[i]).detach().cpu().squeeze().numpy()
                f.imshow(h)

                f = fig.add_subplot(1, 5, 5)
                f.set_title("Mask")
                #  f.imshow(mask[i].detach().cpu().squeeze(0))
                #  h = heatmap[i].detach().cpu().squeeze()
                h = mask[i].detach().cpu().squeeze().numpy()
                f.imshow(h)

                plt.show()
        loss = train_util.loss_w_mask(heatmap, target, loss_fn, mask=mask)
        # Dummy loss to satisfy torch dist
        if "raw_desc" in result:
            desc = result["raw_desc"]
            loss += torch.max(torch.norm(desc, dim=1) - 1, 0).values.mean()
        #  loss = torch.norm(heatmap)
        #  avg_loss += loss.detach().item()
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0 and device == 0:
        #      writer.add_scalar("train/loss", avg_loss/args.log_interval, epoch*len(train_loader)+batch_idx)
            #  avg_loss = 0
            print('Train Epoch: {} \t[{}/{} ({:.0f}%)]\tLoss: {}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()/N))


def test_iter(args, model, loss_fn, device, test_loader, epoch, run_name, writer):
    model.eval()
    test_loss = 0
    mean_painted_val = 0
    mean_heatmap_val = 0
    mean_diff = 0
    n = 0
    train_util.standardize_size(test_loader)
    with torch.no_grad():
        for i, batch in enumerate(test_loader):
            (data, painted, target, mask) = [b.float() for b in batch]
            data = data.to(device)

            N, C, H, W = data.shape
            # data: N, 3, H, W
            # painted: N, 1, H, W
            # mask: N, 1, H, W

            # Run mask max on whole batch for speed
            result = model(data)
            heatmap = result["heatmap"].cpu()
            data = data.cpu()
            test_loss += train_util.loss_w_mask(heatmap, target, loss_fn, mask=mask).item()  # sum up batch loss
            mean_painted_val += painted.mean()
            mean_heatmap_val += heatmap.mean()
            mean_diff += (painted - heatmap).mean()
            n += N

            if i <= 2 and device == 0:
                # Draw keypoints
                #  writer.add_images("data", data, dataformats='NCHW', global_step=epoch)
                writer.add_images("map", train_util.color_tensor((mask*painted).view(N, 1, H, W)), dataformats='NCHW', global_step=epoch)
                writer.add_images("mask", mask.view(N, 1, H, W), dataformats='NCHW', global_step=epoch)

                targets = []
                for j in range(N):
                    drawn = train_util.draw_target(target[j].cpu(), data[j].detach().cpu().unsqueeze(0)).squeeze()
                    targets.append(drawn)
                targets = np.stack(targets, axis=0)
                writer.add_images("label", targets, dataformats='NHWC', global_step=epoch)
                #  mask = det.mask_border(heatmap, border=10)
                #  writer.add_images("output", train_util.color_tensor(heatmap*mask.unsqueeze(1)), dataformats='NCHW', global_step=epoch)
                writer.add_images("output", train_util.color_tensor(heatmap), dataformats='NCHW', global_step=epoch)
                del targets

    mean_painted_val /= n
    mean_heatmap_val /= n
    mean_diff /= n
    writer.add_scalar("test/loss", test_loss, epoch)
    writer.add_scalar("test/mean_painted_val", mean_painted_val, epoch)
    writer.add_scalar("test/mean_heatmap_val", mean_heatmap_val, epoch)
    writer.add_scalar("test/mean_diff", mean_diff, epoch)

    print(f'\nTest set: Average loss: {test_loss}\n')


def train_main(args, gt_source, run_name, threshold):
    args.train_descriptors = False
    #  mp.spawn(train_util.train, args=
    #          (args, gt_source, run_name, threshold, args.world_size, train_iter, test_iter),
    #          nprocs=args.world_size, join=True)
    train_util.train(0, args, gt_source, run_name, threshold, args.world_size, train_iter, test_iter)


if __name__ == '__main__':
    args = train_util.parse_args()
    train_main(args, args.gt_source, args.run_name, args.threshold)
