import sys
import os
import json
import numpy as np
import cv2
import io
import time
import argparse
import re

from unrealcv import client


client.connect() # Connect to the game
if not client.isconnected(): # Check if the connection is successfully established
  print('UnrealCV server is not running. Run the game from http://unrealcv.github.io first.')
  sys.exit(-1)

res = client.request('vget /unrealcv/status')
unreal_path = os.path.dirname([l.split(":")[1] for l in res.split("\n") if "Config file" in l][0])
print(f"Path to unreal: {unreal_path}")

print(res)

def save_pose(location, prefix, use_prev_pose):
    rgb_fname = os.path.join(os.path.abspath(location), f"{prefix}_rgb.png")
    depth_fname = os.path.join(os.path.abspath(location), f"{prefix}_depth.exr")
    pose_fname = os.path.join(os.path.abspath(location), f"{prefix}_pose.json")
    cam_id = 0
    print(pose_fname)
    if use_prev_pose:
        with open(pose_fname, "r") as f:
            pose_dict = json.load(f)
            x,y,z = pose_dict["position"]
            p, a, r = pose_dict["pitchyawroll"]
        client.request(f'vset /camera/{cam_id}/location {x} {y} {z}')
        client.request(f'vset /camera/{cam_id}/rotation {p} {a} {r}')

    old_rgb_fname = client.request(f'vget /camera/{cam_id}/lit rgb.png')
    depth_dat = client.request('vget /camera/0/depth npy')
    xyz = client.request(f'vget /camera/{cam_id}/location')
    pitchyawroll = client.request(f'vget /camera/{cam_id}/rotation')
    depth = np.load(io.BytesIO(depth_dat))
    cv2.imwrite(depth_fname, depth)

    # Move images to desired location
    os.rename(old_rgb_fname, rgb_fname)
    print("Renamed")

    if not use_prev_pose:
        with open(pose_fname, "w+") as f:
            json.dump({
                "position": [float(s) for s in xyz.split(" ")],
                "pitchyawroll": [float(s) for s in pitchyawroll.split(" ")],
            }, f)

parser = argparse.ArgumentParser()
parser.add_argument("output_dir", type=str, help="Output dir for capture")
parser.add_argument("--recapture", action="store_true", help="Recapture based on pose.")
args = parser.parse_args()

print(f"FOV: {client.request('vget /camera/0/horizontal_fieldofview')}")
print("Pausing...")
time.sleep(2)
print("Starting capture.")
if args.recapture:
    pattern = re.compile("[0-9]+_rgb.png")
    num_im = len(
            [path for path in os.listdir(args.output_dir)
                if pattern.match(path) is not None])
    rng = range(num_im)
else:
    rng = range(1000, 3000)

for n in rng:
    #  save_pose("/data/views4/RealisticRendering", f"{n:04}")
    save_pose(args.output_dir, f"{n:04}", args.recapture)
    #  save_pose(args.output_dir, f"{n:04}", False)
    time.sleep(0.1)
