import os
import cv2
import numpy as np
import torch

from pathlib import Path
import argparse

import matplotlib.pyplot as plt

#  from cpaint import overlap_lib
from cpaint import datasets
from cpaint.core.detector_util import mask_border
import pyoctree


def main(dataset, octomap_dir, octomap_res):
    print(f"Generating octomap for: {dataset.name}")
    # ==================== Grab paths and load results =====================
    depth_maps = []
    pose_paths = []
    poses = []
    #  dataset.desired_size = [120, 120]

    print(f"Calib (fx, fy, cx, cy): {dataset.get_calib()}")
    avg_dep = 0
    for i, (rgb_path, depth_path, pose_path, basename) in enumerate(dataset):
        if hasattr(dataset, "repeat_length") and i >= dataset.repeat_length:
            break
        pose_paths.append(pose_path)
        poses.append(torch.tensor(dataset.load_pose(i)).float())
        depth = dataset.load_depth_image(i)

        tens = torch.tensor(depth.astype(np.float32))
        # Mask out border for accuracy
        #  mask = mask_border(tens)
        #  tens *= mask.squeeze()
        #  plt.imshow(tens)
        #  plt.show()
        depth_maps.append(tens)
        avg_dep += tens.mean()

    avg_dep = avg_dep / len(dataset)
    print(f"Avg Dep: {avg_dep}")
    print(f"Length: {len(pose_paths)}")
    # Run dat C++ code from overlap_lib
    octomap_path = dataset.get_octomap_path(octomap_dir)
    os.makedirs(os.path.dirname(octomap_path), exist_ok=True)

    pyoctree.generate_octomap(
        depth_maps, poses, octomap_path, dataset.get_calib(), octomap_res)
    print(f"Saved octomap to {octomap_path}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("--octomap_res", type=float, default=None, help="Resolution of octomap in meters")

    parser.add_argument("--seperate-files", action="store_true",
                        help="Seperate the new scores from the results")
    parser.add_argument("--display", action="store_true",
                        help="Display the results and don't save them")
    args = parser.parse_args()

    #  for dataset in datasets.DatasetList(args.dataset_dir, args.dataset, desired_size=(288, 288)):
    #  for dataset in datasets.DatasetList(args.dataset_dir, args.dataset, desired_size=(144, 144)):
    for dataset in datasets.DatasetList(args.dataset_dir, args.dataset):
        voxel_size = dataset.voxel_size if args.octomap_res is None else args.octomap_res
        main(dataset, args.dataset_dir, voxel_size)

