# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Reads average counts for area

import os
import cv2
import numpy as np
import torch
import math

from pathlib import Path
import argparse

from tqdm import tqdm
from icecream import ic

import matplotlib.pyplot as plt

from cpaint.core import image_ops
from cpaint import overlap_lib
from cpaint import datasets
from cpaint.core import paths

def paint_maps(dataset, painted_path):
    # Convert score maps to keypoints
    print(f"Loading from {painted_path}")
    accum = 0
    total = 0
    N = len(dataset)
    for i in tqdm(range(N)):
        reproj_path, counts_path, _ = paths.get_processed_paths(i, painted_path)

        counts = cv2.imread(counts_path, -1)
        if counts is None:
            print(f"Failed to read counts from {counts_path}")
        accum += int(counts.sum())
        total += math.prod(counts.shape)
    print(f"Average count is: {accum/total}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("prefix", type=str, default="test", help="Saving prefix for output. Becomes gt_source argument")

    parser.add_argument("--display", action="store_true",
                        help="Display the results and don't save them")
    args = parser.parse_args()


    for dataset in datasets.DatasetList(args.dataset_dir, args.dataset):
        paint_maps(dataset, paths.get_painted_dir(args.output_dir, dataset.name, args.prefix))

