# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import cv2
import toml
import numpy as np
import matplotlib.pyplot as plt
import argparse
import random
import copy

import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables import Keypoint, KeypointsOnImage
from imgaug.augmentables.heatmaps import HeatmapsOnImage
from imgaug.augmentables.segmaps import SegmentationMapsOnImage

from cpaint.core import paths, config
from cpaint import datasets



def find_painter_output_threshold(args, desired_num_detections, gt_source, **kwargs):
    # Sample datasets
    num_per_ds = 50

    largs = copy.deepcopy(args)
    if type(largs.desired_size[0]) == list:
        largs.desired_size = largs.desired_size[0]
    largs.binary_target = False
    largs.threshold = None
    largs.color_input = True
    largs.train_descriptors = False
    largs.augmentation = False
    largs.blur = False
    largs.motion_blur = False
    largs.gt_source = gt_source
    largs.cache_dataset = False
    largs.cpu_max_pool = True

    ds = datasets.TrainDatasetList(largs.dataset, largs, )

    print("Finding threshold")
    outputs = []
    points = 0
    for d in ds:
        j = 0
        N = min(len(d), num_per_ds)
        for _ in range(len(d)):
            if j > N:
                break
            i = random.randrange(0, len(d))
            # Get painter output
            im, _, nmsed, mask = d[i]
            if mask.mean() <= 200/240/240:
                continue

            to_sort = np.array(nmsed*mask)
            to_sort = to_sort[np.where(to_sort > 0)]
            sorted_out = -np.sort(-to_sort, axis=None)
            dnd = int(desired_num_detections*mask.mean()+0.5)
            if dnd < 1:
                continue
            if len(sorted_out) <= dnd:
                points += len(sorted_out)
                thres = 0
            else:
                points += dnd
                thres = sorted_out[dnd]
            #  if args.display:
            if False:
                im = (im.permute(1, 2, 0).numpy()*255).astype(np.uint8)
                sm = HeatmapsOnImage(nmsed.numpy().squeeze()*100, shape=nmsed.squeeze().shape, min_value=0, max_value=1)
                mask_i = SegmentationMapsOnImage(mask.numpy().squeeze().astype(np.bool), shape=mask.shape)
                cells = []
                cells.append(im)
                cells.append(mask_i.draw()[0])
                cells.append(sm.draw()[0])
                grid_image = ia.draw_grid(cells, cols=3)
                fig = plt.figure()
                fig.suptitle(f"Len Sorted Out: {len(sorted_out)}, Num extracting: {dnd}")
                plt.imshow(grid_image)
                plt.show()

            outputs.append(thres)
            j += 1
    threshold = np.mean(outputs)

    largs.threshold = threshold
    largs.binary_target = True
    ds = datasets.TrainDatasetList(largs.dataset, largs, )

    n = 0
    num_pts = 0
    mask_sum = 0
    for d in ds:
        for _ in range(3*num_per_ds):
            if n > num_per_ds:
                break
            i = random.randrange(0, len(d))
            # Get painter output
            im, _, nmsed, mask = d[i]
            if mask.mean() <= 200/240/240:
                continue
            mask_sum += mask.mean()
            #  plt.imshow(nmsed.squeeze())
            #  plt.show()
            num_pts += (nmsed).int().sum()
            n += 1
    pt_yield = float(num_pts)/float(n)
    print(f"Threshold yields {pt_yield} per image on average")
    print(f"Accounting for masking, it yields {pt_yield/mask_sum*n} per image on average")
    #  print(f"Accounting for masking, it yields {pt_yield} per image on average")
    # Sort output and return threshold
    return threshold

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("computer_config", type=str, help="Path to computer specific config")
    parser.add_argument("net_config", type=str, help="Path to net config")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument('gt_source', type=str,
                        help='Type of source for the GT. Check datasets/__init__.py')
    parser.add_argument("--desired_num_detections", type=int, help="Number of desired output detections", default=100)
    parser.add_argument('--display', action='store_true', default=False,
                        help='Display debugging view')

    # Look pass a config otherwise it doesn't work

    args = parser.parse_args()
    config.augment_args(args, args.computer_config)
    config.augment_args(args, args.net_config)

    print("Computing output threshold")
    out_thres = find_painter_output_threshold(args, args.desired_num_detections, args.gt_source)
    print(f"Output threshold: {out_thres}")
