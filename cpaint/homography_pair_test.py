# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import math
import cv2
import numpy as np

from pathlib import Path
import argparse

from tqdm import tqdm
from icecream import ic

import matplotlib.pyplot as plt

from cpaint.core.util3d import calib_to_K
from cpaint.core import image_ops
from cpaint import datasets
from cpaint.core import paths

MIN_MATCH_COUNT = 10
ERR_INSUFFICIENT_PTS = "Not enough good points"
ERR_ESSENTIAL_MATRIX = "Failed to solve for essential matrix"
ERR_SOLVING_POSE = "Failed to recover pose"

def visualize_matches(desc1, desc2, pts1, pts2, im1, im2, K, norm_type, crossCheck=False):
    #  print(pts1, pts2)
    #  print([(p[1], p[0], 2) for p in pts1])
    if pts1.shape[0] == 0 or pts2.shape[0] == 0:
        return np.concatenate((im1, im2), axis=1)
    key_pts1 = [cv2.KeyPoint(p[1].item(), p[0].item(), _size=2) for p in pts1]
    key_pts2 = [cv2.KeyPoint(p[1].item(), p[0].item(), _size=2) for p in pts2]
    draw_params = dict(matchColor = (0,255,0),
                   singlePointColor = (255,0,0),
                   flags = cv2.DrawMatchesFlags_DEFAULT)

    if crossCheck:
        matcher = cv2.BFMatcher(norm_type, crossCheck=True)
        matches = matcher.match(desc1,desc2)
        mimg = cv2.drawMatches(im1,key_pts1,im2,key_pts2,matches,None,**draw_params)
    else:
        matcher = cv2.BFMatcher(norm_type)
        matches = matcher.knnMatch(desc1,desc2,k=2)
        # Need to draw only good matches, so create a mask
        matchesMask = [[0,0] for i in range(len(matches))]
        try:
            # ratio test as per Lowe's paper
            for i,(m,n) in enumerate(matches):
                #  if m.distance < 0.9*n.distance:
                if m.distance < 0.7:
                    matchesMask[i]=[1,0]
        except Exception as e:
            print(e)
            return np.concatenate((im1, im2), axis=1)
        mimg = cv2.drawMatchesKnn(im1,key_pts1,im2,key_pts2,matches,None,matchesMask = matchesMask,**draw_params)
    return mimg

def create_point_pairs(desc1, desc2, pts1, pts2, norm_type):
    #  matcher = cv2.BFMatcher(norm_type, crossCheck=True)
    matcher = cv2.BFMatcher(norm_type)
    #  matcher = cv2.FlannBasedMatcher({"algorithm":1, "trees":5}, {"checks":50})
    matches = matcher.knnMatch(desc1, desc2, k = 2)
    # Apply Lowe's ratio test
    good = []
    for m, n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
    # Get points
    qi = [m.queryIdx for m in good]
    ti = [m.trainIdx for m in good]
    src_pts = pts1[qi] # (-1, 2)
    dst_pts = pts2[ti] # (-1, 2)
    return src_pts, dst_pts

def get_pose_diff(desc1, desc2, pts1, pts2, K, norm_type):
    src_pts, dst_pts = create_point_pairs(desc1, desc2, pts1, pts2, norm_type)
    num_matches = src_pts.shape[0]
    if num_matches < MIN_MATCH_COUNT:
        return ERR_INSUFFICIENT_PTS, None, num_matches
    src_pts = src_pts.reshape(-1, 1, 2)
    dst_pts = dst_pts.reshape(-1, 1, 2)
    try:
        E, mask = cv2.findEssentialMat(src_pts.astype(np.float32), dst_pts.astype(np.float32), K, cv2.RANSAC)
    except Exception as e:
        return ERR_ESSENTIAL_MATRIX, None, num_matches
    try:
        retval, R, t, _ = cv2.recoverPose(E, src_pts.astype(np.float32), dst_pts.astype(np.float32), K, mask)
    except:
        return ERR_SOLVING_POSE, None, num_matches
    est_pose_diff = np.eye(4)
    est_pose_diff[:3, 3] = t.squeeze()
    est_pose_diff[:3, :3] = R
    return None, est_pose_diff, num_matches

def isclose(x, y, rtol=1.e-5, atol=1.e-8):
    return abs(x-y) <= atol + rtol * abs(y)

def euler_angles_from_rotation_matrix(R):
    '''
    Code by crmccreary from github
    From a paper by Gregory G. Slabaugh (undated),
    "Computing Euler angles from a rotation matrix
    '''
    phi = 0.0
    if isclose(R[2,0],-1.0):
        theta = math.pi/2.0
        psi = math.atan2(R[0,1],R[0,2])
    elif isclose(R[2,0],1.0):
        theta = -math.pi/2.0
        psi = math.atan2(-R[0,1],-R[0,2])
    else:
        theta = -math.asin(R[2,0])
        cos_theta = math.cos(theta)
        psi = math.atan2(R[2,1]/cos_theta, R[2,2]/cos_theta)
        phi = math.atan2(R[1,0]/cos_theta, R[0,0]/cos_theta)
    return psi, theta, phi

def getEstimationError(pose_diff, est_pose_diff):
    # Decompose matrix into translation and rotation
    trans_diff = pose_diff[:3, 3]
    rot_diff = pose_diff[:3, :3]
    est_trans_diff = est_pose_diff[:3, 3]
    est_rot_diff = est_pose_diff[:3, :3]

    # Calculate error
    dist = np.linalg.norm(trans_diff-est_trans_diff, ord=2)
    angs = euler_angles_from_rotation_matrix(np.linalg.inv(rot_diff).dot(est_rot_diff))
    ang_diff = sum([abs(i) for i in angs])
    return dist, min(abs(2*math.pi-ang_diff), ang_diff)*180/math.pi

def homography_pair_test(args, dataset, painted_path, detector):
    # ==================== Grab paths and load results =====================
    poses = []
    detections = []
    descriptors = []
    for i, (rgb_path, depth_path, pose_path, basename) in enumerate(dataset):
        # TODO augmentation?
        rgb = dataset.load_rgb_image(i)
        gray = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
        #  gray = image_ops.scale_then_crop(gray, result_shape)
        score_map = detector.predict(gray)

        detections.append(np.array(np.where(score_map>0.1)).T)
        descriptors.append(None) # TODO
        poses.append(dataset.load_pose(i))

    pairs = [] # TODO, should only be for overlapping images
    calib = dataset.get_calib()
    K = calib_to_K(calib)
    trans_errs = []
    ang_errs = []
    for (i, j) in pairs:
        err, est_pose_diff = get_pose_diff(descriptors[i], descriptors[j], detections[i], detections[j], K)
        if err is not None:
            return None
        pose_diff = np.linalg.inv(poses[i]).dot(poses[j])
        dist, ang_diff = getEstimationError(pose_diff, est_pose_diff)
        trans_errs.append(dist)
        ang_errs.append(ang_diff)
    print(np.mean(trans_err))
    print(np.mean(ang_err))
    # TODO What do with result?

def test_matching():
    # Hand select points on a pair of images of known pose
    # Calculate homography
    # Calculate error
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Dataset to export detections on")
    parser.add_argument("dataset_dir", type=str, help="Path to datasets. Check \
                        readme.", default="/externd/datasets")
    parser.add_argument("output_dir", type=str, help="Directory with csv \
                        outputs. Check readme")
    parser.add_argument("prefix", type=str, default="test", help="Saving prefix for output")

    parser.add_argument("--display", action="store_true",
                        help="Display the results and don't save them")
    args = parser.parse_args()

    from cpaint.core.detectors import DETECTORS

    detector = DETECTORS['superpoint']()
    result_shape = (480, 480)

    #  for dataset in tqdm(datasets.DatasetList(args.dataset_dir, args.dataset)):

