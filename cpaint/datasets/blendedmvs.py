# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import random
import re
import numpy as np
import math
import json
import cv2
from pyquaternion import Quaternion
from cpaint.datasets.base import BaseDataset
from io import StringIO

import torch
import torch.nn.functional as F
from cpaint.core import image_ops

# these have frames with no pose

AREAS = [
    "59f70ab1e5c5d366af29bf3e",
    "5c1f33f1d33e1f2e4aa6dda4",
    "57f8d9bbe73f6760f10e916a",
    "59338e76772c3e6384afbb15",
    "5a0271884e62597cdee0d0eb",
    "59056e6760bb961de55f3501",
    "59f87d0bfa6280566fb38c9a",
    "5b271079e0878c3816dacca4"
]

# This is unused. Just to infer voxel size
AVERAGE_DEPTH = {
    "59f70ab1e5c5d366af29bf3e": 0.7895880937576294,
    "5c1f33f1d33e1f2e4aa6dda4": 0.8525557518005371,
    "57f8d9bbe73f6760f10e916a": 0.8558391332626343,
    "59338e76772c3e6384afbb15": 6.555761337280273,
    "5a0271884e62597cdee0d0eb": 28.80898094177246,
    "59056e6760bb961de55f3501": 18.00514030456543,
    "59f87d0bfa6280566fb38c9a": 6.142978191375732
}

# These might not be optimized for voxel count
# These are found by finding the minimum voxel size such that
# the surfaces don't have holes
VOXEL_SIZE = {
    "59f70ab1e5c5d366af29bf3e": 0.001,
    "5c1f33f1d33e1f2e4aa6dda4": 0.001,  # verified
    "57f8d9bbe73f6760f10e916a": 0.0004,  # verified
    "59338e76772c3e6384afbb15": 0.01,  # verfied
    "5a0271884e62597cdee0d0eb": 0.03,  # verified
    "59056e6760bb961de55f3501": 0.1,  # verified
    "59f87d0bfa6280566fb38c9a": 0.008,  # verified
    "5b271079e0878c3816dacca4": 0.05  # verified
}

POS_OFFSET = {
    "59056e6760bb961de55f3501": np.array([-1.005029, -1.134990, 812.724060]),
    "59338e76772c3e6384afbb15": np.array([-9.270395, -0.324132, 147.538712]),
    "5a0271884e62597cdee0d0eb": np.array([-9.270395, -53, -48]),
    "5b271079e0878c3816dacca4": np.array([47.971004, -61.766495, 198.812897]),
}

ALL_AREAS = [
    "57f8d9bbe73f6760f10e916a", "59e864b2a9e91f2c5529325f", "5a572fd9fc597b0478a81d14",
    "5ab8713ba3799a1d138bd69a", "5b3b353d8d46a939f93524b9", "5bb7a08aea1cfa39f1a947ab",
    "5bf03590d4392319481971dc", "5c1af2e2bee9a723c963d019",
    "58c4bb4f4a69c55606122be4", "59ecfd02e225f6492d20fcc9", "5a57542f333d180827dfc132",
    "5ab8b8e029f5351f7f2ccf59", "5b4933abf2b5f44e95de482a", "5bb8a49aea1cfa39f1aa7f75",
    "5bf17c0fd439231948355385", "5c1b1500bee9a723c96c3e78", "5bfd0f32ec61ca1dd69dc77b",
    "58cf4771d0f5fb221defe6da", "59f363a8b45be22330016cad", "5a588a8193ac3d233f77fbca",
    "5abc2506b53b042ead637d86", "5b558a928bbfb62204e77ba2", "5bbb6eb2ea1cfa39f1af7e0c",
    "5bf18642c50e6f7f8bdbd492", "5c1dbf200843bc542d8ef8c4", "5bfe5ae0fe0ea555e6a969ca",
    "58d36897f387231e6c929903", "59f70ab1e5c5d366af29bf3e", "5a618c72784780334bc1972d",
    "5acf8ca0f3d8a750097e4b15", "5b60fa0c764f146feef84df0", "5bc5f0e896b66a2cd8f9bd36",
    "5bf21799d43923194842c001", "5c1f33f1d33e1f2e4aa6dda4", "5bff3c5cfe0ea555e6bcbf3a",
    "58eaf1513353456af3a1682a", "59f87d0bfa6280566fb38c9a", "5a6400933d809f1d8200af15",
    "5adc6bd52430a05ecb2ffb85", "5b62647143840965efc0dbde", "5bccd6beca24970bce448134",
    "5bf26cbbd43923194854b270", "5c20ca3a0843bc542d94e3e2", "5c062d84a96e33018ff6f0a6",
    "58f7f7299f5b5647873cb110", "5a0271884e62597cdee0d0eb", "5a6464143d809f1d8208c43c",
    "5ae2e9c5fe405c5076abc6b2", "5b69cc0cb44b61786eb959bf", "5bce7ac9ca24970bce4934b6",
    "5bf3a82cd439231948877aed", "5c2b3ed5e611832e8aed46bf", "5c0d13b795da9479e12e2ee9",
    "59056e6760bb961de55f3501", "5a3ca9cb270f0e3f14d0eddb", "5a69c47d0d5d0a7f3b2e9752",
    "5af02e904c8216544b4ab5a2", "5b6e716d67b396324c2d77cb", "5bcf979a6d5f586b95c258cd",
    "5bf7d63575c26f32dbf7413b", "5c34300a73a8df509add216d", "5c1892f726173c3a09ea9aeb",
    "59338e76772c3e6384afbb15", "5a3cb4e4270f0e3f14d12f43", "5a7d3db14989e929563eb153",
    "5af28cea59bc705737003253", "5b6eff8b67b396324c5b2672", "5bd43b4ba6b28b1ee86b92dd",
    "5bfc9d5aec61ca1dd69132a2", "5c34529873a8df509ae57b58", "5c189f2326173c3a09ed7ef3"
    "59350ca084b7f26bf5ce6eb8", "5a3f4aba5889373fbbc5d3b5", "5a8315f624b8e938486e0bd8",
    "5afacb69ab00705d0cefdd5b", "5b78e57afc8fcf6781d0c3ba", "5be3a5fb8cfdd56947f6b67c",
    "5947719bf1b45630bd096665", "5a489fb1c7dab83a7d7b1070", "5a8aa0fab18050187cbe060e",
    "5b08286b2775267d5b0634ba", "5b7a3890fc8fcf6781e2593a", "5be3ae47f44e235bdbbc9771",
    "5947b62af1b45630bd0c2a02", "5a48ba95c7dab83a7d7b44ed", "5a969eea91dfc339a9a3ad2c",
    "5b192eb2170cf166458ff886", "5b864d850d072a699b32f4ae", "5be47bf9b18881428d8fbc1d",
    "59817e4a1bd4b175e7038d19", "5a48c4e9c7dab83a7d7b5cc7", "5aa0f9d7a9efce63548c69a1",
    "5b21e18c58e2823a67a10dd8", "5b908d3dc6ab78485f3d24a9", "5be4ab93870d330ff2dce134",
    "599aa591d5b41f366fed0d58", "5a48d4b2c7dab83a7d7b9851", "5aa235f64a17b335eeaf9609",
    "5b22269758e2823a67a3bd03", "5b950c71608de421b1e7318f", "5be883a4f98cee15019d5b83",
    "59d2657f82ca7774b1ec081d", "5a4a38dad38c8a075495b5d2", "5aa515e613d42d091d29d300",
    "5b271079e0878c3816dacca4", "5ba19a8a360c7c30c1c169df", "5bea87f4abd34c35e1860ab5",
    "59e75a2ca9e91f2c5526005d", "5a563183425d0f5186314855", "5ab85f1dac4291329b17cb50",
    "5b2c67b5e0878c381608b8d8", "5ba75d79d76ffa2c86cf2f05", "5beb6e66abd34c35e18e66b9",
]

def read_matrix(path, tag, rows):
    with open(path, "r") as f:
        ret = np.zeros((4, 4), np.float)
        for l in f:
            if tag in l:
                data = ""
                for i in range(rows):
                    data += f.readline()
                return np.loadtxt(StringIO(data))
    raise Exception(f"Tag {tag} not found in {path}")

class BlendedMVS(BaseDataset):

    def _init(self, basepath, dname):
        self.voxel_size = VOXEL_SIZE[dname]
        self.version = "BlendedMVS"
        self.type = "BlendedMVS"

        self.rgb_paths = []
        self.depth_paths = []
        self.pose_paths = []

        area_path = os.path.join(basepath, self.type, "areas", dname)

        # Here is where we handle if the full dataset consists of multiple seasons. All of them have the directory format:
        # Parent/Child
        self.main_area = self.dname
        max_length = 3000

        pattern = re.compile("([0-9]+).pfm")
        inds = []
        for path in os.listdir(os.path.join(area_path, "rendered_depth_maps")):
            match = pattern.match(path)
            if match is not None:
                i = int(match.groups(0)[0])
                inds.append(i)
        inds.sort()
        self.repeat_length = len(inds)
        self.repeat_length = min(max_length, self.repeat_length)

        self.rgb_paths, self.depth_paths, self.pose_paths = [], [], []
        # We don't need to load the masked versions because it will be handled automatically by
        # voxel size masking
        self.rgb_paths.extend([os.path.join(area_path, "blended_images", f"{inds[i]:08d}.jpg")
                          for i in range(self.repeat_length)])
        self.depth_paths.extend([os.path.join(area_path, "rendered_depth_maps", f"{inds[i]:08d}.pfm")
                            for i in range(self.repeat_length)])
        self.pose_paths.extend([os.path.join(area_path, "cams", f"{inds[i]:08d}_cam.txt")
                           for i in range(self.repeat_length)])

        K = read_matrix(self.pose_paths[0], "intrinsic", 3)
        self.default_calib = [K[0, 0], K[1, 1], K[0, 2], K[1, 2]]

    def get_depth_factor(self):
        return 1

    def __len__(self):
        # There are 3 files for each frame. rgb, depth, pose
        return len(self.rgb_paths)

    def __getitem__(self, i):
        fname = os.path.basename(self.rgb_paths[i])
        basename = fname.split(".")[0]
        return self.rgb_paths[i], self.depth_paths[i], self.pose_paths[i], basename

    def load_pose(self, i):
        path = self.pose_paths[i]
        pose = read_matrix(path, "extrinsic", 4)
        pose = np.linalg.inv(pose)
        if self.dname in POS_OFFSET:
            pose[:3, 3] -= POS_OFFSET[self.dname]
        return pose

    #  def load_depth_image(self, i):
    #      dep = super().load_depth_image(i)
    #      H, W = dep.shape
    #      # Ya see, they had a mask on the depth image, then performed linear interp
    #      tens = torch.tensor(dep).view(1, 1, H, W)
    #      tens == 0
    #      max_v = F.max_pool2d(
    #          F.pad(tens, [2] * 4, mode='constant', value=-1.),
    #          5, stride=1)
    #      min_v = -F.max_pool2d(
    #          F.pad(-tens, [2] * 4, mode='constant', value=1.),
    #          5, stride=1)
    #      mask = max_v/min_v < 1.03
    #      tens *= mask.float()
    #      return tens.numpy().squeeze()

    def _load_depth_image(self, i):
        depth_path = self.depth_paths[i]
        depth = cv2.imread(str(depth_path), -1)
        return depth

    def _load_rgb_image(self, i):
        rgb_path = self.rgb_paths[i]
        rgb = cv2.imread(str(rgb_path), cv2.IMREAD_COLOR)
        return rgb

