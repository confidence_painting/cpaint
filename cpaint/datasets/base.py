# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re
import cv2
import random
import numpy as np
import math

import torch
from cpaint.core import image_ops

from abc import ABCMeta, abstractmethod

# Your dataset should also come with a list of areas called AREAS

class BaseDataset(metaclass=ABCMeta):

    def __init__(self, basepath, dname, default_shape="square", desired_size=None, overlap_thres=None, output_dir=None):
        super().__init__()
        self.basepath = basepath
        self.dname = dname
        self.name = f"{dname}"

        self._init(basepath, dname)

        # Find the original image size. We assume uniform image size across
        # images, both depth and rgb.
        self.original_size = self._load_rgb_image(0).shape[:2]

        # Set desired size if it hasn't been set
        if desired_size is not None:
            self.desired_size = desired_size
        else:
            if default_shape == "square":
                self.desired_size = self.get_largest_square()
            elif default_shape == "original":
                self.desired_size = self.original_size
        print(f"Calib (fx, fy, cx, cy): {self.get_calib()}")

        # Set repeat length if it has not yet been set
        # Used for datasets with multiple images of the same thing using
        # different variables
        if not hasattr(self, "repeat_length"):
            self.repeat_length = len(self)

        # Here is where we add information if this dataset is part of a multiseason dataset
        if not hasattr(self, "multi_data"):
            self.multi_data = None
            self.child = None

        if overlap_thres is not None:
            from cpaint.benchmark import precompute_pairs
            pair_map = precompute_pairs.retrieve_pair_map(output_dir, self.name)
            self.pair_map = np.minimum(pair_map, pair_map.T) - np.eye(pair_map.shape[0])
            if pair_map.shape[0] > len(self):
                pair_map = pair_map[:len(self), :len(self)]
            print(f"Pair map shape: {pair_map.shape}, number images: {len(self)}")
            self.inds = np.array(np.where(np.triu(self.pair_map>=overlap_thres, 1))).T # (N, 2)
            print(f"Involving: {(np.sum(np.triu(self.pair_map>=overlap_thres, 1), axis=0) > 0).sum()} images")
            print(f"Num images: {self.pair_map.shape[0]}")
            print(f"Num pairs: {self.inds.shape[0]}")

    @abstractmethod
    def _init(self, basepath, dname):
        """To be implemented by the child class."""
        """ Must set self.main_area to something """
        """ Must set self.default_calib to something """
        """ Must set self.voxel_size to something """
        """ Must set self.version to something """
        """ Must set self.type to something """
        raise NotImplementedError

    @abstractmethod
    def get_depth_factor(self):
        """To be implemented by the child class."""
        raise NotImplementedError

    @abstractmethod
    def __len__(self):
        """To be implemented by the child class."""
        raise NotImplementedError

    @abstractmethod
    def __getitem__(self, i):
        """To be implemented by the child class."""
        """ Returns rgb_path, depth_path, pose_path, basename """
        raise NotImplementedError

    @abstractmethod
    def load_pose(self, i):
        """To be implemented by the child class."""
        """ Returns a 4x4 matrix pose """
        raise NotImplementedError

    @abstractmethod
    def _load_depth_image(self, i):
        """To be implemented by the child class."""
        """ Ignores desired size and depth factor"""
        raise NotImplementedError


    @abstractmethod
    def _load_rgb_image(self, i):
        """To be implemented by the child class."""
        """ Ignores desired size """
        raise NotImplementedError

    @property
    def is_part_of_multi_season(self):
        return "/" in self.dname

    def get_largest_square(self):
        min_dim = min(self.original_size)
        return [min_dim, min_dim]

    def get_pair(self, pair_idx):
        i, j = self.inds[pair_idx % self.repeat_length]
        return i, j

    def get_num_pairs(self):
        num_folders = len(self) // self.repeat_length
        return self.inds.shape[0] * num_folders

    def get_voxel_mask(self, dep):
        # Calculate mask based on voxel size
        # mask needs to be eroded additional times based on self.post_augmentation_erosion
        pix_size = dep / dep.shape[0]
        pix_size[pix_size == 0] = 1e6
        pixels_per_voxel = np.floor(self.voxel_size/pix_size)
        pixels_per_voxel[np.isnan(pixels_per_voxel)] = 0
        pixels_per_voxel[np.isinf(pixels_per_voxel)] = 0
        biggest_voxel = int(np.max(pixels_per_voxel))
        biggest_voxel = min(biggest_voxel, dep.shape[0]//4)
        mask = (pixels_per_voxel <= 1).astype(np.uint8)
        #  mask = cv2.erode(mask, np.ones((3, 3), np.uint8), iterations=int(np.max(pixels_per_voxel))-self.post_augmentation_erosion)
        mask = (cv2.erode(mask, np.ones((3, 3), np.uint8), iterations=biggest_voxel) > 0.01).astype(np.uint8)
        return mask

    def get_calib(self):
        ori = self.original_size
        long_dim = ori.index(max(ori))
        short_dim = ori.index(min(ori))

        resize_ratio = self.desired_size[short_dim] / ori[short_dim]
        # fx fy cx cy
        new_calib = np.array(self.default_calib)*resize_ratio
        # Adjust cx, cy if the image has been center cropped
        long_diff = self.desired_size[long_dim] - (ori[long_dim] * resize_ratio)
        # have to reverse index to go from row col to x y
        new_calib[(1 - long_dim)+2] += long_diff // 2
        return list(new_calib)

    def get_octomap_path(self, octomap_dir):
        return os.path.join(octomap_dir, f"{self.type}/octomaps/{self.main_area}.oct")

    def load_depth_image(self, i):
        depth = self._load_depth_image(i)
        depth /= self.get_depth_factor()
        short_dim = self.original_size.index(min(self.original_size))
        depth = image_ops.scale_then_crop(depth, self.desired_size, interp=cv2.INTER_NEAREST)

        return depth

    def load_rgb_image(self, i):
        rgb = self._load_rgb_image(i)
        rgb = image_ops.scale_then_crop(rgb, self.desired_size)
        return rgb

