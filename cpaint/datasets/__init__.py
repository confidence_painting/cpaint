#  from . import scannet
#  from . import gibson
from . import unrealcv
from . import blendedmvs
from . import gt_source
from cpaint.core import paths
from cpaint.core import image_ops
import random


DATASET_LIST = []
#  DATASET_LIST.extend(scannet.AREAS)
#  DATASET_LIST.extend(gibson.AREAS)
DATASET_LIST.extend(unrealcv.AREAS)
DATASET_LIST.extend(blendedmvs.AREAS)
CATEGORIES = {
    #  "scannet": scannet.AREAS,
    #  "gibson": gibson.AREAS,
    "unrealcv": unrealcv.AREAS,
    "blendedmvs": blendedmvs.AREAS,
    "unreal_blended": blendedmvs.AREAS+unrealcv.AREAS,
    "all": DATASET_LIST,
}
for category in CATEGORIES:
    assert category not in DATASET_LIST

def get_dataset(dataset_dir, dname, **kwargs):
    #  if dname in scannet.AREAS:
    #      return scannet.ScannetDataset(dataset_dir, dname, **kwargs)
    #  elif dname in gibson.AREAS:
    #      return gibson.GibsonDataset(dataset_dir, dname, **kwargs)
    #  elif dname in unrealcv.AREAS:
    if dname in unrealcv.AREAS:
        return unrealcv.UnrealCVDataset(dataset_dir, dname, **kwargs)
    if dname in blendedmvs.AREAS:
        return blendedmvs.BlendedMVS(dataset_dir, dname, **kwargs)
    else:
        print(f"Dataset '{dname}' not found")
        return None

def get_training_set(dname, args):
    if type(args.gt_source) == list:
        dat = [dat for dat in args.gt_source if dat["area"] == dname][0]
        gts = paths.name_prefix(dat["prefix"], dat["run_num"])
        args.painted_path = paths.get_painted_dir(args.cache_dir, dname, gts)
        threshold = dat["threshold"]
        overlap_thres = dat["overlap_thres"]
    else:
        args.painted_path = paths.get_painted_dir(args.cache_dir, dname, args.gt_source)
        threshold = args.threshold
        overlap_thres = args.overlap_thres
    if args.train_descriptors:
        dataset = get_dataset(args.dataset_dir, dname, overlap_thres=overlap_thres, output_dir=args.output_dir)
        return gt_source.DescDetWrapper(dataset, args, threshold=threshold)
    else:
        dataset = get_dataset(args.dataset_dir, dname)
        return gt_source.DetectionWrapper(dataset, args, threshold=threshold)

class Iterator(object):
    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def __len__(self):
        return len(self.names)


class DatasetList(Iterator):
    def __init__(self, dataset_dir, dname, **kwargs):
        if dname not in CATEGORIES.keys():
            self.names = [dname]
        elif dname in CATEGORIES.keys():
            self.names = CATEGORIES[dname]
        self.i = 0
        self.dataset_dir = dataset_dir
        self.kwargs = kwargs

    def __iter__(self):
        return self

    def next(self):
        if self.i < len(self):
            cur, self.i = self.i, self.i+1
            return get_dataset(self.dataset_dir, self.names[cur], **self.kwargs)
        else:
            raise StopIteration()

class TrainDatasetList(Iterator):
    def __init__(self, dname, args):
        if dname in DATASET_LIST:
            self.names = [dname]
        elif dname in CATEGORIES.keys():
            self.names = CATEGORIES[dname]
        elif type(args.gt_source) == list:
            self.names = [dat["area"] for dat in args.gt_source]
        self.i = 0
        self.args = args

    def __len__(self):
        return len(self.names)

    def __getitem__(self, i):
        return get_training_set(self.names[i], self.args)

    def next(self):
        if self.i < len(self):
            cur, self.i = self.i, self.i+1
            return self[cur]
        else:
            raise StopIteration()
