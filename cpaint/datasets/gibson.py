import os
import random
import json
import cv2
from pyquaternion import Quaternion
from cpaint.core import image_ops
from torch.utils.data import Dataset


# these have frames with no pose

AREAS = [
    "Adrian", "Alfred", "Alstown", "American", "Annawan", "Anthoston",
    "Artois", "Ashport", "Assinippi", "Auburn", "Aulander", "Beach", "Belpre",
    "Booth", "Bountiful", "Bremerton", "Brentsville", "Brewton", "Broadwell",
    "Callicoon", "Calmar", "Cantwell", "Capistrano", "Carpendale", "Caruthers",
    "Cason", "Castroville", "Cebolla", "Chiloquin", "Circleville", "Clive",
    "Cobalt", "Codell", "Coeburn", "Connellsville", "Copemish", "Corder",
    "Cornville", "Coronado", "Dalcour", "Dauberville", "Dedham", "Deemston",
    "Destin", "Dryville", "Dunmor", "Eastville", "Espanola", "Ewansville",
    "Fitchburg", "Fleming", "Fredericksburg", "Frierson", "Funkstown", "Galatia",
    "Gasburg", "Graceville", "Grassy", "Hambleton", "Hartline", "Hendrix",
    "Herricks", "Hindsboro", "Holcut", "Hornsby", "Imbery", "Inkom", "Islandton",
    "Jacobus", "Jenners", "Kangley", "Kathryn", "Kerrtown", "Kinde", "Kremlin",
    "Landing", "Lenoir", "Lessley", "Lluveras", "Ludlowville", "Macedon",
    "Macksville", "Mahtomedi", "Maiden", "Marksville", "Maryhill", "Mayesville",
    "McClure", "McEwen", "Mesic", "Michiana", "Moark", "Moberly", "Mobridge",
    "Mosquito", "Mullica", "Munsons", "Nicut", "Ohoopee", "Ooltewah", "Orason",
    "Oriole", "Paige", "Pettigrew", "Pleasant", "Potosi", "Reserve",
    "Roane", "Ruckersville", "Sanctuary", "Scandinavia", "Schiller",
    "Schoolcraft", "Seatonville", "Seiling", "Shumway", "Smoketown", "Stokes",
    "Torrington", "Traver", "Tysons", "Umpqua", "Wappingers", "Wattsville",
    "Waucousta", "Winfield", "Wyatt", "Yankeetown", "Yscloskey"
]

BROKEN_AREAS = [
    "Alfred", "Cebolla", "Chiloquin", "Clive", "Codell", "Dedham", "Fitchburg",
    "Funkstown", "Graceville", "Hartline", "Hendrix", "Hindsboro", "Holcut",
    "Lessley", "Ludlowville", "Marksville", "McClure", "Mesic", "Moark",
    "Mosquito", "Ohoopee", "Orason", "Paige", "Potosi", "Reserve", "Ruckersville",
    "Sanctuary", "Scandinavia", "Schiller", "Seatonville", "Shumway", "Smoketown",
    "Torrington", "Tysons", "Umpqua", "Wappingers", "Waucousta", "Winfield",
    "Yankeetown",
]

AREAS = list(set(AREAS).difference(set(BROKEN_AREAS)))

class GibsonDataset(Dataset):

    def __init__(self, basepath, area, filtered_files=False, shape_type="square"):
        self.basepath = basepath
        self.area = area
        self.calib_path = ""
        self.name = f"gibson/{area}"
        self.version = "views3"
        self.type = "gibson"
        self.shape_type = shape_type

        self.rgb_paths = []
        self.depth_paths = []
        self.pose_paths = []

        num_im = len(os.listdir(os.path.join(
            basepath, f"{self.version}/gibson/{self.area}/rgb")))

        area_path = os.path.join(basepath,
                                 f"{self.version}/gibson/{self.area}")
        self.rgb_paths = [os.path.join(area_path, f"rgb/{i:07d}.png")
                          for i in range(num_im)]
        self.depth_paths = [os.path.join(area_path, f"depth/{i:07d}.tiff")
                            for i in range(num_im)]
        self.pose_paths = [os.path.join(area_path, f"metadata/{i:07d}.json")
                           for i in range(num_im)]

    def get_canonical_size(self):
        return [512, 512]

    def get_depth_factor(self):
        return 512

    def get_calib(self):
        # fx fy cx cy
        return [256.0, 256.0, 256.0, 256.0]

    def get_octomap_path(self, octomap_dir):
        return os.path.join(octomap_dir, f"{self.area}/mesh.bk.binvox.bt")

    def __len__(self):
        # There are 3 files for each frame. rgb, depth, pose
        return len(self.rgb_paths)

    def __getitem__(self, i):
        fname = os.path.basename(self.rgb_paths[i])
        basename = fname.split(".")[0]
        return self.rgb_paths[i], self.depth_paths[i], self.pose_paths[i], basename

    def load_pose(self, i):
        path = self.pose_paths[i]
        with open(path, "r") as f:
            dat = json.load(f)
            ori = dat["orientation"]
            pos = np.array(dat["position"])
            q = Quaterion(w=ori[3], x=ori[0], y=ori[0], z=ori[0])
            corr = Quaternion(axis=[1,0,0], degrees=-90) * \
                   Quaternion(axis=[0,1,0], degrees=90)
            ret = (q*corr).transformation_matrix
            ret[:3, 3] = pos
            return ret

    def load_depth_image(self, i):
        depth_path = self.depth_paths[i]
        if self.shape_type == "square":
            depth = cv2.imread(str(depth_path), -1)
            m_size = min(depth.shape[:2])
            depth = image_ops.scale_then_crop(depth, (m_size, m_size))
            # convert to planar depth
            depth /= self.get_depth_factor()
            return depth
        if self.shape_type == "original":
            depth = cv2.imread(str(depth_path), -1)
            # convert to planar depth
            depth /= self.get_depth_factor()
            return depth

    def load_rgb_image(self, i):
        rgb_path = self.rgb_paths[i]
        if self.shape_type == "square":
            rgb = cv2.imread(str(rgb_path), -1)
            m_size = min(rgb.shape[:2])
            rgb = image_ops.scale_then_crop(rgb, (m_size, m_size))
            return rgb
        if self.shape_type == "original":
            rgb = cv2.imread(str(rgb_path), -1)
            # convert to planar depth
            return rgb
