import torch
import torchvision.transforms.functional as TF

import time
import random
import cv2
import numpy as np
import matplotlib.pyplot as plt

from cpaint.core.image_ops import scale_then_crop, fast_interp, batch_fast_interp
from cpaint.core.image_transformer import Morpholgical
from cpaint.core import paths
from cpaint.core import util3d

import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables import Keypoint, KeypointsOnImage
from imgaug.augmentables.heatmaps import HeatmapsOnImage
from imgaug.augmentables.segmaps import SegmentationMapsOnImage

import cpaint.core.detector_util as det

from torch.utils.data import Dataset
from PIL import Image
from tqdm import tqdm


def resize_accurately(img, size):
    min_side = min(*img.shape[:2])
    if min_side % size[0] == 0:
        square_size = tuple([min(*img.shape[:2])]*2)
    else:
        multi = min_side // size[0]
        square_size = (size[0]*multi, size[1]*multi)
    img = scale_then_crop(img, square_size, cv2.INTER_NEAREST)
    img = fast_interp(img, size, False)
    return img


class DetectionWrapper(Dataset):

    #  def __init__(self, dataset, binary_target, desired_size, painted_path, threshold, color_input, augmentation):
    def __init__(self, dataset, args, threshold=None):
        self.dataset = dataset
        # Assign manually so we don't get any unpleasant runtime errors
        self.binary_target = args.binary_target
        self.painted_path = args.painted_path
        #  self.num_dets = num_dets
        self.color_input = args.color_input
        self.augmentation = args.augmentation
        self.post_augmentation_erosion = 3
        self.blur = args.blur
        self.mask_cellwise = args.mask_cellwise
        self.motion_blur = args.motion_blur
        self.maxpool_radius = args.maxpool_radius
        self.cache_dataset = args.cache_dataset
        self.gt_source = args.gt_source
        self.merge_seasons = args.merge_seasons
        self.main_season_relative_importance = args.main_season_relative_importance
        self.zero_data_zero_points = args.zero_data_zero_points
        self.output_dir = args.output_dir
        self.point_filtering_method = args.point_filtering_method

        self.desired_size = args.desired_size

        self.threshold = args.threshold if threshold is None else threshold
        self.mask_means = []
        if self.cache_dataset:
            if self.multiresolution:
                print("Caching dataset")
                self.cache = []
                for size in self.desired_size:
                    self.cache.append([])
                    for i in tqdm(range(len(self))):
                        self.cache[-1].append(self._load(i, size))
            else:
                print("Caching dataset")
                self.cache = []
                for i in tqdm(range(len(self))):
                    self.cache.append(self._load(i, self.desired_size))
            print(f"Percentage of dataset usable: {self.percent_usable}")
        self.size_to_largest()

    def size_to_largest(self):
        if self.multiresolution:
            largest_size = sorted(self.desired_size)[-1]
            self.size_index = self.desired_size.index(largest_size)

    @property
    def multiresolution(self):
        return type(self.desired_size[0]) == list

    @property
    def current_size(self):
        return self.desired_size[self.size_index]

    def create_augmentations(self, seed=None):
        # Because of multithreading, this needs to be init every. single. time.
        if seed is None:
            #  seed = random.randint(ia.random.SEED_MIN_VALUE, ia.random.SEED_MAX_VALUE)
            seed = int(time.time()*100000)

        # Literally the demo code
        seq = iaa.Sequential([
            iaa.OneOf([
                iaa.PerspectiveTransform(
                    scale=(0.05, 0.25),
                    seed=seed),
                iaa.Affine(
                    #  scale={"x": (0.4, 1.4), "y": (0.4, 1.4)},
                    scale=(0.4, 2.0),
                    translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)},
                    rotate=(-180, 180),
                    shear=(-30, 30),
                    seed=seed,
                ),
            ]),
            iaa.Fliplr(0.5),  # horizontal flips
            # But we only blur about 50% of all images.
            iaa.Sometimes(
                0.2,
                iaa.GaussianBlur(sigma=(0, 5.5)),
                seed=seed,
            ),
            iaa.Sometimes(
                0.5,
                iaa.SaltAndPepper(p=(0.0, 0.03), seed=seed),
                seed=seed,
            ),
            # Strengthen or weaken the contrast in each image.
            iaa.LinearContrast((0.75, 1.5), seed=seed),
            # Add gaussian noise.
            # For 50% of all images, we sample the noise once per pixel.
            # For the other 50% of all images, we sample the noise per pixel AND
            # channel. This can change the color (not only brightness) of the
            # pixels.
            iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5, seed=seed),
            # Make some images brighter and some darker.
            # In 20% of all cases, we sample the multiplier once per channel,
            # which can end up changing the color of the images.
            iaa.Multiply((0.8, 1.2), per_channel=0.2, seed=seed),
            iaa.Sometimes(
                0.5,
                iaa.JpegCompression(compression=(20, 60), seed=seed),
                seed=seed,
            ),
        ], random_order=False, seed=seed)  # apply augmenters in random order
        return seq

    def _load_painted(self, i, painted_path, size):

        reproj_path, counts_path, _ = paths.get_processed_paths(i, painted_path)
        reproj = cv2.imread(reproj_path, -1)
        counts = cv2.imread(counts_path, -1)
        if reproj is None:
            print(f"Could not load counts from {reproj_path}")
        if counts is None:
            print(f"Could not load counts from {counts_path}")

        reproj = scale_then_crop(reproj, size)
        counts = resize_accurately(counts, size)

        # Simple op to convert counts + reproj -> score map
        counts = np.where(counts < 0.5, 1, counts)
        painted = reproj/counts
        painted[np.isnan(painted)] = 0
        return painted, counts

    def _load(self, i, size):
        im = self.dataset.load_rgb_image(i)
        dep = self.dataset.load_depth_image(i)

        # We actually resize before the augmentation so we can use the resize to increase tolerance of large voxels
        im = scale_then_crop(im, size)
        dep = resize_accurately(dep, size)
        painted, counts = self._load_painted(i, self.painted_path, size)

        # Add custom masking here
        voxel_mask = self.dataset.get_voxel_mask(dep).astype(np.bool)
        count_mask = (cv2.erode((counts > 2).astype(np.uint8), np.ones((3, 3), np.uint8), iterations=2) > 0.01).astype(np.uint8)
        mask = np.bitwise_and(voxel_mask, count_mask)
        #  mask = voxel_mask
        # Add 1 so we know where the border of the image is
        self.mask_means.append(mask.mean())
        tf_mask = torch.tensor(mask).view(1, 1, mask.shape[-2], mask.shape[-1]).byte() + 1
        # Mask border
        tf_mask *= det.mask_border(tf_mask, border=2).byte()
        mask = tf_mask.numpy().squeeze()

        # Deal with the painted map
        if self.dataset.is_part_of_multi_season and type(self.gt_source) == list and self.merge_seasons:
            # If the dataset is part of a multi_season dataset, we want to merge the ground truth across seasons,
            # but only if we have access to multiple runs. That's where gt_source comes in
            other_painteds = []
            for season in self.dataset.multi_data["all"]:
                # Skip self, we already have painted
                if season == self.dataset.season:
                    continue
                # For each season this dataset is a part of, we retrieve the run data for that season
                dat = [dat for dat in self.gt_source if dat["area"] == season]
                if len(dat) == 0:
                    # We don't have data about that dataset
                    continue
                dat = dat[0]
                gts = paths.name_prefix(dat["prefix"], dat["run_num"])
                painted_path = paths.get_painted_dir(self.cache_dir, self.dname, gts)

                # We then load the painted map associated with that run
                other_painted, other_counts = self._load_painted(i, painted_path)
                other_painteds.append(other_painted)
            # Merge the painted maps

            # First idea: weighted sum
            # Advantage: very expressive.
            # Disadvantage: adds one parameter: main_season_relative_importance
            other_weight = 1/(self.main_season_relative_importance+len(other_painteds))
            main_weight = other_weight*self.main_season_relative_importance
            painted *= main_weight
            for op in other_painteds:
                painted += op * other_weight

        # At this point, mask is 1 where the mask is and 2 where the image is
        return im, dep, painted, mask

    @property
    def percent_usable(self):
        return sum(self.mask_means) / len(self.mask_means)

    def load(self, i):
        if self.multiresolution:
            if self.cache_dataset:
                return self.cache[self.size_index][i]
            else:
                return self._load(i, self.current_size)
        else:
            if self.cache_dataset:
                return self.cache[i]
            else:
                return self._load(i, self.desired_size)

    def augment_score_map(self, painted):
        if self.point_filtering_method == "gaussian":
            painted = det.score_gaussian_peaks(painted)
        elif self.point_filtering_method == "edgyness":
            painted = det.edgyness(painted)

        mask = det.mask_border(painted).int()

        if self.mask_cellwise:
            mask2 = det.mask_max_cellwise(painted).int().squeeze(1)
            mask3 = det.mask_max(painted*mask2, radius=self.maxpool_radius).int()
            mask *= mask2*mask3
        else:
            mask *= det.mask_max(painted, radius=self.maxpool_radius).int()

        nmsed = painted*mask.squeeze()
        if self.threshold is not None:
            nmsed = (nmsed > self.threshold).float()
            if not self.binary_target:
                nmsed *= painted
        return nmsed

    def unwrap_mask(self, mask, painted):
        voxel_mask = TF.to_tensor(mask != 1).float() # just masks out regions that are too close
        aug_mask = TF.to_tensor(mask != 0).float() # just masks out areas with 0 data because of augmentation
        if self.zero_data_zero_points:
            # This removes the mask on areas that are black because of augmentations
            # and just makes sure that there are 0 points there. This is to teach the network that black = no points.
            # Yes it has failed to learn that
            out_mask = voxel_mask
            #  aug_mask = self.eroder.erode(aug_mask).squeeze()
            painted *= aug_mask
        else:
            out_mask = voxel_mask * aug_mask
        # Now we need to create a mask where the augmentation mask meets the image, because there will always be points here
        # if we don't do otherwise
        eroded = (cv2.erode((mask == 0).astype(np.uint8), np.ones((3, 3), np.uint8), iterations=1) > 0.01).astype(np.uint8)
        edge = (mask != 0) - eroded
        dilated_edge = (cv2.erode(edge.astype(np.uint8), np.ones((3, 3), np.uint8), iterations=2) > 0.01).astype(np.uint8)
        out_mask *= dilated_edge
        return out_mask, painted

    def __getitem__(self, i):
        im, dep, painted, mask = self.load(i)
        painted_i = HeatmapsOnImage(painted, shape=painted.shape, min_value=0, max_value=1)
        mask_i = SegmentationMapsOnImage(mask, shape=mask.shape)
        if self.augmentation:
            seq = self.create_augmentations()
            im, tf_painted, tf_mask = seq(image=im, heatmaps=painted_i, segmentation_maps=mask_i)
            painted = tf_painted.get_arr()
            mask = tf_mask.get_arr()
        if not self.color_input:
            im = TF.to_grayscale(Image.fromarray(im))
        im = TF.to_tensor(im)
        painted = TF.to_tensor(painted)
        mask, painted = self.unwrap_mask(mask, painted)

        target = self.augment_score_map(painted)
        return im, painted, target, mask

    def __len__(self):
        return len(self.dataset)


class DescDetWrapper(Dataset):
    def __init__(self, dataset, args, threshold=None):
        # Scaling steps corresponds to the ratio in size from the descriptor grid to the image size
        self.ds = DetectionWrapper(dataset, args, threshold)
        self.scaling_steps = args.scaling_steps
        # grid height and width
        self.safe_dist = args.safe_dist
        # too lazy to deal with non square images
        self.device = torch.device('cpu')

    def __len__(self):
        return self.ds.dataset.get_num_pairs()

    @property
    def K(self):
        # K camera matrix
        def_calib = np.array(self.ds.dataset.get_calib())
        calib = def_calib * self.ds.current_size[0] / self.ds.dataset.desired_size[0]
        K = torch.from_numpy(util3d.calib_to_K(calib))
        return K

    def augment(self, im, painted, mask, u_points):
        painted_i = HeatmapsOnImage(painted, shape=painted.shape, min_value=0, max_value=1)
        mask_i = SegmentationMapsOnImage(mask, shape=mask.shape)
        kps_i = KeypointsOnImage([Keypoint(x=pt[1], y=pt[0]) for pt in u_points.T], shape=painted.shape)

        seq = self.ds.create_augmentations()
        im, tf_painted, tf_mask, tf_kps = seq(image=im, heatmaps=painted_i, segmentation_maps=mask_i, keypoints=kps_i)
        painted = tf_painted.get_arr()
        mask = tf_mask.get_arr()
        pts = np.zeros(u_points.shape, np.float64)
        for i in range(len(tf_kps.keypoints)):
            k = tf_kps.keypoints[i]
            pts[1, i] = k.x
            pts[0, i] = k.y
        return im, painted, mask, torch.tensor(pts)

    def __getitem__(self, pair_idx):
        i, j = self.ds.dataset.get_pair(pair_idx)
        #  print(f"Overlap: {self.pair_map[i, j]}")

        im1, dep1, painted1, mask1 = self.ds.load(i)
        im2, dep2, painted2, mask2 = self.ds.load(j)
        pose1 = self.ds.dataset.load_pose(i)
        pose2 = self.ds.dataset.load_pose(j)

        gh = self.ds.current_size[0] // (2**self.scaling_steps)
        gw = self.ds.current_size[1] // (2**self.scaling_steps)

        #  Step 1: Generate points in image 1 (use a grid)
        #  Step 2: Warp these points to image 2 densely
        # Generate points in image 1
        # (2, gh*gw)
        grid = util3d.grid_positions(gh, gw, self.device)
        # (2, gh*gw)
        up_grid = util3d.upscale_positions(grid, self.scaling_steps)

        # Warp points to image 2
        try:
            u_points1, u_points2, ids = util3d.warp(up_grid,
                torch.from_numpy(dep1), self.K, torch.from_numpy(pose1), [0, 0],
                torch.from_numpy(dep2), self.K, torch.from_numpy(pose2), [0, 0])
        except util3d.EmptyTensorError:
            return self[pair_idx+1]

        if self.ds.augmentation:
            im1, painted1, mask1, u_points1 = self.augment(im1, painted1, mask1, u_points1)
            im2, painted2, mask2, u_points2 = self.augment(im2, painted2, mask2, u_points2)

        im1 = TF.to_tensor(TF.to_grayscale(Image.fromarray(im1)))
        painted1 = TF.to_tensor(painted1)
        mask1, painted1 = self.ds.unwrap_mask(mask1, painted1)

        im2 = TF.to_tensor(TF.to_grayscale(Image.fromarray(im2)))
        painted2 = TF.to_tensor(painted2)
        mask2, painted2 = self.ds.unwrap_mask(mask2, painted2)

        #  Step 5&6: Convert augmented binary images back to points with an index
        target1 = self.ds.augment_score_map(painted1)
        target2 = self.ds.augment_score_map(painted2)

        # Convert the augmented points back to grid coordinates
        p1 = util3d.downscale_positions(u_points1, self.scaling_steps)
        p2 = util3d.downscale_positions(u_points2, self.scaling_steps)

        # Convert points to indices
        p1m = clip_mask(p1, 0, gh)
        p2m = clip_mask(p2, 0, gh)

        match = torch.zeros((gw, gh, gw, gh)).int()

        # Now that the pair points1, points2 have been augmented, we need to
        # find the cells they don't correspond within some radius to find the
        # negatives
        # (gw, gh, gw, gh)
        cell_dist1 = torch.norm(
            p1.view(2, -1, 1)-grid.view(2, 1, -1),
            dim=0)
        cell_dist2 = torch.norm(
            p2.view(2, -1, 1)-grid.view(2, 1, -1),
            dim=0)

        s1 = (cell_dist1 >= self.safe_dist) * p2m.unsqueeze(1)
        s2 = (cell_dist2 >= self.safe_dist) * p1m.unsqueeze(1)
        # remove points that are out of points
        # point coordinates, grid coordinates = torch.where(s1)
        p1_c, g1_c = torch.where(s1)
        p2_c, g2_c = torch.where(s2)

        # The points from the grid form negative pairs with the points in image
        # 2
        p1 = to_ind(p1, 0, gh-1)
        p2 = to_ind(p2, 0, gh-1)
        gc = to_ind(grid, 0, gh-1)

        match[p1[0, p2_c], p1[1, p2_c],
              gc[0, g2_c], gc[1, g2_c]] = -1
        match[gc[0, g1_c], gc[1, g1_c],
              p2[0, p1_c], p2[1, p1_c]] = -1

        # Add positives
        match[p1[0, :], p1[1, :],
              p2[0, :], p2[1, :]] = 1

        # First, mask out areas with no points
        _, H, W = target2.shape
        mask_t1 = batch_fast_interp(target1.view(1, 1, H, W).float(), (1, 1, gw, gh), local_min=False)
        mask_t2 = batch_fast_interp(target2.view(1, 1, H, W).float(), (1, 1, gw, gh), local_min=False)

        # Mask out matches based on areas that are not being used
        mi1_i, mi1_j = torch.where(mask_t1.squeeze() == 0)
        mi2_i, mi2_j = torch.where(mask_t2.squeeze() == 0)
        match[mi1_i, mi1_j, :, :] = 0
        match[:, :, mi2_i, mi2_j] = 0

        return im1, im2, painted1, painted2, target1, target2, mask1, mask2, match.view(gw*gh, gw*gh)


def clip_mask(p, min_v, max_v):
    # Points: (2, :)
    min_f = (p[0, :] >= min_v) & (p[1, :] >= min_v)
    max_f = (p[0, :] < max_v) & (p[1, :] < max_v)
    return min_f & max_f


def to_ind(p, min_v, max_v):
    p = (p + 0.5).long()
    p = torch.clamp(p, min_v, max_v)
    return p
