# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import random
import re
import numpy as np
import math
import json
import cv2
from pyquaternion import Quaternion
from cpaint.datasets.base import BaseDataset

# these have frames with no pose

AREAS = [
    "ArchinteriorsVol2Scene1",
    "RealisticRendering", "UrbanCity", "ArchVisInterior",
    "Forests/winter_forest", "ArchinteriorsVol2Scene2",
]

UNUSED_AREAS = [
    "ArchinteriorsVol2Scene3",
    "Forests",
    "Forests/summer_forest",
    "Forests/autumn_forest",
]

VOXEL_SIZE = {
    "ArchinteriorsVol2Scene1": 0.005,
    "ArchinteriorsVol2Scene2": 0.005,
    "ArchinteriorsVol2Scene3": 0.005,
    "RealisticRendering": 0.005,
    "UrbanCity": 0.010, # here's why this exists btw
    "ArchVisInterior": 0.005,
    "Forests": 0.005,
    "Forests/winter_forest": 0.005,
    "Forests/summer_forest": 0.005,
    "Forests/autumn_forest": 0.005,
}

MULTI_SEASONS = {
    "Forests": {
        "main": "winter_forest",
        #  "all": ["winter_forest", "winter_snowfall_forest", "summer_forest", "spring_forest", "autumn_forest"],
        "all": ["winter_forest", "summer_forest", "autumn_forest"],
        "max_len": 2000, # weird bug in painter, no idea
    },
}

def convert_depth(calib, point_depth):
    H = point_depth.shape[0]
    W = point_depth.shape[1]
    #  fov = 67.5
    #  f = H / (2 * math.tan(fov * math.pi / 360))

    fx, fy, cx, cy = calib
    #  f = 360
    #  i_c = np.float(H) / 2 - 1
    #  j_c = np.float(W) / 2 - 1
    columns, rows = np.meshgrid(np.linspace(0, W-1, num=W), np.linspace(0, H-1, num=H))
    y = (rows - cy)/fy
    x = (columns - cx)/fx
    z = 1
    dist_to_plane = (x**2 + y**2 + z**2)**(0.5)
    #  depth = f * point_depth / np.sqrt(f**2 + (rows - i_c)**2 + (columns - j_c)**2)
    depth = point_depth/dist_to_plane
    return depth


def read_pose(path):
    with open(path, "r") as f:
        dat = json.load(f)
        ori = dat["pitchyawroll"]
        pos = np.array(dat["position"])*0.01
        pos[2] *= -1
        q = Quaternion(axis=[0,0,1], degrees=ori[1]) * \
            Quaternion(axis=[0,1,0], degrees=ori[0])
        corr = np.array([[0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [1, 0, 0, 0],
                         [0, 0, 0, 1]]).T
        ret = q.transformation_matrix.dot(corr)
        ret[:3, 3] = pos
        return ret

class UnrealCVDataset(BaseDataset):

    def _init(self, basepath, dname):
        self.voxel_size = VOXEL_SIZE[self.dname]
        self.version = "unrealcv"
        self.type = "unrealcv"
        self.default_calib = [320, 320, 320, 240]

        self.rgb_paths = []
        self.depth_paths = []
        self.pose_paths = []

        # Here is where we handle if the full dataset consists of multiple seasons. All of them have the directory format:
        # Parent/Child
        pattern = re.compile("[0-9]+_rgb.png")
        self.main_area = self.dname if self.dname not in MULTI_SEASONS.keys() else os.path.join(self.dname, MULTI_SEASONS[self.dname]['main'])
        max_length = 3000 if self.dname not in MULTI_SEASONS.keys() else MULTI_SEASONS[self.dname]['max_len']
        self.folders = [self.dname] if self.dname not in MULTI_SEASONS.keys() else [os.path.join(self.dname, sub_area) for sub_area in MULTI_SEASONS[self.dname]['all']]

        impath = f"{self.version}/images/{self.main_area}"
        self.repeat_length = len(
                [path for path in
                    os.listdir(os.path.join(basepath, impath))
                    if pattern.match(path) is not None])
        self.repeat_length = min(max_length, self.repeat_length)

        if self.is_part_of_multi_season:
            parent, child = self.dname.split("/")
            self.multi_data = MULTI_SEASONS[parent]
            self.season = child

        self.rgb_paths, self.depth_paths, self.pose_paths = [], [], []
        for folder in self.folders:
            area_path = os.path.join(basepath,
                                     f"{self.version}/images/{folder}")
            self.rgb_paths.extend([os.path.join(area_path, f"{i:04d}_rgb.png")
                              for i in range(self.repeat_length)])
            self.depth_paths.extend([os.path.join(area_path, f"{i:04d}_depth.exr")
                                for i in range(self.repeat_length)])
            self.pose_paths.extend([os.path.join(area_path, f"{i:04d}_pose.json")
                               for i in range(self.repeat_length)])

    def get_depth_factor(self):
        return 1

    def __len__(self):
        # There are 3 files for each frame. rgb, depth, pose
        return len(self.rgb_paths)

    def __getitem__(self, i):
        fname = os.path.basename(self.rgb_paths[i])
        basename = fname.split(".")[0]
        return self.rgb_paths[i], self.depth_paths[i], self.pose_paths[i], basename

    def load_pose(self, i):
        path = self.pose_paths[i]
        pose = read_pose(path)
        return pose

    def _load_depth_image(self, i):
        depth_path = self.depth_paths[i]
        depth = cv2.imread(str(depth_path), -1)
        depth /= self.get_depth_factor()
        depth = convert_depth(self.default_calib, depth)
        depth *= self.get_depth_factor()
        return depth

    def _load_rgb_image(self, i):
        rgb_path = self.rgb_paths[i]
        rgb = cv2.imread(str(rgb_path), cv2.IMREAD_COLOR)
        return rgb
