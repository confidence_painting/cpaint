import os
import random
import json
import cv2
from cpaint.core import image_ops
from torch.utils.data import Dataset


# these have frames with no pose
"""
"scene0035_00"
"scene0026_00"
"scene0138_00"
"scene0055_00"
"scene0002_00"
"scene0002_01"
"scene0178_00"
"scene0038_01"
"scene0126_02"
"scene0142_01"
"scene0051_01"
"scene0111_00"
"scene0127_00"
"scene0181_02"
"scene0199_00"
"scene0051_02"
"scene0092_03"
"scene0111_01"
"scene0127_01"
"scene0151_00"
"scene0051_03"
"scene0111_02"
"scene0151_01"
"scene0169_01"
"scene0185_00"
"scene0042_01"
"scene0069_00"
"scene0152_00"
"""

AREAS = [
    "scene0000_00", "scene0011_00", "scene0025_00", "scene0031_01",
    "scene0042_02", "scene0054_00", "scene0072_00", "scene0086_01",
    "scene0096_01", "scene0114_01", "scene0137_02", "scene0152_01",
    "scene0174_01", "scene0186_01", "scene0000_01", "scene0011_01",
    "scene0025_01", "scene0031_02", "scene0046_00",
    "scene0072_01", "scene0086_02", "scene0096_02", "scene0114_02",
    "scene0152_02", "scene0177_00", "scene0187_00",
    "scene0000_02", "scene0012_00", "scene0025_02", "scene0033_00",
    "scene0046_01", "scene0055_01", "scene0073_00", "scene0087_00",
    "scene0101_01", "scene0115_00", "scene0140_00", "scene0155_00",
    "scene0177_01", "scene0188_00", "scene0012_01",
    "scene0046_02", "scene0055_02",
    "scene0074_00", "scene0087_01", "scene0102_00", "scene0121_00",
    "scene0140_01", "scene0155_01", "scene0177_02", "scene0189_00",
    "scene0012_02", "scene0027_00", "scene0038_00",
    "scene0050_00", "scene0056_00", "scene0074_01", "scene0087_02",
    "scene0106_00", "scene0126_00", "scene0142_00", "scene0163_00",
    "scene0195_00", "scene0006_00", "scene0014_00",
    "scene0029_01", "scene0050_01", "scene0057_00",
    "scene0074_02", "scene0088_02", "scene0106_01", "scene0126_01",
    "scene0166_00", "scene0181_00", "scene0195_01",
    "scene0006_01", "scene0017_00", "scene0029_02", "scene0040_00",
    "scene0050_02", "scene0057_01", "scene0079_00", "scene0089_01",
    "scene0107_00", "scene0147_00", "scene0166_01",
    "scene0181_01", "scene0195_02", "scene0006_02", "scene0017_01",
    "scene0030_00", "scene0040_01", "scene0059_00",
    "scene0084_00", "scene0089_02",
    "scene0147_01", "scene0166_02",
    "scene0009_00", "scene0024_00", "scene0030_01", "scene0041_00",
    "scene0059_01", "scene0084_01",
    "scene0169_00",
    "scene0184_00", "scene0009_02", "scene0024_01", "scene0030_02",
    "scene0042_00", "scene0059_02", "scene0085_00",
    "scene0092_04", "scene0129_00",
    "scene0010_00", "scene0024_02",
    "scene0031_00", "scene0052_02",
    "scene0086_00", "scene0096_00", "scene0114_00", "scene0137_01",
    "scene0174_00", "scene0186_00"
]


class ScannetDataset:

    def __init__(self, basepath, area, filtered_files=False, shape_type="square"):
        self.type = "scannet"
        self.basepath = basepath
        self.area = area
        self.calib_path = os.path.join(
                basepath, "scannet/images", area, "calib_depth_nice.txt")
        self.name = f"scannet/{area}"
        self.shape_type = shape_type

        if filtered_files:
            self.framerate = 1
        else:
            if area == 'scene0000_01':
                self.framerate = 30
            else:
                self.framerate = 60

        self.rgb_paths = []
        self.depth_paths = []
        self.pose_paths = []
        for path in sorted(os.listdir(os.path.join(
                basepath, "scannet/images", area))):
            if "frame" not in path:
                continue
            abs_path = os.path.join(basepath, "scannet/images", area, path)
            if "color" in path:
                self.rgb_paths.append(abs_path)
            if "depth" in path:
                self.depth_paths.append(abs_path)
            if "pose" in path:
                self.pose_paths.append(abs_path)
        assert(len(self.rgb_paths) == len(self.depth_paths))
        assert(len(self.rgb_paths) == len(self.pose_paths))

    def get_canonical_size(self):
        return [1296, 968]

    def get_depth_factor(self):
        return 1000

    def get_octomap_path(self, octomap_dir):
        return os.path.join(octomap_dir, f"{self.area}/{self.area}_vh_clean_2.binvox.bt")

    def get_calib(self):
        with open(self.calib_path, "r") as f:
            dat = json.load(f)
            return [dat["fx"], dat["fy"], dat["cx"], dat["cy"]]

    def __len__(self):
        # There are 3 files for each frame. rgb, depth, pose
        return len(self.rgb_paths) // self.framerate + 1

    def __getitem__(self, i):
        fname = os.path.basename(self.rgb_paths[i*self.framerate])
        basename = fname.split(".")[0]
        return self.rgb_paths[i*self.framerate], self.depth_paths[i*self.framerate], self.pose_paths[i*self.framerate], basename

    def load_pose(self, i):
        path = self.pose_paths[i]
        dat = np.loadtxt(path, delimiter=" ")
        return dat

    def load_depth_image(self, i):
        if self.shape_type == "square":
            depth = cv2.imread(str(depth_path), -1)
            m_size = min(depth.shape[:2])
            depth = image_ops.scale_then_crop(depth, (m_size, m_size))
            # convert to planar depth
            depth /= self.get_depth_factor()
            return depth
        if self.shape_type == "original":
            depth = cv2.imread(str(depth_path), -1)
            # convert to planar depth
            depth /= self.get_depth_factor()
            return depth

    def load_rgb_image(self, i):
        rgb_path = self.rgb_paths[i]
        if self.shape_type == "square":
            rgb = cv2.imread(str(rgb_path), -1)
            m_size = min(rgb.shape[:2])
            rgb = image_ops.scale_then_crop(rgb, (m_size, m_size))
            return rgb
        if self.shape_type == "original":
            rgb = cv2.imread(str(rgb_path), -1)
            # convert to planar depth
            return rgb
