# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import argparse
import toml
import torch
import numpy as np
from cpaint import datasets
from cpaint.core import paths, config
from cpaint.core.detectors import DETECTORS
from cpaint.train_detector import train_main
from cpaint.paint_maps import paint_maps
#  from cpaint.core.superpoint.model import SuperPointNet
from cpaint.models import MODELS, model_to_detector
from cpaint.find_threshold import find_painter_output_threshold

import gc

def paint_train_iter(args, desired_num_detections, i, j):
    prev_prefix = paths.name_prefix(args.preprefix, i)
    new_prefix = paths.name_prefix(args.preprefix, j)

    # Load detector using prev_prefix
    device = torch.device("cuda" if not args.no_cuda else "cpu")
    #  model = SuperPointNet().to(device)
    model_cfg = MODELS[args.model]
    if i == 0:
        # Inject signal
        if args.signal_source == "pretrained":
            model = model_cfg["net"]().to(device)
            model.load_default_state_dict()
            model.eval()
            detector = model_to_detector(model, model_cfg["color_input"])
        elif args.signal_source == "untrained":
            # this is important for models that use batch norm
            model = model_cfg["net"]().to(device)
            model.train()
            detector = model_to_detector(model, model_cfg["color_input"])
        elif args.signal_source in DETECTORS:
            detector = DETECTORS[args.signal_source]()
        else:
            # Assume that the signal source is a checkpoint
            ckpt_path = args.signal_source
            model = model_cfg["net"]().to(device)
            ckpt = torch.load(ckpt_path)
            if "state_dict" in ckpt:
                state_dict = ckpt["state_dict"]
                state_dict = {".".join(k.split(".")[1:]): v for k, v in state_dict.items()}
            else:
                state_dict = ckpt
            model.load_state_dict(state_dict)
            detector = model_to_detector(model, model_cfg["color_input"])
        ckpt_path = ''
        print(f"Initializing with {args.signal_source}")
    else:
        model = model_cfg["net"]().to(device)
        ckpt_path = paths.latest_checkpoint_path(args.output_dir, prev_prefix)
        ckpt = torch.load(ckpt_path)
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
            state_dict = {".".join(k.split(".")[1:]): v for k, v in state_dict.items()}
        else:
            state_dict = ckpt

        model.load_state_dict(state_dict)
        model.eval()
        detector = model_to_detector(model, model_cfg["color_input"])
        print(f"Initializing model from {ckpt_path}")

    if not args.continue_iter:
        if args.threshold_input:
            # Calculate painter input threshold (used to maximize interpretability of results of painter)
            input_threshold = find_painter_input_threshold(args.dataset_dir, args.dataset, detector)
            torch.cuda.empty_cache()
            print(f"Found input threshold: {input_threshold}")
        else:
            input_threshold = None

        # Paint map under new prefix
        print("Painting maps.")
        for dataset in datasets.DatasetList(args.dataset_dir, args.dataset):
            painted_path = paths.get_painted_dir(args.cache_dir, dataset.name, new_prefix)
            paint_maps(dataset, painted_path, args.dataset_dir, detector, input_threshold, cudatree=args.cudatree)
            gc.collect()
        torch.cuda.empty_cache()
        print("Done.")
    del detector

    # Calculate painter output threshold (used to shape desired_num_detections and threshold the output of the NMS)
    # The threshold for points added to the NMS is fixed to be 0.0001
    output_threshold = find_painter_output_threshold(args, desired_num_detections, new_prefix)
    print(f"Found output threshold: {output_threshold}")

    # Train
    if args.use_previous_checkpoint:
        args.checkpoint = ckpt_path
    else:
        args.checkpoint = ''
    train_main(args, new_prefix, new_prefix, output_threshold)
    args.continue_iter = False
    if args.no_cache:
        for dataset in datasets.DatasetList(args.dataset_dir, args.dataset):
            print(f"Deleting cache dir: {painted_path}")
            try:
                painted_path = paths.get_painted_dir(args.cache_dir, dataset.name, new_prefix)
                os.rmdir(painted_path)
            except:
                print("Failed to delete directory")
                break

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("computer_config", type=str, help="Path to computer specific config")
    parser.add_argument("net_config", type=str, help="Path to net config")
    parser.add_argument("dataset", choices=datasets.DATASET_LIST+list(datasets.CATEGORIES.keys()),
                        help="Training set")
    parser.add_argument("preprefix", type=str, help="Preprefix to name run.")
    parser.add_argument('--display', action='store_true', default=False,
                        help='Display debugging view')
    parser.add_argument('--world_size', default=1, help="Number of gpus")
    parser.add_argument('--start_i', type=int, default=0, help="Starting iteration to allow for resuming")
    parser.add_argument('--continue_iter', action="store_true", default=False, help="Whether to use the painted maps from last run")
    parser.add_argument('--single_iter', action="store_true", default=False, help="Whether to iterate a single time (used to avoid the memory leak)")
    parser.add_argument('--cudatree', action="store_true", default=False, help="Whether to use the cuda accelerated tree raycasting")
    parser.add_argument('--no_cache', action="store_true", default=False, help="Delete painted maps afterwards to save memory")
    parser.add_argument('--test_interval', type=int, default=20,
                        help='Interval between test iterations')
    args = parser.parse_args()

    config.augment_args(args, args.computer_config)
    config.augment_args(args, args.net_config)
    args.world_size = int(args.world_size)

    var_range = np.array(args.varieties)
    dnd_range = np.repeat(var_range, args.iters)
    print(dnd_range)
    iters = len(dnd_range)

    for (i, j, desired_num_detections) in list(zip(range(iters), range(1, iters+1), dnd_range))[args.start_i:]:
        print(f"Number of desired detections: {desired_num_detections}")
        paint_train_iter(args, desired_num_detections, i, j)
        if args.single_iter:
            break
