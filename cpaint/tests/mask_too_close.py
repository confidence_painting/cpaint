# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import cv2
import os
import numpy as np
from cpaint.datasets.unrealcv import read_pose, convert_depth
from cpaint.core.image_ops import scale_then_crop, fast_interp
from cpaint.tests.util import replace_filename, point_selector
import cpaint.homography_pair_test as hpt
import matplotlib.pyplot as plt
from cpaint.core.detectors import DETECTORS, nms_score_map, peakiness, edgyness

if __name__ == "__main__":
    # Initialize vars
    im_paths = [
        "../assets/0000_rgb.png",
        "../assets/0002_rgb.png",
        "../assets/0103_rgb.png",
        "../assets/0349_rgb.png",
        "../assets/0274_rgb.png",
    ]
    dep_paths = [
        "../assets/0000_depth.exr",
        "../assets/0002_depth.exr",
        "../assets/0103_depth.exr",
        "../assets/0349_depth.exr",
        "../assets/0274_depth.exr",
    ]
    counts_paths = [
        "../assets/data_counts_00000.exr",
        "../assets/data_counts_00002.exr",
        "../assets/data_counts_00103.exr",
        "../assets/data_counts_00349.exr",
        "../assets/data_counts_00274.exr",
    ]
    reproj_paths = [
        "../assets/reproj_score_00000.exr",
        "../assets/reproj_score_00002.exr",
        "../assets/reproj_score_00103.exr",
        "../assets/reproj_score_00349.exr",
        "../assets/reproj_score_00274.exr",
    ]

    square_size = (480, 480)
    image_size = (240, 240)
    # fx, fy, cx, cy
    calib = [image_size[0]*320/480, image_size[1]*320/480, image_size[0]/2, image_size[1]/2]

    voxel_size = 0.005
    # Begin calculations for pixel size
    #  x, y = np.meshgrid(range(image_size[0]), range(image_size[1]))
    #  x = (x - calib[2])/calib[0]
    #  y = (y - calib[3])/calib[1]

    for (im_path, dep_path, counts_path, reproj_path) in zip(im_paths, dep_paths, counts_paths, reproj_paths):
        # Load
        im = cv2.imread(replace_filename(__file__, im_path))
        dep = cv2.imread(replace_filename(__file__, dep_path), -1)
        counts = cv2.imread(replace_filename(__file__, counts_path), -1)
        reproj = cv2.imread(replace_filename(__file__, reproj_path), -1)

        im = scale_then_crop(im, image_size)
        gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        reproj = scale_then_crop(reproj, image_size)

        # Special scale
        dep = scale_then_crop(dep, square_size, cv2.INTER_NEAREST)
        counts = scale_then_crop(counts, square_size, cv2.INTER_NEAREST)
        dep = fast_interp(dep, image_size)
        counts = fast_interp(counts, image_size, False)
        dep = convert_depth(calib, dep)

        # Size of pixels as planes
        pix_size = dep / image_size[0]
        pixels_per_voxel = np.floor(voxel_size/pix_size)
        # At depth = 1, each pixel has size 1/w
        mask = (pixels_per_voxel <= 3).astype(np.uint8)
        mask = cv2.erode(mask, np.ones((3,3), np.uint8), iterations=int(np.max(pixels_per_voxel)))

        score = reproj/counts
        score[np.isnan(score)] = 0
        #  score = cv2.blur(score, (3, 3))

        fig = plt.figure(figsize=(3,3), dpi=300)

        f = fig.add_subplot(2, 3, 1)
        f.set_title("RGB")
        f.imshow(im)

        f = fig.add_subplot(2, 3, 2)
        f.set_title("Pix Per Vox")
        f.imshow(pixels_per_voxel)

        f = fig.add_subplot(2, 3, 3)
        f.set_title("Masked")
        f.imshow(score * mask)

        f = fig.add_subplot(2, 3, 4)
        f.set_title("Peakiness")
        p_score_map = peakiness(score)
        e_score_map = edgyness(gray.astype(np.double))
        f.imshow(p_score_map)

        f = fig.add_subplot(2, 3, 5)
        f.set_title("NMS")
        f.imshow(nms_score_map(score * mask))

        f = fig.add_subplot(2, 3, 6)
        f.set_title("Edgyness")
        f.imshow(e_score_map)

        plt.show()
