# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import torch
import cv2
import os
import numpy as np
from cpaint.datasets.unrealcv import read_pose, convert_depth
from cpaint.core.image_ops import scale_then_crop, fast_interp
from cpaint.tests.util import replace_filename, point_selector
from cpaint.models import loss
import matplotlib.pyplot as plt
from cpaint.core.util3d import calib_to_K

def image_space_to_graph_space(x, y):
    return x, y

if __name__ == "__main__":
    # Initialize vars
    im_paths = [
        "../assets/0000_rgb.png",
        "../assets/0002_rgb.png",
        "../assets/0103_rgb.png",
        "../assets/0349_rgb.png",
        "../assets/0274_rgb.png",
    ]
    dep_paths = [
        "../assets/0000_depth.exr",
        "../assets/0002_depth.exr",
        "../assets/0103_depth.exr",
        "../assets/0349_depth.exr",
        "../assets/0274_depth.exr",
    ]
    pose_paths = [
        "../assets/0000_pose.json",
        "../assets/0002_pose.json",
        "../assets/0103_pose.json",
        "../assets/0349_pose.json",
        "../assets/0274_pose.json",
    ]

    pairs = [
        [0, 1],
        [1, 2],
    ]

    square_size = (480, 480)
    image_size = (240, 240)
    h, w = image_size
    # fx, fy, cx, cy
    calib = [image_size[0]*320/480, image_size[1]*320/480, image_size[0]/2, image_size[1]/2]
    K = calib_to_K(calib)
    print(K)
    device = torch.device("cpu")

    scaling_steps = 2
    gh, gw = h//(2**scaling_steps), w//(2**scaling_steps)

    paths = list(zip(im_paths, dep_paths, pose_paths))
    for (im_path1, dep_path1, pose_path1), (im_path2, dep_path2, pose_path2) in [[paths[i], paths[j]] for (i, j) in pairs]:
        # Load rgb image
        im1 = cv2.imread(replace_filename(__file__, im_path1))
        im1 = scale_then_crop(im1, image_size)

        im2 = cv2.imread(replace_filename(__file__, im_path2))
        im2 = scale_then_crop(im2, image_size)

        # Load depth images
        dep1 = cv2.imread(replace_filename(__file__, dep_path1), -1)
        dep1 = scale_then_crop(dep1, square_size, cv2.INTER_NEAREST)
        dep1 = fast_interp(dep1, image_size)
        dep1 = convert_depth(calib, dep1)

        dep2 = cv2.imread(replace_filename(__file__, dep_path2), -1)
        dep2 = scale_then_crop(dep2, square_size, cv2.INTER_NEAREST)
        dep2 = fast_interp(dep2, image_size)
        dep2 = convert_depth(calib, dep2)

        # Load poses
        pose1 = read_pose(replace_filename(__file__, pose_path1))
        pose2 = read_pose(replace_filename(__file__, pose_path2))

        # Generate points in image 1
        gp1 = loss.grid_positions(gh, gw, device)
        u_gp1 = loss.upscale_positions(gp1, scaling_steps)

        # Warp points to image 2
        u_gp1, u_gp2, ids = loss.warp(u_gp1,
         torch.from_numpy(dep1), torch.from_numpy(K), torch.from_numpy(pose1), [0, 0],
         torch.from_numpy(dep2), torch.from_numpy(K), torch.from_numpy(pose2), [0, 0])

        gp1 = loss.downscale_positions(u_gp1, scaling_steps)
        gp2 = loss.downscale_positions(u_gp2, scaling_steps)

        fig = plt.figure(figsize=(3,3), dpi=300)
        f = fig.add_subplot(2, 1, 1)
        f.set_title("im 1")
        f.imshow(im1, extent=[0, w, 0, h])
        f.scatter(u_gp1[1], h-u_gp1[0], s=0.1, marker=".")

        f = fig.add_subplot(2, 1, 2)
        f.set_title("im 2")
        f.imshow(im2, extent=[0, w, 0, h])
        f.scatter(u_gp2[1], h-u_gp2[0], s=0.1, marker=".")

        plt.show()
