# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import cv2
import os
import numpy as np
from cpaint.datasets.unrealcv import read_pose
from cpaint.core.image_ops import scale_then_crop
from cpaint.tests.util import replace_filename, point_selector
import cpaint.homography_pair_test as hpt
from cpaint.core.util3d import calib_to_K
import matplotlib.pyplot as plt

if __name__ == "__main__":
    im1 = cv2.imread(replace_filename(__file__, "../assets/0000_rgb.png"))
    im2 = cv2.imread(replace_filename(__file__, "../assets/0002_rgb.png"))
    im1 = scale_then_crop(im1, (480, 480))
    im2 = scale_then_crop(im2, (480, 480))

    pose1 = read_pose(replace_filename(__file__, "../assets/0000_pose.json"))
    pose2 = read_pose(replace_filename(__file__, "../assets/0002_pose.json"))
    # Hand select points on a pair of images of known pose
    n = 10
    #  pts1 = np.array(point_selector(im1, n))
    #  pts2 = np.array(point_selector(im2, n))

    # Hand selected points
    pts1 = np.array([[484.76072115, 200.60016871],
                     [481.46492329,  84.77641543],
                     [544.08508259, 132.8008985 ],
                     [547.85170871, 214.72501668],
                     [521.48532585, 205.77927963],
                     [545.96839565, 271.22440852],
                     [591.6387374,  266.51612587],
                     [593.05122219, 300.88658924],
                     [594.46370699,  36.28110409],
                     [569.50980892, 146.92574646]])
    pts2 = np.array([[106.68562404, 239.67891474],
                     [126.46041119,  93.72215247],
                     [245.57996233, 182.70869463],
                     [241.34250795, 266.51612587],
                     [193.31802488, 253.33293444],
                     [161.30170283, 339.96533527],
                     [271.00468866, 323.01551771],
                     [270.5338604,  362.09426374],
                     [285.60036489, 105.02203084],
                     [288.89616275, 205.77927963]])
    desc1 = np.eye(n).astype(np.float32)
    desc2 = np.eye(n).astype(np.float32)
    calib = [320, 320, 240, 240]
    #  calib = [320, 320, 320, 240]
    K = calib_to_K(calib)

    err, est_pose_diff = hpt.get_pose_diff(desc1, desc2, pts1, pts2, K, cv2.NORM_L2)
    pose_diff = np.linalg.inv(pose1).dot(pose2)
    print(f"Estimated Pose Diff Mat: \n{est_pose_diff}")
    print(f"Pose Diff Mat: \n{pose_diff}")
    # Calculate error
    dist, ang_err = hpt.getEstimationError(pose_diff, est_pose_diff)
    print(f"Dist Err: {dist}, Angle Err(Radians): {ang_err}")
