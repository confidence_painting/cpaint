# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from cpaint.core import image_ops
from cpaint.tests.util import replace_filename
from cpaint.core.detectors import DETECTORS
import matplotlib.pyplot as plt
import os
import cv2
import torch
import numpy as np
from PIL import Image

import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables import Keypoint, KeypointsOnImage
from imgaug.augmentables.heatmaps import HeatmapsOnImage
from imgaug.augmentables.segmaps import SegmentationMapsOnImage

detector = DETECTORS['fast']()

if __name__ == "__main__":
    # test image
    im = cv2.imread(replace_filename(__file__, "../assets/0000_rgb.png"))

    score_map = detector.predict(im)
    score_map_i = HeatmapsOnImage(score_map, shape=score_map.shape, min_value=0, max_value=1)

    mask1 = (np.ones_like(score_map) > 0)
    mask2 = (im[:, :, 0] < 10)
    mask = np.where(mask1 & ~mask2, 1, 0) + mask2.astype(np.int) * 2
    mask_i = SegmentationMapsOnImage(mask.astype(np.int32), shape=mask.shape)

    drawn_score_map = image_ops.draw_keypoints(score_map, im)

    seq = iaa.Sequential([
        iaa.Fliplr(0.5),  # horizontal flips
        iaa.Crop(percent=(0, 0.1)),  # random crops
        # Small gaussian blur with random sigma between 0 and 0.5.
        # But we only blur about 50% of all images.
        iaa.Sometimes(
            0.5,
            iaa.GaussianBlur(sigma=(0, 0.5))
        ),
        # Strengthen or weaken the contrast in each image.
        iaa.LinearContrast((0.75, 1.5)),
        # Add gaussian noise.
        # For 50% of all images, we sample the noise once per pixel.
        # For the other 50% of all images, we sample the noise per pixel AND
        # channel. This can change the color (not only brightness) of the
        # pixels.
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
        # Make some images brighter and some darker.
        # In 20% of all cases, we sample the multiplier once per channel,
        # which can end up changing the color of the images.
        iaa.Multiply((0.8, 1.2), per_channel=0.2),
        # Apply affine transformations to each image.
        # Scale/zoom them, translate/move them, rotate them and shear them.
        iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
            translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
            rotate=(-25, 25),
            shear=(-8, 8)
        )
    ], random_order=True) # apply augmenters in random order

    cells = []
    for i in range(10):
        tf_im, tf_score_map, tf_mask = seq(image=im, heatmaps=score_map_i, segmentation_maps=mask_i)
        cells.append(tf_im)
        cells.append(tf_score_map.draw()[0])
        cells.append(tf_mask.draw_on_image(tf_im)[0])
        #  cells.append(tf_score_map.get_arr())
        #  cells.append(tf_mask.get_arr())
    grid_image = ia.draw_grid(cells, cols=3)

    plt.imshow(grid_image)
    plt.show()
