import random
import numpy as np
import matplotlib.pyplot as plt
import torch

from cpaint.core import image_ops

for multi in range(1, 10):
    for i in range(10):
        channels = 1 if random.random() < 0.5 else 3
        size = [random.randint(50, 200) for i in range(2)]
        im = np.random.rand(size[0], size[1], channels)

        input_ = torch.from_numpy(np.rollaxis(im, 2, 0)).unsqueeze(0)
        padded = image_ops.pad_to_multiple(input_, multi, 'constant')
        unpadded = image_ops.unpad_multiple(padded, input_.shape, multi)

        padded = padded.permute(0, 2, 3, 1).squeeze()
        unpadded = unpadded.permute(0, 2, 3, 1).squeeze()
        print(f"Input size: {im.shape}, output_size: {padded.shape}, unpadded: {unpadded.shape}")
        assert(size[0] == unpadded.shape[0])
        assert(size[1] == unpadded.shape[1])
        assert(padded.shape[0] % multi == 0)
        assert(padded.shape[1] % multi == 0)

        if False:
            fig = plt.figure(figsize=(3,3), dpi=600)
            f = fig.add_subplot(1, 3, 1)
            f.imshow(im.squeeze())
            f = fig.add_subplot(1, 3, 2)
            f.imshow(padded)
            f = fig.add_subplot(1, 3, 3)
            f.imshow(unpadded)
            plt.show()
