# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This is some code to visualize the method of finding the detection threshold

from cpaint.core import image_ops
from cpaint.tests.util import replace_filename
from cpaint.core.detectors import DETECTORS
import matplotlib.pyplot as plt
import os
import cv2
import torch
import numpy as np
from PIL import Image
import kneed

detector = DETECTORS['superpoint']()

if __name__ == "__main__":
    # test image
    im = cv2.imread(replace_filename(__file__, "../assets/0000_rgb.png"))
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
    score_map = detector.predict(gray)
    thresholds = np.linspace(0, np.max(score_map), 2000)
    num_detections = thresholds.copy()
    for i, t in enumerate(thresholds):
        num_detections[i] = np.count_nonzero(score_map > t)

    # Find the elbow
    kneedle = kneed.KneeLocator(thresholds, num_detections, direction="decreasing", curve="convex")
    threshold = kneedle.knee
    print(f"Threshold: {threshold}")

    fig = plt.figure(figsize=(6,6), dpi=300)
    kneedle.plot_knee_normalized()
    f = fig.add_subplot(2, 1, 1)
    f.scatter(thresholds, num_detections, s=0.1)
    f.vlines(threshold, 0, np.max(num_detections), linestyles='--', colors='r')
    f.yscale = "log"
    f = fig.add_subplot(2, 1, 2)
    f.imshow(score_map > threshold)
    plt.show()
