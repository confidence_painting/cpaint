import numpy as np

n = 5
idx = np.zeros((n, n), np.int)

def offset(y):
    return (n-1)*(n-2)//2 - (n-y-2)*(n-y-1)//2 - 1

offsets = np.array([offset(y) for y in range(n)])
print(offsets)

for x in range(n):
    for y in range(x):
        idx[y, x] = x + offset(y)

print(idx)
maxes = (offsets+n-1)[:-1]
idx2 = np.zeros((n, n), np.int)

for i in range((n-1)*n//2):
    y = np.argmax(i <= maxes)
    x = i - offset(y)
    print(y, x)
    idx2[y, x] = i
print(idx2)
