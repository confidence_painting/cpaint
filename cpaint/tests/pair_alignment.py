import numpy as np
import random

def align_inds(inds1, inds2):
    # relies on the inds being sorted
    N = len(inds1)
    M = len(inds2)
    left = np.zeros(N, np.long)
    right = np.zeros(M, np.long)
    n = 0
    left_off = 0
    for i in range(N):
        for j in range(left_off, M):
            if inds1[i] < inds2[j]:
                break
            if inds1[i] == inds2[j]:
                left_off = j+1
                left[n] = i
                right[n] = j
                n += 1
                break
    return left[:n], right[:n]

ind = list(range(10000))
random.shuffle(ind)
inds1 = ind[:6000]
inds1.sort()
random.shuffle(ind)
inds2 = ind[:7000]
inds2.sort()
l, r = align_inds(inds1, inds2)
a = [inds1[i] for i in l]
b = [inds2[i] for i in r]
print(a == b)
