import os

def replace_filename(path, fname):
    return os.path.join(os.path.dirname(path), fname)

def point_selector(img, n):
    pts = []
    fig = plt.figure()
    ax = fig.add_subplot(111)
    def onclick(event):
        if event.xdata != None and event.ydata != None:
            #  inv = ax.transData.inverted()
            #  trans_pt = inv.transform(fig.transFigure(transform(pt)))
            pts.append([event.xdata, event.ydata])
            ax.scatter(*zip(*pts))
            fig.canvas.draw()
            if len(pts) >= n:
                plt.close()
    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    ax.imshow(img)
    plt.show()
    return pts
