# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Depends on: detector, homography
# What this does: Visualizes matches using descriptors

import torch
import cv2
import os
import numpy as np
from cpaint.datasets.unrealcv import read_pose
from cpaint.core.image_ops import scale_then_crop
from cpaint.tests.util import replace_filename
import matplotlib.pyplot as plt

from cpaint.core.descriptors import DESCRIPTORS, NORM_TYPES
from cpaint.core.detectors import DETECTORS, doublewrap
import cpaint.homography_pair_test as hpt
from cpaint.core.util3d import calib_to_K
from cpaint import models


def test_det_desc(det_desc, norm_type):
    im1 = cv2.imread(replace_filename(__file__, "../assets/0000_rgb.png"))
    im2 = cv2.imread(replace_filename(__file__, "../assets/0002_rgb.png"))
    default_size = im1.shape
    #  desired_size = (120, 160)
    desired_size = (120, 160)
    im1 = scale_then_crop(im1, desired_size)
    im2 = scale_then_crop(im2, desired_size)
    #  im1 = scale_then_crop(im1, (480, 480))
    #  im2 = scale_then_crop(im2, (480, 480))
    pose1 = read_pose(replace_filename(__file__, "../assets/0000_pose.json"))
    pose2 = read_pose(replace_filename(__file__, "../assets/0002_pose.json"))
    def_calib = np.array([320, 320, 240, 240])
    calib = def_calib * desired_size[0] / default_size[0]
    calib[2] = desired_size[0]//2
    calib[3] = desired_size[1]//2
    print(f"Calib: {calib} with image size: {desired_size}")

    K = calib_to_K(calib)
    h, w = im1.shape[:2]

    """
    # Detect
    # Format: [[x, y, score]]
    pts1 = det.detect(im1, threshold=0.02)
    pts2 = det.detect(im2, threshold=0.02)
    # Convert pts back to opencv points
    key_pts1 = [cv2.KeyPoint(p[1], p[0], _size=p[2]) for p in pts1]
    key_pts2 = [cv2.KeyPoint(p[1], p[0], _size=p[2]) for p in pts2]

    # Describe pts
    _, desc1 = desc.compute(im1, key_pts1)
    _, desc2 = desc.compute(im2, key_pts2)
    """
    pts1, desc1 = det_desc.detectAndCompute(im1)
    pts2, desc2 = det_desc.detectAndCompute(im1)

    pts1 = pts1[:, :2]
    pts2 = pts2[:, :2]

    print(f"Pts1 shape: {pts1.shape}")
    print(f"Pts2 shape: {pts2.shape}")
    print(f"Desc1 shape: {desc1.shape}")
    print(f"Desc2 shape: {desc2.shape}")

    mimg = hpt.visualize_matches(desc1, desc2, pts1, pts2, im1, im2, K, norm_type)
    plt.imshow(mimg)
    plt.show()

    """
    src_pts, dst_pts = hpt.create_point_pairs(desc1, desc2, pts1, pts2, norm_type)

    #  pts1 = pts1[:, (1, 0)]
    #  pts2 = pts2[:, (1, 0)]

    # Draw pts
    kkpts_img1 = cv2.drawKeypoints(im1, key_pts1, np.array([]))
    kkpts_img2 = cv2.drawKeypoints(im2, key_pts2, np.array([]))
    # Draw matche
    matches = np.hstack((kkpts_img1, kkpts_img2))
    endpts = dst_pts[:,:2]+np.array([0, w]).reshape(1, 2)
    for (p1, p2) in zip(src_pts[:, :2], endpts):
        cv2.line(matches, tuple(p1[::-1].astype(np.int)), tuple(p2[::-1].astype(np.int)), (0, 0, 255))
    #  quivs = np.concatenate((, ), axis=1)

    fig = plt.figure(figsize=(3,3), dpi=600, num='')
    f = fig.add_subplot(1, 2, 1)
    f.set_title("Matches")
    f.imshow(matches)
    f = fig.add_subplot(1, 2, 2)
    original = np.hstack((im1, im2))
    f.set_title("Keypoints")
    f.imshow(original)
    plt.show()
    """

    # Estimate pose difference and print error
    err, est_pose_diff, num_matches = hpt.get_pose_diff(desc1, desc2, pts1, pts2, K, norm_type)
    if err is None:
        pose_diff = np.linalg.inv(pose1).dot(pose2)
        print(f"Estimated Pose Diff Mat: \n{est_pose_diff}")
        print(f"Pose Diff Mat: \n{pose_diff}")
        # Calculate error
        dist, ang_err = hpt.getEstimationError(pose_diff, est_pose_diff)
        print(f"Dist Err: {dist}, Angle Err(Degrees): {ang_err}")
    else:
        print(f"Failed to estimate pose. Error: {err}")

if __name__ == "__main__":

    """
    # We only want to initialize each detector once
    detectors = {}
    descriptors = {}
    for det_name in DETECTORS:
        det = DETECTORS[det_name]()
        if not hasattr(det, "detect"):
            print(f"Detector {dname} does not have detect.")
            continue
        detectors[det_name] = det

    for desc_name in DESCRIPTORS:
        desc = DESCRIPTORS[desc_name]()
        if not hasattr(desc, "compute"):
            print(f"Descriptor {desc_name} does not have compute.")
            continue
        descriptors[desc_name] = desc

    # Run viz
    for desc_name in DESCRIPTORS:
        for det_name in DETECTORS:
            title = f"Testing detector: {det_name} using descriptor: {desc_name}"
            print(title)
            det = detectors[det_name]
            desc = descriptors[desc_name]
            norm_type = NORM_TYPES[desc_name]
    """
    #  det = DETECTORS["superpoint"]()
    #  desc = DESCRIPTORS["superpoint"]
    #  checkpoint = 'checkpoints/spatial_superpoint.pth'
    checkpoint = '/data/output/checkpoints/checkpoint003.pth'
    checkpoint = ''
    use_pretrained = True

    device = torch.device("cuda")
    #  model_cfg = models.MODELS["spatial_superpoint"]
    model_cfg = models.MODELS["superpoint"]
    model = model_cfg["net"]().to(device)

    # Load checkpoint
    if (checkpoint != ''):
        ckpt = torch.load(checkpoint)
        if "state_dict" in ckpt:
            state_dict = ckpt["state_dict"]
        else:
            state_dict = ckpt
        model.load_state_dict(state_dict)
        if "optim_state_dict" in ckpt:
            optimizer.load_state_dict(ckpt["optim_state_dict"])
    elif use_pretrained:
        model.load_default_state_dict()

    #  det = models.model_to_detector(model, model_cfg["color_input"], model_cfg["input_size_multiple"])
    #  det = detectors.doublewrap(det)()
    #  desc = models.model_to_descriptor(model, model_cfg["color_input"], model_cfg["input_size_multiple"])
    det_desc = models.model_to_feature_extractor(model, model_cfg["color_input"], 0.019, model_cfg["input_size_multiple"])
    #  desc = DESCRIPTORS["orb"]()
    norm = NORM_TYPES["superpoint"]
    #  test_det_desc(det, desc, norm_type=norm)
    test_det_desc(det_desc, norm_type=norm)
