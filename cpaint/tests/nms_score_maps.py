
import cv2
import skimage.feature
import matplotlib.pyplot as plt
from cpaint.tests.util import replace_filename
from cpaint.core.detectors import DETECTORS, nms_score_map, peakiness

if __name__ == "__main__":
    detector = DETECTORS['superpoint']()
    # test image
    im = cv2.imread(replace_filename(__file__, "../assets/0000_rgb.png"))
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
    score_map = detector.predict(gray)

    p_score_map = peakiness(score_map)

    sk_det = skimage.feature.peak_local_max(
        score_map, min_distance=2,
        threshold_rel=0.0001, indices=False) * score_map

    psk_det = skimage.feature.peak_local_max(
        p_score_map, min_distance=2,
        threshold_rel=0.0001, indices=False) * p_score_map

    fig = plt.figure(figsize=(3,3), dpi=600)
    f = fig.add_subplot(2, 3, 1)
    f.imshow(score_map)
    f = fig.add_subplot(2, 3, 2)
    f.imshow(sk_det)
    f = fig.add_subplot(2, 3, 3)
    #  f.imshow()


    f = fig.add_subplot(2, 3, 4)
    f.imshow(p_score_map)
    f = fig.add_subplot(2, 3, 5)
    f.imshow(psk_det)
    f = fig.add_subplot(2, 3, 6)
    #  f.imshow()

    plt.show()
