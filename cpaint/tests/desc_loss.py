import torch
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import cv2
import matplotlib.pyplot as plt
from icecream import ic

from cpaint.models import loss_util

N, D, H, W = 32, 256, 30, 30
epochs = 15


def draw_h():
    drawn = torch.zeros(H, W)
    if H < 10:
        drawn[0, 0] = 1
        drawn[1, 1] = 2
    else:
        ind = 1
        for i in range(10):
            drawn[i, 3] = ind
            ind += 1
            drawn[i, 7] = ind
            ind += 1
        for j in range(3, 7):
            drawn[4, j] = ind
            ind += 1
    return drawn

def nn_match_two_way(desc1, desc2, nn_thresh):
    """
    From Superpoint by MagicLeap
    Performs two-way nearest neighbor matching of two sets of descriptors, such
    that the NN match from descriptor A->B must equal the NN match from B->A.
    Inputs:
      desc1 - NxM numpy matrix of N corresponding M-dimensional descriptors.
      desc2 - NxM numpy matrix of N corresponding M-dimensional descriptors.
      nn_thresh - Optional descriptor distance below which is a good match.
    Returns:
      matches - 3xL numpy array, of L matches, where L <= N and each column i is
                a match of two descriptors, d_i in image 1 and d_j' in image 2:
                [d_i index, d_j' index, match_score]^T
    """
    assert desc1.shape[0] == desc2.shape[0]
    if desc1.shape[1] == 0 or desc2.shape[1] == 0:
      return np.zeros((3, 0))
    if nn_thresh < 0.0:
      raise ValueError('\'nn_thresh\' should be non-negative')
    # Compute L2 distance. Easy since vectors are unit normalized.
    dmat = np.dot(desc1.T, desc2)
    dmat = np.sqrt(2-2*np.clip(dmat, -1, 1))
    # Get NN indices and scores.
    idx = np.argmin(dmat, axis=1)
    scores = dmat[np.arange(dmat.shape[0]), idx]
    # Threshold the NN matches.
    keep = scores < nn_thresh
    ic(dmat.mean())
    ic(scores.mean())
    ic(keep.sum())
    # Check if nearest neighbor goes both directions and keep those.
    idx2 = np.argmin(dmat, axis=0)
    keep_bi = np.arange(len(idx)) == idx2[idx]
    keep = np.logical_and(keep, keep_bi)
    idx = idx[keep]
    scores = scores[keep]
    # Get the surviving point indices.
    m_idx1 = np.arange(desc1.shape[1])[keep]
    m_idx2 = idx
    # Populate the final 3xN match data structure.
    matches = np.zeros((3, int(keep.sum())))
    matches[0, :] = m_idx1
    matches[1, :] = m_idx2
    matches[2, :] = scores
    return matches

def draw_matches(im_pts1, im_pts2, desc1, desc2):
    # Perform mutual matching (use superpoint)
    matches = nn_match_two_way(desc1.detach().cpu().numpy().squeeze().T, desc2.detach().cpu().numpy().squeeze().T, 0.7)
    # Initialize the stacked images using the h's
    H, W = im_pts1.shape
    s = 10

    print(f"Num matches: {matches.shape[1]}")
    stacked = np.concatenate((im_pts1, im_pts2), axis=1).reshape(H, 2*W, 1)
    stacked /= np.max(stacked)/128
    stacked = np.repeat(stacked, 3, axis=2)
    stacked = cv2.resize(stacked, (2*s*W, s*H), interpolation=cv2.INTER_NEAREST)
    lines_im = np.zeros((s*W, 2*s*H, 3), np.uint8)
    m = max(1 if matches.shape[1] == 0 else np.max(matches[2,:]), 0.01)
    intensity = int(min(np.max(stacked), 255)/m)
    # iterate through and draw lines
    for n, (i, j, score) in enumerate(matches.T):
        i = int(i)
        j = int(j)
        x1 = i % W
        y1 = i // W
        x2 = j % W + W
        y2 = j // W
        offset = n % s
        lines_im = cv2.line(
                lines_im,
                (x1*s+offset, y1*s+offset),
                (x2*s+offset, y2*s+offset),
                (0, intensity, 0))

    im = stacked.astype(np.uint32) + lines_im.astype(np.uint32)
    plt.imshow(im)
    plt.show()


# Create match matrix
points1_im = draw_h()
points2_im = draw_h()
pts1 = loss_util.img_to_pts(points1_im)
pts2 = loss_util.img_to_pts(points2_im)
mask1 = torch.ones(1, H, W)
mask2 = torch.ones(1, H, W)

match = loss_util.create_pairs(pts1, pts2, mask1, mask2, W, H)
matches = torch.stack([match.clone() for _ in range(N)], dim=0)

# Draw matches to debug
drawn_matches = loss_util.draw_matches(points1_im, points2_im, match)
num_pos = len(torch.where(match==1)[0])
num_neg = len(torch.where(match==-1)[0])
#  plt.imshow(drawn_matches)
#  plt.show()
# They look good

# Initialize "network"
desc1 = torch.rand(N, H*W, D)
desc1.requires_grad = True
desc2 = torch.rand(N, H*W, D)
desc2.requires_grad = True
optimizer = optim.Adam([desc1, desc2], lr=0.1, betas=(0.9, 0.999))

# Draw initial matches
n_desc1 = F.normalize(desc1, p=2, dim=2)
n_desc2 = F.normalize(desc2, p=2, dim=2)
draw_matches(points1_im, points2_im, n_desc1[0], n_desc2[0])
#  print(n_desc1)
#  print(n_desc2)
#  ic(matches.sum())

for i in range(epochs):
    #  ic(n_desc1.mean())
    #  ic(n_desc2.mean())
    optimizer.zero_grad()
    # Normalize
    n_desc1 = F.normalize(desc1, p=2, dim=2)
    n_desc2 = F.normalize(desc2, p=2, dim=2)
    p, n = loss_util.hinge_loss(n_desc1, n_desc2, matches)
    #  p, n = loss_util.hard_constrative_loss(n_desc1, n_desc2, match)
    ic(p, n)
    loss = p + n
    loss.backward()
    optimizer.step()
    if i % 10 == 0:
        print(f"Loss: {loss}")

# Draw trained matches
n_desc1 = F.normalize(desc1, p=2, dim=2)
n_desc2 = F.normalize(desc2, p=2, dim=2)
draw_matches(points1_im, points2_im, n_desc1[0], n_desc2[0])
