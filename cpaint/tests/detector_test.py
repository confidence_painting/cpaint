# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# What this does: Visualizes detections on some example images

import cv2
import os
import numpy as np
from cpaint.datasets.unrealcv import read_pose
from cpaint.tests.util import replace_filename
import matplotlib.pyplot as plt

from cpaint.core.detectors import DETECTORS

if __name__ == "__main__":
    im1 = cv2.imread(replace_filename(__file__, "../assets/0000_rgb.png"))
    im2 = cv2.imread(replace_filename(__file__, "../assets/0002_rgb.png"))
    stacked = np.vstack([im1, im2])
    print(stacked.shape)
    for dname in DETECTORS:
        print(f"Testing {dname}")
        det = DETECTORS[dname]()
        if not hasattr(det, "predict"):
            print(f"Detector {dname} does not have predict.")
            pred = np.ones_like(stacked)
        else:
            pred = det.predict(stacked)
        if not hasattr(det, "detect"):
            print(f"Detector {dname} does not have detect.")
            pts = []
        else:
            # Format: [[x, y, score]]
            pts = det.detect(stacked, top_n=500)
            print(f"Found {len(pts)} keypoints.")

        # Convert pts back to opencv points
        key_pts = [cv2.KeyPoint(p[1], p[0], _size=p[2]) for p in pts]
        kkpts_img = cv2.drawKeypoints(stacked, key_pts, np.array([]))
        fig = plt.figure(figsize=(3,3), dpi=600)
        f = fig.add_subplot(1, 3, 1)
        f.set_title("Source")
        f.imshow(stacked)
        f = fig.add_subplot(1, 3, 2)
        f.set_title("Keypoints")
        f.imshow(kkpts_img)
        f = fig.add_subplot(1, 3, 3)
        f.set_title("Pred")
        f.imshow(pred)
        plt.show()
