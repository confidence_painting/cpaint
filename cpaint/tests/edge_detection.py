# MIT License
#
# Copyright (c) 2020 Alexander Mai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import cv2
import os
import torch
import numpy as np
from cpaint.datasets.unrealcv import read_pose, convert_depth
from cpaint.core.image_ops import scale_then_crop, fast_interp
from cpaint.tests.util import replace_filename, point_selector
import cpaint.homography_pair_test as hpt
import matplotlib.pyplot as plt
from cpaint.core import detector_util

import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables import Keypoint, KeypointsOnImage
from imgaug.augmentables.heatmaps import HeatmapsOnImage
from imgaug.augmentables.segmaps import SegmentationMapsOnImage

def norm(tens):
    return (tens-tens.min())/(tens.max()-tens.min())

if __name__ == "__main__":
    # Initialize vars
    edgys = [
        ("../assets/reproj_score_00225.exr", "../assets/data_counts_00225.exr",)
    ]

    square_size = (480, 480)
    image_size = (240, 240)
    # fx, fy, cx, cy
    calib = [image_size[0]*320/480, image_size[1]*320/480, image_size[0]/2, image_size[1]/2]

    voxel_size = 0.005

    for (reproj_path, count_path) in edgys:
        # Load
        reproj = cv2.imread(replace_filename(__file__, reproj_path), -1)
        count = cv2.imread(replace_filename(__file__, count_path), -1)
        count = np.where(count < 0.5, 1, count)
        score = reproj/count
        score[np.isnan(score)] = 0

        score = scale_then_crop(score, image_size)
        score = torch.from_numpy(score)
        #  edgy_mask = detector_util.mask_max(score, radius=1)
        #  edgy_mask = detector_util.mask_edgyness(score)
        #  peakiness = detector_util.peakiness(score)
        edgyness = detector_util.edgyness(score)
        print(edgyness.shape)
        #  mask = detector_util.mask_max((edgyness > 1e-4)*score, radius=2)
        mask = detector_util.mask_max(edgyness, radius=2)

        """
        cells = []
        edgy = HeatmapsOnImage(norm(score*edgy_mask).numpy().squeeze(), shape=score.shape, min_value=0, max_value=1)
        peaki = HeatmapsOnImage(norm(peakiness).numpy().squeeze(), shape=score.shape, min_value=0, max_value=1)
        peaki_m = HeatmapsOnImage(norm(score*peaki_mask).numpy().squeeze(), shape=score.shape, min_value=0, max_value=1)
        score_i = HeatmapsOnImage(norm(score).numpy(), shape=score.shape, min_value=0, max_value=1)

        cells.append(edgy.draw_on_image(score_i.draw()[0])[0])
        cells.append(edgy.draw()[0])

        cells.append(peaki.draw_on_image(score_i.draw()[0])[0])
        cells.append(peaki.draw()[0])

        cells.append(peaki_m.draw_on_image(score_i.draw()[0])[0])
        cells.append(peaki_m.draw()[0])

        cells.append(score_i.draw()[0])

        grid_image = ia.draw_grid(cells, cols=2)
        plt.imshow(grid_image)
        plt.show()
        """
        fig = plt.figure(figsize=(4.5, 4.5), dpi=600)

        f = fig.add_subplot(2, 2, 1)
        f.set_title("First order")
        #  f.imshow(local_max_1.squeeze()*score)
        #  f.imshow(di.squeeze())
        f.imshow(edgyness.squeeze())

        f = fig.add_subplot(2, 2, 2)
        f.set_title("Second order")
        #  f.imshow(local_max_2.squeeze()*score)
        f.imshow((mask).squeeze())

        f = fig.add_subplot(2, 2, 3)
        f.set_title("Both")
        mask = (mask*edgyness) > 0.0001
        #  f.imshow(local_max_2.squeeze()*local_max_1.squeeze()*score)
        f.imshow(mask.squeeze()*edgyness.squeeze())

        f = fig.add_subplot(2, 2, 4)
        f.set_title("Original")
        f.imshow(score)

        plt.show()

