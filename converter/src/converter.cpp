#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <fstream>
#include <vector>
#include <string>
#include "mat4f.h"

namespace fs = boost::filesystem;

void export_calib (fs::path dataPath)
{
  std::ifstream f;
  f.open((dataPath / "calib_depth.txt").string());
  mat4f calib;
  f.read((char*)&calib, sizeof(mat4f));
  f.close();

  std::ofstream nice_calib;
  nice_calib.open((dataPath / "calib_depth_nice.txt").string(), std::ios::trunc);
  nice_calib << "{\n";
  nice_calib << "  \"fx\":" << calib._m00 << ",\n";
  nice_calib << "  \"fy\":" << calib._m11 << ",\n";
  nice_calib << "  \"cx\":" << calib._m02 << ",\n";
  nice_calib << "  \"cy\":" << calib._m12 << "\n";
  nice_calib << "}";
  nice_calib.close();
}

int main(int argc, char * argv[])
{
  if (argc != 2) {
    std::cout << "Usage: converter <scannet_dir>, where each \n\t<scannet_dir>/images/<scene>/calib_depth.txt \ncan be found." << std::endl;
    return -1;
  }
  fs::path scannet_dir(argv[1]);
  scannet_dir = scannet_dir / "images";

  if(fs::is_directory(scannet_dir)) {
    std::cout << "Converting" << std::endl;

    for(auto& area : boost::make_iterator_range(
          fs::directory_iterator(scannet_dir), {}))
    {
      std::cout << "exporting calib for " << area << std::endl;
      export_calib(area);
    }
  }

  return 0;
}
