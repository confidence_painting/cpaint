(897ec9c123b74681f031a53718f12dd6)
first_sota_cpainted.pth
suffers from the grid issue. It sees grids where there are none.

(f2f81049adf7e5ab16f2a746c18ef083)
second_sota_cpainted.pth
simultaneously has gridding issues and doesn't predict enough points

(03031a6592c66f4b2096e389ce8593cc)
third_sota_cpainted.pth
sometimes has issues where it highlights lines. This is the model I used to beat superpoint on aachen with threshold 0.03, maxpool radius 3

(ce237ac850fb44096e0e994d7c798284)
fourth_sota_cpainted.pth
From: edgy-highres_03
