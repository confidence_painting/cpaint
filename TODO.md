## Thresholds
# Role of threshold on output of painting
- Since we have a binary input to the painting, the values are simply ratio of seen to unseen
- This is the place we want to control the number of detections from
- The best way to do this would be to threshold straight from the map
- However, since the values of the detections are the same across views, we can approximate the same result using a threshold
- To do so, we sample detections from the datasets and find a threshold that gives us the desired number of points per a frame
- One thing to keep in mind that we need to feed good points to the network, which means that we need to perform some kind of NMS to get sparse points
# Role of threshold on input to painting
- However, the best way to keep interpretability is to simply let the output be what it is
- The reason we threshold the input is so the output is interpretable
- We need to control the input here to make sure the output stays as clear as possible
- If the threshold is too high, then when the detector "spots" a detection, it might not register, making the output inaccurate
- If the threshold is too low, then the regions that designate a detection might become fuzzy, reducing accuracy
- The threshold to number of detections should form a curve. In fact, there is a very sharp elbow, which we can utilize to find the threshold using the kneedle method
- However, after finding these knees, we are still left with regions rather than points. It is not useful to threshold the input to the painter

## How to train descriptors
Option 1: warp grid using depth and train like superpoint (didn't work well the first time)
Option 2: train on homography pairs like superpoint (kinda lame)
Option 3: augment network to produce higher res descriptors (counter the fact that we freed superpoint from grid) and train on just detections (like R2D2)

# How to find correspondences for training
Step 1: Generate points in image 1 (use a grid)  
Step 2: Warp these points to image 2 densely  
Step 3: Convert these points to images with the index  
Step 4: Pass these binary images to the image augmentation using nearest neighbor  
Step 5: Assert uniqueness of these new points (numpy.unique along axis)  
Step 6: Convert augmented binary images back to points with an index  
Step 7: Match the index of these points with it's pair  
Step 8: Index the descriptor grids using these points, then normalize  
Step 9: Find the negatives using a radius around points to assert that negative pairs are negative  
Step 10: Pass these negatives with eye as positive to margin loss  

## Training detector
Our method requires some kind of signal source to amplify because the networks cannot learn if they are given nothing to work with.
The problem is particularly prominent when the input to the painter is uniform
Another issue is that, if the enforced sparsity of points is too low, edges become the most optimally repeatable zones  
Run the paint train iterations on each individual area, separately. This could help with training stability  

## Descriptor testing
So we want to keep track of descriptor performance during descriptor training  
We can keep track of the follow extra information by adding matching tests
- Number of matches
- Number of accurate matches, as relayed by RANSAC
- Pose Accuracy
- Number of images with sufficient matches
If we use the existing API setup with detectors and descriptors, it will be easier to test
Need to incorporate into normal test dataset
Disable homographies on test set? If not, don't have pose accuracy
First, develop these metrics on the test file. Then add it to the training file.
However, we don't need to perform some kind of comprehensive matching

## Running the precision recall test
We don't want to waste GPU time while running this test. It's all CPU.
- Run detector on dataset and write raw scores to images
  - python cpaint/export\_detections.py configs/superpoint.toml configs/vivelab.toml RealisticRendering untrained\_test --num\_runs 9
- Each process reads these images and thresholds them using it's predefined threshold
- It then runs the painter using these images and retrieves the output
- Here, the problem opens up a bit more
  - Do we run NMS on it?
    - I would say no. Unnecessary bias
  - If we thresholded it
    - Too many options on how to do this
  - Average?
    - Results become hard to interpret because of all the empty space
    - If averaged across each viewpoints' detections, this would work
    - Somehow, we need to determine what is a positive and what is a negative
    - Imposing a threshold on the score of the point would do this, but introduces garbage
    - Since the score of the point is actually the probability that the point would be found in a different viewpoint, there might be a clean solution, like a weighted sum
    - Our input is a set of detections and a set of the probability that the detections are correct
    - Another question is whether we need to apply NMS to find the set of all detections so we can calculate recall
    - We can do average precision

## Multi area training
So we have run individual areas separately under different names and need a way to send this information to each gt source that corresponds to the run
We need to provide some map from areas to run names, preferrably some kind of file
we then input this filename to the gt\_source argument and it loads this map
This is passed this to the TrainDatasetList, which needs to be retooled to always handle this kind of information
Indeed, so does train\_util, which needs to handle cases when the gt\_source is not a file and maps the usual info to a dict
I need to somehow make it so the dataset list is determined by the json file itself

# How to select a threshold
I have the capacity to run the threshold test using the final model on each area.
I can then select the threshold based on which area corresponds best to the desired test.

# Run History
## First Run
- Huge clusters of features in dark areas - remove mask so that the model learns that those dark areas have no points. Right now, it has been detecting points in those masked regions
- Bad points in grass - should be fixed by forest area
- No points on cars - oh well, my datasets have no cars
- Lots of points on edges - I have a new edge removal algorithm to fix this
