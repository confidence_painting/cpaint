# LICENSE
This project uses code from R2D2, which is licensed under the Creative Commons license, which only permits non-commercial use. These files contain the original header. The rest is under the MIT license.  
The specific file is `cpaint/models/r2d2.py`, which is not core and can be removed.

# Running benchmarks
https://stackoverflow.com/questions/42094324/python-struct-pack-and-write-vs-matlab-fwrite  
`hpatch_eval_patch.py` contains a patch for the d2net `hpatches_sequences` evaluation that allows it to handle ORB and BRIEF.  

# Setup Steps
## Installation
Read the Dockerfile. The docker image is available at gitlab-registry.nautilus.optiputer.net/vivecenter-berkeley-edu/zzft-training.
# Expected Directory Structure
├── 2020VisualLocalization  
│   ├── Aachen-Day-Night  
│   ├── CMU-Seasons  
│   ├── InLoc  
│   └── RobotCar-Seasons  
├── d2-net  
│   ├── extract_features.py  
│   ├── extract_hesaff.m  
│   ├── hpatches_sequences  
│   ├── image_list_hpatches_sequences.txt  
│   ├── image_list_qualitative.txt  
│   ├── inloc  
│   ├── lib  
│   ├── LICENSE  
│   ├── megadepth_utils  
│   ├── models  
│   ├── qualitative  
│   ├── README.md  
│   └── train.py  
├── FM-Bench  
│   ├── Dataset  
│   ├── Evaluation  
│   ├── output  
│   ├── Pipeline  
│   ├── README.md  
│   └── vlfeat-0.9.21  
├── output  
│   ├── checkpoints  
│   ├── detector_out  
│   ├── histograms  
│   ├── painted  
│   ├── painted_maps  
│   └── pair_maps  
├── unrealcv  
│   ├── images  
│   ├── octomaps  
│   ├── painted.tar.gz  
│   ├── pair_maps  
│   ├── sims  
│   ├── src  
│   ├── tars  
│   └── unrealcv.tar.gz  
# Notes to future users
Benchmark contains an old, unused benchmark for detector repeatability.  
I want to apologize to anyone reading this for the mess that is the dataset class. It only needed to work once. Extensions of this work need to refactor that part in particular. Perhaps inheritance could solve the issue.   
Issues:  
- The way images are rescaled and what not is not handled in the dataset class like it should be
- As a result, the way the calibration matrix is handled is not consistent
- Because of how the ground truth is separated, a lot of complexity is in the __init__.py
- The datasets that are not unrealcv are not up to date and should not be expected to work out of the box
- A strange bug in the painter causes the values to all zero out if there are too many images input/output. Therefore, the number of images is capped. Could be torch related
  - Issue is not in writing output
- When debugging, make sure to check the input range on both the painter and the trainer
- Keep your images square unless you want to tempt fate
