import cv2

def l2_mnn_matcher(descriptors_a, descriptors_b):
    # descriptors_a: (N, m)
    # descriptors_b: (M, m)
    device = descriptors_a.device
    # (N, M)
    sim = descriptors_a @ descriptors_b.t()
    nn12 = torch.max(sim, dim=1)[1]
    nn21 = torch.max(sim, dim=0)[1]
    ids1 = torch.arange(0, sim.shape[0], device=device)
    mask = (ids1 == nn21[nn12])
    # matches from descriptors_a to descriptors_b
    matches = torch.stack([ids1[mask], nn12[mask]])
    return matches.t().data.cpu().numpy()

def ham_mnn_matcher(descriptors_a, descriptors_b):
    descriptors_a = descriptors_a.data.numpy().astype(np.uint8)
    descriptors_b = descriptors_b.data.numpy().astype(np.uint8)

    matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    good = matcher.match(descriptors_a, descriptors_b)
    matches_list = []
    for match in good:
        matches_list.append([match.queryIdx, match.trainIdx])
    return np.array(matches_list)

def mnn_matcher(descriptors_a, descriptors_b):
    if descriptors_a.dtype == torch.uint8:
        return ham_mnn_matcher(descriptors_a, descriptors_b)
    else:
        return l2_mnn_matcher(descriptors_a, descriptors_b)
