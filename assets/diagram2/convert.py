import numpy as np
import matplotlib.pyplot as plt
import cv2
import argparse
from pathlib import Path
parser = argparse.ArgumentParser()
parser.add_argument('image', type=Path)
parser.add_argument('--colormap', type=str, default='jet')
parser.add_argument('--filter', action='store_true')
args = parser.parse_args()

# some data (512x512)
data = cv2.imread(str(args.image), -1)
if args.filter:
    # filter outliers
    iqr = np.percentile(data, 75) - np.percentile(data, 25)
    data = np.where(data < np.percentile(data, 25) - 1.5*iqr, np.median(data), data)
    data = np.where(data > np.percentile(data, 75) + 1.5*iqr, np.median(data), data)

# a colormap and a normalization instance
cmap = plt.get_cmap(args.colormap)
#  norm = plt.Normalize(vmin=data.min(), vmax=data.max())
norm = plt.Normalize(vmin=data.min(), vmax=np.percentile(data, 99.99))

# map the normalized data to colors
# image is now RGBA (512x512x4) 
image = cmap(norm(data))

# save the image
plt.imsave(f"{args.image.name}.{args.colormap}.png", image)
