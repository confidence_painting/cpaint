import numpy as np
import matplotlib.pyplot as plt
import cv2
import argparse
from pathlib import Path
import torch
import torch.nn.functional as F

# pytorch
def mask_max(score_map, radius=8):
    batch = score_map.view(-1, 1, score_map.shape[-2], score_map.shape[-1])
    N, _, H, W = batch.shape

    padded = F.pad(batch, [radius] * 4, mode='constant', value=0.)
    l_max = F.max_pool2d(
            padded,
            radius*2+1, stride=1
        )
    mask = l_max == batch
    return mask.view(N, H, W)

def resize_with_crop(image, shape):
    #  scale = max(shape[0] / image.shape[0], shape[1] / image.shape[1])
    #  image = cv2.resize(image,
    #                     (int(scale*image.shape[0]), int(scale*image.shape[1])),
    #                     interp)
    (h, w) = image.shape[:2]
    (nh, nw) = shape[:2]
    oh, ow = (h-nh)//2, (w-nw)//2
    return image[oh:oh+nh, ow:ow+nw]

parser = argparse.ArgumentParser()
parser.add_argument('image', type=Path)
parser.add_argument('scores', type=Path)
parser.add_argument('counts', type=Path)
parser.add_argument('--threshold', type=float, default=0.03)
parser.add_argument('--colormap', type=str, default='jet')
parser.add_argument('--filter', action='store_true')
args = parser.parse_args()

# some data (512x512)
img = cv2.imread(str(args.image), -1)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
rp = cv2.imread(str(args.scores), -1)
counts = cv2.imread(str(args.counts), -1)
scores = rp/counts
scores[np.isnan(scores)] = 0
img = resize_with_crop(img, rp.shape)

mask = mask_max(torch.tensor(scores)).squeeze().numpy()

key_pts = [cv2.KeyPoint(p[0], p[1], _size=8) for p in np.array(np.where(scores*mask > args.threshold)).T.astype(np.float)[:, ::-1]]
kkpts_img = img.copy()
kkpts_img = cv2.drawKeypoints(img, key_pts, kkpts_img, color=(0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#  plt.imshow((mask * scores))
plt.imsave(f"{args.image.name}.kpts.png", kkpts_img)
