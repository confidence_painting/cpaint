# Current Problems
- Can't easily save checkpoints under a name
  - I want to rename things after done
- It is not easy to remember which checkpoints represent what kind of changes
- If I save a model with a checkpoint, I need to somehow remember the parameters of the model it was saved with
  - kind of tempted to save the source code
  - there should be a metadata file that stores config as well as the status of git, like the log and diff
- I want an easy way to see what the detections looked like with a model
  - save a photo
- I need to configure parameters (loss, color, etc) and those need to be easily loaded by the model_to_detector
  - kind of like the code being part of the config
  - however, this is messy. Log + diff + config should be enough
- I'm not sure which models need what shape of input
  - standardize models inside themselves
- not all checkpoints need to be fixed
  - rotate 5 checkpoints
- need to graph loss
  - should be part of the metadata folder
- There might be things like feature pyramids or multiscale
  - just make this part of the model
  - train and eval mode

# Solution
checkpoints/{name}/metadata/datetime
checkpoints/{name}/metadata/config.toml
checkpoints/{name}/metadata/gitdiff (git diff)
checkpoints/{name}/metadata/gitlog (git show --oneline -s)
checkpoints/{name}/models/ckpt01-ckpt05.pth

# Checkpoint numbers
based on epoch number
0 = initial checkpoint
1+ = (epoch number)/(number of epochs) * 5 + 1

# Logging Loss
tag, date, loss
